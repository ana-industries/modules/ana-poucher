# Dart wrapper for PouchDB

## Poucher -- Simple wrapper

Dart Wrapper for PouchDB.js. Wire it up with [`Dorker`](https://yuan-kuan.gitlab.io/post/web-worker-and-dart-2/) so that we can use PouchDB in a Web Worker.

Use `PoucherCommand` to standardize the types of operation.

**Caveat**: Since PouchDB only accept JS object in most of it's API. We forced to make a series of expensive parse/encode/decode operation just to get Dart's object in and out of PouchDB.

### PoucherServiceBase

A base class for [Angular Service](https://angulardart.dev/tutorial/toh-pt4) in ANA that use Poucher to do database operation.

- It hides the tedious request respond handling between main application and dorker(Web Worker).
- It provides a more friendly `Future` based asynchronous API.

Because of this, I strongly advice everyone to use this class, and improve it's capabilities instead of by-passing it by talking to `Poucher` directly.

## Smoucher -- Complex wrapper with git like functionality

Sharing the same API and commands, but not extending `Poucher`, Smoucher allow the user of the API to operate on the data the same way of `Poucher`. The differences:

- For all read and find. Smoucher will replicate remote data into a local `PouchDB`, namely `localDb`.
- Smoucher will stage all the create, update and deletes in a another local `PouchDB` namely `smodDb`.
- Smoucher will replicate all changes from `smodDb` into `localDb` all the time, to ensure all user read/find has all the latest local changes combined with remote changes.
- Local changes will need to be merge and push later by the user, so that it appear in the remote DB.

### SmoucherSerivce

Extend `PoucherServiceBase` and add additional functionality like listing all changes and revert changes,

`SmoucherService` can reuse `PoucherServiceBase`'s API because they share the same Command. These commands are converted to text and convert back to `PoucherCommand`/`SmoucherCommand` in the `Dorker`.

## Development

### When to use Built Value
WIth Built Value we can serialize a Dart object before passing it to Dorker and unserialize it to get back a Dart Built Value object ready for use in the program. 
The rule of thumb of when to use Built Value is:
a. If it is a PouchDB document use Built Value as we need to get back a Built Value form when reading from the database.
b. If it is an option we pass into a PouchDB function, we never need to store it or read the option back from the database, then we have no need to use Built Value to serialize / unserialize it. Both `queryOptions` argument of `query()` and the `selector` argument of `find()` fall into this category. 

### Git branches

`master` is protected and reserve for main application development.

All active development should happen on `dev` branch. You should always branch out from `dev` and create push request against the `dev`.

### Pub Packages and ANA Modules

We open sourced a few building blocks of ANA, and these projects are ANA Modules. The cost of this is a tedious packages management in the main application.

Developers went through the trial projects will not be foreign to ANA Modules. Just a rule of thumb, we should always use the git packages unless you are working on feature related to that module.

### Style guide

[Dart's official guide](https://dart.dev/guides/language/effective-dart)

### Code Formatting

For `.dart`, use `dartfmt`. It is a tool came with Dart SDK. Do this in your command line `dartfmt -w .`

For `.html` `.scss` `.yaml`, use [prettier](https://prettier.io/docs/en/index.html). Install it and type `prettier --write "**/*.html" "**/*.scss" "**/*.yaml"` in your command line.

Better, if your IDE support it, just install the extension of these and enabled 'format on save'. It will be easier.

### Rules

- Commit and push your branch to GitLab, often.
- Have fun.

## Test

All tests are run in browser only, because PouchDB is a js library.

To run the test in command line:  
for dartvec version: `pub run build_runner test`  
for dert2js version: `pub run build_runner test -r` or `pub run test`

To run the test on a single file:
```
pub run build_runner test -- test/poucher/poucher_service_test.dart
```

To run a single test
```
pub run build_runner test -- test/poucher/poucher_service_test.dart -n "<name of a single test, or test name matching the word>"
```

To run the test in browser:  
`webdev serve test` and visit the serving local address. Click on the link to start the test.

## TODO

### Poucher

- More test to cover all functions.

### Smoucher

- Pull remote changed into `smodDb`.
- Display all the conflicts, allow user to resolve them.
- Push (replicate) `smodDb` to remote
