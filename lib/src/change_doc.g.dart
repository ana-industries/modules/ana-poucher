// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_doc.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const OperationType _$conflict = const OperationType._('conflict');
const OperationType _$resolved = const OperationType._('resolved');
const OperationType _$replicate = const OperationType._('replicate');
const OperationType _$create = const OperationType._('create');
const OperationType _$update = const OperationType._('update');
const OperationType _$delete = const OperationType._('delete');

OperationType _$operationTypeValueOf(String name) {
  switch (name) {
    case 'conflict':
      return _$conflict;
    case 'resolved':
      return _$resolved;
    case 'replicate':
      return _$replicate;
    case 'create':
      return _$create;
    case 'update':
      return _$update;
    case 'delete':
      return _$delete;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<OperationType> _$operationTypeValues =
    new BuiltSet<OperationType>(const <OperationType>[
  _$conflict,
  _$resolved,
  _$replicate,
  _$create,
  _$update,
  _$delete,
]);

Serializer<OperationType> _$operationTypeSerializer =
    new _$OperationTypeSerializer();
Serializer<Change> _$changeSerializer = new _$ChangeSerializer();
Serializer<ChangeDoc> _$changeDocSerializer = new _$ChangeDocSerializer();

class _$OperationTypeSerializer implements PrimitiveSerializer<OperationType> {
  static const Map<String, String> _toWire = const <String, String>{
    'conflict': 'aConflict',
    'resolved': 'adResolved',
    'replicate': 'aReplicate',
    'create': 'bCreate',
    'update': 'cUpdate',
    'delete': 'dDelete',
  };
  static const Map<String, String> _fromWire = const <String, String>{
    'aConflict': 'conflict',
    'adResolved': 'resolved',
    'aReplicate': 'replicate',
    'bCreate': 'create',
    'cUpdate': 'update',
    'dDelete': 'delete',
  };

  @override
  final Iterable<Type> types = const <Type>[OperationType];
  @override
  final String wireName = 'OperationType';

  @override
  Object serialize(Serializers serializers, OperationType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  OperationType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      OperationType.valueOf(_fromWire[serialized] ?? serialized as String);
}

class _$ChangeSerializer implements StructuredSerializer<Change> {
  @override
  final Iterable<Type> types = const [Change, _$Change];
  @override
  final String wireName = 'Change';

  @override
  Iterable<Object> serialize(Serializers serializers, Change object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'operationType',
      serializers.serialize(object.operationType,
          specifiedType: const FullType(OperationType)),
    ];
    if (object.rev != null) {
      result
        ..add('rev')
        ..add(serializers.serialize(object.rev,
            specifiedType: const FullType(String)));
    }
    if (object.serializedDoc != null) {
      result
        ..add('serializedDoc')
        ..add(serializers.serialize(object.serializedDoc,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Change deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ChangeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'operationType':
          result.operationType = serializers.deserialize(value,
              specifiedType: const FullType(OperationType)) as OperationType;
          break;
        case 'rev':
          result.rev = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'serializedDoc':
          result.serializedDoc = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$ChangeDocSerializer implements StructuredSerializer<ChangeDoc> {
  @override
  final Iterable<Type> types = const [ChangeDoc, _$ChangeDoc];
  @override
  final String wireName = 'ChangeDoc';

  @override
  Iterable<Object> serialize(Serializers serializers, ChangeDoc object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'docId',
      serializers.serialize(object.docId,
          specifiedType: const FullType(String)),
      'docType',
      serializers.serialize(object.docType,
          specifiedType: const FullType(String)),
      'lastOperation',
      serializers.serialize(object.lastOperation,
          specifiedType: const FullType(OperationType)),
      'changes',
      serializers.serialize(object.changes,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Change)])),
      '_id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];
    if (object.rev != null) {
      result
        ..add('_rev')
        ..add(serializers.serialize(object.rev,
            specifiedType: const FullType(String)));
    }
    if (object.deleted != null) {
      result
        ..add('_deleted')
        ..add(serializers.serialize(object.deleted,
            specifiedType: const FullType(bool)));
    }
    if (object.conflicts != null) {
      result
        ..add('_conflicts')
        ..add(serializers.serialize(object.conflicts,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    return result;
  }

  @override
  ChangeDoc deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ChangeDocBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'docId':
          result.docId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'docType':
          result.docType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'lastOperation':
          result.lastOperation = serializers.deserialize(value,
              specifiedType: const FullType(OperationType)) as OperationType;
          break;
        case 'changes':
          result.changes.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(Change)]))
              as BuiltList<dynamic>);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_rev':
          result.rev = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_deleted':
          result.deleted = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case '_conflicts':
          result.conflicts.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$Change extends Change {
  @override
  final OperationType operationType;
  @override
  final String rev;
  @override
  final String serializedDoc;

  factory _$Change([void Function(ChangeBuilder) updates]) =>
      (new ChangeBuilder()..update(updates)).build();

  _$Change._({this.operationType, this.rev, this.serializedDoc}) : super._() {
    if (operationType == null) {
      throw new BuiltValueNullFieldError('Change', 'operationType');
    }
  }

  @override
  Change rebuild(void Function(ChangeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChangeBuilder toBuilder() => new ChangeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Change &&
        operationType == other.operationType &&
        rev == other.rev &&
        serializedDoc == other.serializedDoc;
  }

  @override
  int get hashCode {
    return $jf($jc($jc($jc(0, operationType.hashCode), rev.hashCode),
        serializedDoc.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Change')
          ..add('operationType', operationType)
          ..add('rev', rev)
          ..add('serializedDoc', serializedDoc))
        .toString();
  }
}

class ChangeBuilder implements Builder<Change, ChangeBuilder> {
  _$Change _$v;

  OperationType _operationType;
  OperationType get operationType => _$this._operationType;
  set operationType(OperationType operationType) =>
      _$this._operationType = operationType;

  String _rev;
  String get rev => _$this._rev;
  set rev(String rev) => _$this._rev = rev;

  String _serializedDoc;
  String get serializedDoc => _$this._serializedDoc;
  set serializedDoc(String serializedDoc) =>
      _$this._serializedDoc = serializedDoc;

  ChangeBuilder();

  ChangeBuilder get _$this {
    if (_$v != null) {
      _operationType = _$v.operationType;
      _rev = _$v.rev;
      _serializedDoc = _$v.serializedDoc;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Change other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Change;
  }

  @override
  void update(void Function(ChangeBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Change build() {
    final _$result = _$v ??
        new _$Change._(
            operationType: operationType,
            rev: rev,
            serializedDoc: serializedDoc);
    replace(_$result);
    return _$result;
  }
}

class _$ChangeDoc extends ChangeDoc {
  @override
  final String docId;
  @override
  final String docType;
  @override
  final OperationType lastOperation;
  @override
  final BuiltList<Change> changes;
  @override
  final String id;
  @override
  final String rev;
  @override
  final bool deleted;
  @override
  final BuiltList<String> conflicts;

  factory _$ChangeDoc([void Function(ChangeDocBuilder) updates]) =>
      (new ChangeDocBuilder()..update(updates)).build();

  _$ChangeDoc._(
      {this.docId,
      this.docType,
      this.lastOperation,
      this.changes,
      this.id,
      this.rev,
      this.deleted,
      this.conflicts})
      : super._() {
    if (docId == null) {
      throw new BuiltValueNullFieldError('ChangeDoc', 'docId');
    }
    if (docType == null) {
      throw new BuiltValueNullFieldError('ChangeDoc', 'docType');
    }
    if (lastOperation == null) {
      throw new BuiltValueNullFieldError('ChangeDoc', 'lastOperation');
    }
    if (changes == null) {
      throw new BuiltValueNullFieldError('ChangeDoc', 'changes');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('ChangeDoc', 'id');
    }
  }

  @override
  ChangeDoc rebuild(void Function(ChangeDocBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChangeDocBuilder toBuilder() => new ChangeDocBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChangeDoc &&
        docId == other.docId &&
        docType == other.docType &&
        lastOperation == other.lastOperation &&
        changes == other.changes &&
        id == other.id &&
        rev == other.rev &&
        deleted == other.deleted &&
        conflicts == other.conflicts;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc($jc($jc(0, docId.hashCode), docType.hashCode),
                            lastOperation.hashCode),
                        changes.hashCode),
                    id.hashCode),
                rev.hashCode),
            deleted.hashCode),
        conflicts.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ChangeDoc')
          ..add('docId', docId)
          ..add('docType', docType)
          ..add('lastOperation', lastOperation)
          ..add('changes', changes)
          ..add('id', id)
          ..add('rev', rev)
          ..add('deleted', deleted)
          ..add('conflicts', conflicts))
        .toString();
  }
}

class ChangeDocBuilder
    implements Builder<ChangeDoc, ChangeDocBuilder>, CouchyBuilder {
  _$ChangeDoc _$v;

  String _docId;
  String get docId => _$this._docId;
  set docId(String docId) => _$this._docId = docId;

  String _docType;
  String get docType => _$this._docType;
  set docType(String docType) => _$this._docType = docType;

  OperationType _lastOperation;
  OperationType get lastOperation => _$this._lastOperation;
  set lastOperation(OperationType lastOperation) =>
      _$this._lastOperation = lastOperation;

  ListBuilder<Change> _changes;
  ListBuilder<Change> get changes =>
      _$this._changes ??= new ListBuilder<Change>();
  set changes(ListBuilder<Change> changes) => _$this._changes = changes;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _rev;
  String get rev => _$this._rev;
  set rev(String rev) => _$this._rev = rev;

  bool _deleted;
  bool get deleted => _$this._deleted;
  set deleted(bool deleted) => _$this._deleted = deleted;

  ListBuilder<String> _conflicts;
  ListBuilder<String> get conflicts =>
      _$this._conflicts ??= new ListBuilder<String>();
  set conflicts(ListBuilder<String> conflicts) => _$this._conflicts = conflicts;

  ChangeDocBuilder();

  ChangeDocBuilder get _$this {
    if (_$v != null) {
      _docId = _$v.docId;
      _docType = _$v.docType;
      _lastOperation = _$v.lastOperation;
      _changes = _$v.changes?.toBuilder();
      _id = _$v.id;
      _rev = _$v.rev;
      _deleted = _$v.deleted;
      _conflicts = _$v.conflicts?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant ChangeDoc other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ChangeDoc;
  }

  @override
  void update(void Function(ChangeDocBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChangeDoc build() {
    _$ChangeDoc _$result;
    try {
      _$result = _$v ??
          new _$ChangeDoc._(
              docId: docId,
              docType: docType,
              lastOperation: lastOperation,
              changes: changes.build(),
              id: id,
              rev: rev,
              deleted: deleted,
              conflicts: _conflicts?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'changes';
        changes.build();

        _$failedField = 'conflicts';
        _conflicts?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ChangeDoc', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
