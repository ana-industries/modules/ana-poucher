import 'dart:convert';
import 'package:dorker/dorker.dart' show Dorker;
import 'package:dorker/dorker_messaging.dart';

import 'js_interop/common.dart';
import 'js_interop/pouchdb.dart';
import 'poucher_command.dart';

import 'utils.dart';

/// Poucher is a `Dorker` for PouchDB.
///
/// By this, we able to spawn dedicated Web Worker in the browser for each PouchDB connections.
/// It is especially important because we forced to do a series of expensive parse/encode/decode to get
/// Dart object in and out PouchDB. Doing it in Web Worker free our main UI thread up for UI task.
///
/// We make all the interop JS API call to PouchDB here. And communicate with the outside with `PoucherCommand`.
///
class Poucher {
  Dorker _boss;
  PouchDB _db;
  String _dbName;

  Poucher(this._boss) {
    _boss.onMessage.listen(_handleBoss);
  }

  void _handleBoss(data) async {
    final request = Request.fromPayload(data);

    switch (PoucherCommand.valueOf(request.command)) {
      case PoucherCommand.init:
        _dbName = request.params['dbName'];
        final username = request.params['username'];
        final password = request.params['password'];

        // Authentication information is optional for local CouchDB or local storage.
        CreateOption createOption;
        if (username != null && password != null) {
          createOption = CreateOption(
              auth: AuthOption(username: username, password: password));
        }

        _db = PouchDB(_dbName, createOption);
        Respond.to(request)..sendThrough(_boss);
        break;

      case PoucherCommand.put:
        try {
          // Unfortunately, I haven't find a better solution to this.
          // 1, encode the params to JSON string.
          // 2, parse the JSON string into JavaScript object.
          // 3, put this object into PouchDB.
          // 4, stringify the returned JavaScript object to JSON string.
          // 5, decode this JSON string to Dart's JSON map
          var doc = jsToDart(
              await promiseToFuture(_db.put(dartToJs(request.params))));
          Respond.to(request, params: doc)..sendThrough(_boss);
        } catch (e) {
          _handleError(e, request);
        }
        break;

      case PoucherCommand.bulkDocs:
        try {
          var doc = jsToDart(
              await promiseToFuture(_db.bulkDocs(dartToJs(request.params))));
          Respond.to(request, params: doc)..sendThrough(_boss);
        } catch (e) {
          _handleError(e, request);
        }
        break;

      case PoucherCommand.get:
        try {
          var doc = json.decode(
              stringify(await promiseToFuture(_db.get(request.params))));
          Respond.to(request, params: doc)..sendThrough(_boss);
        } catch (e) {
          _handleError(e, request);
        }
        break;

      case PoucherCommand.allDocs:
        try {
          var doc = jsToDart(
              await promiseToFuture(_db.allDocs(dartToJs(request.params))));
          Respond.to(request, params: doc)..sendThrough(_boss);
        } catch (e) {
          _handleError(e, request);
        }
        break;

      case PoucherCommand.remove:
        try {
          await promiseToFuture(_db.remove(dartToJs(request.params)));
          Respond.to(request)..sendThrough(_boss);
        } catch (e) {
          _handleError(e, request);
        }
        break;

      case PoucherCommand.createIndex:
        try {
          await promiseToFuture(_db.createIndex(dartToJs(request.params)));
          Respond.to(request)..sendThrough(_boss);
        } catch (e) {
          _handleError(e, request);
        }
        break;

      case PoucherCommand.find:
        try {
          var doc = jsToDart(
              await promiseToFuture(_db.find(dartToJs(request.params))));
          Respond.to(request, params: doc)..sendThrough(_boss);
        } catch (e) {
          _handleError(e, request);
        }
        break;

      case PoucherCommand.query:
        final url = request.params['url'];
        final params = request.params['params'];
        final doc =
            jsToDart(await promiseToFuture(_db.query(url, dartToJs(params))));
        Respond.to(request, params: doc)..sendThrough(_boss);
        break;

      case PoucherCommand.replicate:
        final target = request.params['target'];
        await promiseToFuture(replicate(_dbName, target));
        Respond.to(request)..sendThrough(_boss);
        break;

      case PoucherCommand.destroy:
        await promiseToFuture(_db.destroy());
        Respond.to(request)..sendThrough(_boss);
        break;
    }
  }

  void _handleError(e, Request request) {
    final jsError = jsToDart(e);
    final error = jsError.isEmpty ? {'message': e.toString()} : jsError;
    Respond.error(request, params: error)..sendThrough(_boss);
  }
}
