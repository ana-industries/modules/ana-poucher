import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

import 'package:ana_poucher/src/change_doc.dart';

part 'serializer.g.dart';

@SerializersFor([BuiltList, Change, ChangeDoc, OperationType])
final Serializers serializers = _$serializers;
final standardSerializers = (serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin(discriminator: 'type')))
    .build();
