import 'dart:convert' show JsonEncoder;

JsonEncoder _encoder = JsonEncoder.withIndent('  ');
String beautify(json) => _encoder.convert(json);
