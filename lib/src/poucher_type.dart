class BulkDocRespond {
  final bool ok;
  final String id;
  final String rev;

  BulkDocRespond({this.ok, this.id, this.rev});

  @override
  String toString() => '{ok: $ok, id: $id, rev: $rev}';
}
