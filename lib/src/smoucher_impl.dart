import 'dart:convert' show json;

import 'package:dorker/dorker.dart' show Dorker;
import 'package:dorker/dorker_messaging.dart';
import 'package:js/js.dart';
import 'package:mutex/mutex.dart';
import 'package:uuid/uuid.dart';

import 'js_interop/pouchdb.dart';
import 'change_doc.dart';
import 'poucher_command.dart';
import 'pretty_json.dart';
import 'serializer.dart' show standardSerializers;
import 'utils.dart';

/// Git-style Poucher.
///
/// [Read more](https://sites.google.com/view/ana-wiki/tech-stack/how-it-works/smoucher)
class Smoucher {
  final Dorker _boss;
  final Dorker _logger;
  final _uuid = Uuid();

  /// Mutex to prevent a read/write into local PouchDB that undergoing flushing (destryed and recreate)
  ///
  /// Accessing a PouchDB while it is being destroy or destroyed will throw an exception, and result in
  /// wrong result/behaviour. We introduce the mutex to help us prevent these bad accessing pattern.
  ///
  /// `_mutex.AcquireRead()` is used for all read operations (get, alldocs, find). Despite there are
  /// writing operation in the midst of these operation (replicate document from remote DB). These operations
  /// will not trigger a flush to both local and smod PouchDB. So having multiple of them performing concurrently
  /// is fine.
  ///
  /// `_mutex.AcquireWrite()` is used for all writing operation (put, bulkDocs, revert). It also used
  /// whenever local or smod PouchDB might need to be flushed, e.g. remote database changed.
  final _mutex = ReadWriteMutex();

  /// Cache that store a `find` parameter hash.
  ///
  /// This help reduce remote hit for similar `find`.
  ///
  /// It will be clear when there is any remote changes.
  final _upToDateFinds = Set<int>();

  /// The main DB to be the ultimate truth, i.e. baselines for all documents.
  ///
  /// Not necessary other DB over the internet, can be local CouchDB, or local PouchDB.
  PouchDB _remoteDb;

  /// PouchDB acting as the staging DB for remote baseline combine with local changes.
  ///
  /// It allows READ/FIND operation, but should never perform UPDATE/DELETE directly.
  /// All documents are either replicated from [_remoteDB] or [_smodDB]
  PouchDB _localDb;

  /// PouchDB that store all the local changes.
  ///
  /// It store a complimentary [ChangeDoc] document alongside with each change, keeping track.
  PouchDB _smodDb;

  /// Names to identify the local PouchDB.
  ///
  /// It is critical to use consistent name, if these keep changing, we might leave lot of
  /// garbage in user's browser.
  String _localDbName;
  String _smodDbName;

  /// Special DB act as the restore point before a "pull"
  String _smodRestoreDbName;

  /// Auxiliary [Smoucher] means another instance that spawn by other process, not the main app.
  ///
  /// This usually happen for worker/dorker that spawn a [Smoucher] on it's own process.
  /// e.g. [Importer]
  ///
  /// For the main [Smoucher], this flag must be false.
  bool _isAuxiliary;

  Smoucher(this._boss, [this._logger]) {
    _boss.onMessage.listen(_handleBoss);
  }

  void _handleBoss(data) async {
    final request = Request.fromPayload(data);
    _log(request.toString(), header: 'Smoucher processing request');

    switch (SmoucherCommand.valueOf(request.command)) {
      case SmoucherCommand.init:
        _init(request);
        break;
      case SmoucherCommand.get:
        _get(request);
        break;
      case SmoucherCommand.allDocs:
        _allDocs(request);
        break;
      case SmoucherCommand.createIndex:
        _createIndex(request);
        break;
      case SmoucherCommand.find:
        _find(request);
        break;
      case SmoucherCommand.put:
        _put(request);
        break;
      case SmoucherCommand.bulkDocs:
        _bulkDocs(request);
        break;
      case SmoucherCommand.revert:
        _revert(request);
        break;
      case SmoucherCommand.smodCreateIndex:
        _smodCreateIndex(request);
        break;
      case SmoucherCommand.smodFind:
        _smodFind(request);
        break;
      case SmoucherCommand.smodAllDocs:
        _smodAllDocs(request);
        break;
      case SmoucherCommand.smodBulkGet:
        _smodBulkGet(request);
        break;
      case SmoucherCommand.smodReset:
        _smodReset(request);
        break;
      case SmoucherCommand.smodPull:
        _smodPull(request);
        break;
      case SmoucherCommand.smodPush:
        _smodPush(request);
        break;
      case SmoucherCommand.resolve:
        _resolve(request);
        break;
      case SmoucherCommand.bail:
        _bail(request);
        break;
      case SmoucherCommand.query:
        _query(request);
        break;
      case SmoucherCommand.destroy:
        _destroy(request);
        break;
      default:
        assert(false, 'havent implement command ${request.command}');
        break;
    }
  }

  /// Boot up all the PouchDB.
  ///
  /// Setting `request.params['isAuxiliary']` to true make this instance the main one,
  /// it will flush the [_localDb], keeping it clean with just local changes.
  void _init(Request request) async {
    final remoteDbName = request.params['dbName'];
    final username = request.params['username'];
    final password = request.params['password'];
    _isAuxiliary = request.params['isAuxiliary'] ?? false;
    _localDbName = request.params['localDbName'];
    _smodDbName = request.params['smodDbName'];
    _smodRestoreDbName = 'restore-$_smodDbName';

    CreateOption createOption;
    if (username != null && password != null) {
      createOption = CreateOption(
          auth: AuthOption(username: username, password: password));
    }

    await _mutex.acquireWrite();
    try {
      _remoteDb = PouchDB(remoteDbName, createOption);
      _remoteDb.changes(ChangeOption(live: true, since: 'now'))
        ..on('change', allowInterop(_handleRemoteChanges));

      _smodDb = PouchDB(_smodDbName);
      _localDb = PouchDB(_localDbName);

      if (!_isAuxiliary) {
        await _flushLocalDb();
      }

      Respond.to(request)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Any changes in the remote DB will invoke this.
  ///
  /// The [_] and [__] arguments are not use here, not even used by PouchDB.js itself.
  /// But we need this function signature to get interop correctly into PouchDB.js.
  void _handleRemoteChanges(changes, _, __) async {
    await _mutex.acquireWrite();
    try {
      //TODO: If we flushed and no change ever since, we should not flush again
      //TODO: Cleaning up on any change break user will current opened document that going to be edit.
      await _flushLocalDb();
    } catch (e) {
      _handleError(e);
    } finally {
      _mutex.release();
    }
  }

  /// This will clean up the [_localDb], and repopulate it with [_smodDB]'s documents.
  ///
  /// Main purpose is removing reverted/tangling document from [_localDB].
  /// Get it to the cleanest state with only local changes. Remote documents will be
  /// re-replicate again when needed.
  ///
  /// Happens in 3 situations:
  /// - first initialization when the application started.
  /// - After any Revert operation.
  /// - After any remote changes.
  Future _flushLocalDb() async {
    // Clear the founded cache, force all `find` to hit the remote now.
    _upToDateFinds.clear();

    // Reset the local DB
    if (_localDb != null) {
      await promiseToFuture(_localDb.destroy());
    }
    _localDb = PouchDB(_localDbName);

    // Base on all the changes registered in [_smodDb],
    // replicate only those changed document from [_smodDb] to [_localDb]
    final changedDocIds = await _getAllChangedIds();
    await _replicateIds(changedDocIds, _smodDb, _localDb);

    // Replicate remote DB's design document into localDB
    //TODO: This shall not change too often to warrant a replication from the remote for every flush.
    await promiseToFuture(_remoteDb.replicate.to(_localDb,
        ReplicateOption(filter: allowInterop((doc, _) {
      final ddoc = jsToDart(doc);
      return (ddoc['_id'] as String).startsWith('_design/');
    }))));
  }

  /// This will clean up all the reverted document from [_smodDb], keep only relevant chanes.
  ///
  /// Once a document is stored inside a PouchDB, it is in. There's no good way to remove it.
  /// For reverted document, we want it to vanish, with no trace. In order to preserve the revisions.
  /// The only way to achieve that in PouchDB is destroy the DB. And that's what we do here.
  ///
  /// We backup all relevant file to a staging DB, i.e. all except document with 'smod_reverted'.
  /// Destroy the original, recreate a new one with the same name, replicate all from staging DB.
  Future _flushSmodDB() async {
    final stagingSmodName = '$_smodDbName-staging';
    final stagingSmod = PouchDB(stagingSmodName);

    await promiseToFuture(_smodDb.replicate.to(stagingSmod,
        ReplicateOption(filter: allowInterop((doc, _) {
      // The second parameter `_` in filter() is important for dart2js, in order for it to find the correct function.
      final ddoc = jsToDart(doc);
      return (!ddoc['smod_reverted'] ?? true);
    }))));

    await promiseToFuture(_smodDb.destroy());
    _smodDb = PouchDB(_smodDbName);

    await promiseToFuture(stagingSmod.replicate.to(_smodDb));
    await promiseToFuture(stagingSmod.destroy());
  }

  /// Return the most relevant version of the document.
  ///
  /// If there is one in [_localDb], always return that because:
  /// 1. Either it is replicated from the remote and no change ever since,
  /// 2. or it is replicated from [_smodDb], which is the local changes.
  ///    And we want that local changes to be returned.
  ///
  /// If there is none in local. Replicate it from the [_remoteDb].
  /// Which is done by calling [_replicateMissingId()].
  void _get(Request request) async {
    final docId = request.params;

    await _mutex.acquireRead();
    try {
      await _replicateMissingId([docId]);

      _log(docId.toString(), header: 'LOCAL GET');
      final doc = jsToDart(await promiseToFuture(
          _localDb.get(request.params, GetOption(conflicts: true))));

      Respond.to(request, params: doc)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Same as [_get()] but for multiple documents.
  void _allDocs(Request request) async {
    final docIds = request.params['keys'];

    await _mutex.acquireRead();
    try {
      await _replicateMissingId(docIds);

      _log(docIds.toString(), header: 'LOCAL ALLDOCS');
      var result = jsToDart(
          await promiseToFuture(_localDb.allDocs(dartToJs(request.params))));
      Respond.to(request, params: result)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Create find index for both remote and local
  //TODO: What happen if we flush the DB? How can be know index always created by user?
  void _createIndex(Request request) async {
    await _mutex.acquireWrite();
    try {
      await promiseToFuture(_localDb.createIndex(dartToJs(request.params)));

      Respond.to(request)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Perform the CouchDB/PouchDB find() and return the most relevant result.
  ///
  /// When a find is requested. We perform the operation onto [_remoteDb], but only
  /// return the document Id. We then replicate these document from [_remoteDb],
  /// if we cannot find them in the [_localDb].
  ///
  /// Finally we perform the `find()` on [_localDb],
  /// which should have douments:
  /// 1. Either is replicated from [_remoteDb]
  /// 2. Or local changes from [_smodDb]
  ///
  /// We skip checking with the [_remoteDb] if the query was performed before,
  /// and there is no changes from the remote.
  void _find(Request request) async {
    await _mutex.acquireRead();
    try {
      final selector = request.params;
      final selectorHash = selector.toString().hashCode;

      if (_upToDateFinds.add(selectorHash)) {
        // Only get id from remote
        final remoteSelector = Map<String, dynamic>.from(selector);
        remoteSelector['fields'] = ['_id'];

        _log(remoteSelector.toString(), header: 'REMOTE FIND');
        final remoteFindResults = jsToDart(
            await promiseToFuture(_remoteDb.find(dartToJs(remoteSelector))));

        // Instead of returning the full document from previous find, we take only Id.
        // And opts to replicate the doucment if only we can't find it locally.
        final docIds =
            remoteFindResults['docs'].map((doc) => doc['_id']).toList();
        await _replicateMissingId(docIds);
      }

      _log(selector.toString(), header: 'LOCAL FIND');

      final result =
          jsToDart(await promiseToFuture(_localDb.find(dartToJs(selector))));

      _log(result.toString(), header: 'LOCAL FIND RESULT');

      Respond.to(request, params: result)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Replicate any document in [docIds] that does not exist in [_localDb] from [_remoteDb].
  Future _replicateMissingId(docIds) async {
    // Find out which docs are missing locally
    final idOnlyQuery = {
      'keys': docIds,
    };
    var allDocResult = jsToDart(
        await promiseToFuture(_localDb.allDocs(dartToJs(idOnlyQuery))));
    final rows = allDocResult['rows'];
    final missingIds = [];
    for (var i = 0; i < rows.length; i++) {
      final row = rows[i];

      // Only missing doc will be error, 'deleted' doc is just... deleted.
      if (row['error'] != null) {
        missingIds.add(docIds[i]);
      }
    }

    return _replicateIds(missingIds, _remoteDb, _localDb, isRemoteCall: true);
  }

  /// Submit a document change. These changes is known as "local changes".
  ///
  /// All local changes will be recorded in [_smodDb]. The recording is a pair.
  /// First item is the full document itself, with a new `_rev`.
  /// Second item is the [ChangeDoc] which has the history of changes tied to first item.
  /// The pair will be generated, or updated by this method.
  ///
  /// If the changes is not a Creation, i.e. new document.
  /// A [ChangeDoc] for the relevant document will be acquire first.
  /// If such [ChangeDoc] cannot be found, it is a new changes performing on a document existed in the [_remoteDb].
  /// In that case, the original document need to be replicated first, from the [_localDb].
  ///
  /// Why not from [_remoteDb]? To save bandwidth. And if we able to submit a change,
  /// that document must have returned from [_localDb] in the first place.
  ///
  /// Why we must replicate? It is a errornous operation to put a document with a revision
  /// into PouchDB. Thus, we must replicate the previous version of the document before
  /// we apply the new changes on top. Even if we want to delete it.
  ///
  /// Once we have the document in [_smodDb], we perform the `put()` for real.
  ///
  /// And then create/update the [ChangeDoc] accordingly.
  ///
  /// Last step is replicate the local changes back to [_localDb].
  /// This ensure that local changes will be return in place of original remote documents.
  /// Also new document will show up in [_find()], and deleted document will not show up.
  void _put(Request request) async {
    await _mutex.acquireWrite();
    try {
      final Map<String, dynamic> puttingObject = request.params;
      final id = puttingObject['_id'];
      final type = puttingObject['type'];
      final isCreation = !puttingObject.containsKey('_rev');
      final isDeletion = !isCreation && (puttingObject['_deleted'] ?? false);

      ChangeDoc changeDoc;
      if (!isCreation) {
        changeDoc = await _findChangeDocForDoc(id, _smodDb);
        if (changeDoc == null) {
          // Missing ChangeDoc means new local changes applying to remote document
          await _replicateIds([id], _localDb, _smodDb);
          await _addReplicatedChange(id, type, _smodDb);

          changeDoc = await _findChangeDocForDoc(id, _smodDb);
        } else if (changeDoc.lastOperation == OperationType.create &&
            isDeletion) {
          // Deleting a creation, it is a Revert operation.
          await _revertByChangeDocIds([changeDoc.id]);

          // We bail out after the revert is done.
          Respond.to(request)..sendThrough(_boss);

          await _broadcastChanges();
          return;
        }
      }

      //TODO: This can fail
      final result = jsToDart(
          await promiseToFuture(_smodDb.put(dartToJs(request.params))));

      final newRev = result['rev'];
      puttingObject['_rev'] = newRev;

      if (isCreation) {
        await _addCreatedChange(id, type, newRev, puttingObject, _smodDb);
      } else if (isDeletion) {
        await _addDeletedChange(changeDoc, newRev, puttingObject, _smodDb);
      } else {
        await _addUpdatedChange(changeDoc, newRev, puttingObject, _smodDb);
      }

      //TODO: Should we verify the result?
      await _replicateIds([id], _smodDb, _localDb);

      Respond.to(request, params: result)..sendThrough(_boss);

      await _broadcastChanges();
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Same as [_put()] but for multiple document changes.
  ///
  /// It appears to be more complex because this method anticipate a mixed
  /// list of documents with different type of operations (create, update, delete).
  void _bulkDocs(Request request) async {
    await _mutex.acquireWrite();
    try {
      final List puttingObjects = request.params;
      final processingObjects = [];
      final creatingIds = [];
      final updateingIds = [];
      final deletingIds = [];
      final replicatingId = [];
      final revertingChanges = [];
      final changeDocs = Map<String, ChangeDoc>();

      // Divide and conquer for create, update, delete
      for (var doc in puttingObjects) {
        final id = doc['_id'];
        final type = doc['type'];
        final isCreation = !doc.containsKey('_rev');
        if (isCreation) {
          creatingIds.add(DocIdType(id, type));
        } else {
          final isDeletion = doc['_deleted'] ?? false;
          final changeDoc = await _findChangeDocForDoc(id, _smodDb);
          if (changeDoc == null) {
            replicatingId.add(DocIdType(id, type));
          } else if (changeDoc.lastOperation == OperationType.create &&
              isDeletion) {
            // If we deleting a creation, we actually reverting the creation
            revertingChanges.add(changeDoc.id);

            // skip the loop here, revert take other operations
            continue;
          } else {
            changeDocs[id] = changeDoc;
          }

          if (isDeletion) {
            deletingIds.add(id);
          } else {
            updateingIds.add(id);
          }
        }

        // Add the doc as object to use in bulkDocs
        processingObjects.add(doc);
      }

      // Replicate for new changes
      if (replicatingId.isNotEmpty) {
        await _replicateIds(
            replicatingId.map((d) => d.docId).toList(), _localDb, _smodDb);
        for (var docIdType in replicatingId) {
          //TODO: Bad, just bad.
          await _addReplicatedChange(
              docIdType.docId, docIdType.docType, _smodDb);
          final updatedChangeDoc =
              await _findChangeDocForDoc(docIdType.docId, _smodDb);
          changeDocs[docIdType.docId] = updatedChangeDoc;
        }
      }

      final results = jsToDart(
          await promiseToFuture(_smodDb.bulkDocs(dartToJs(processingObjects))));

      for (var i = 0; i < results.length; i++) {
        final processedObject = processingObjects[i];
        final result = results[i];
        assert(!result.containsKey('error'),
            'Bulk operation on smod DB failed for $result');

        final id = result['id'];
        final newRev = result['rev'];
        final docIdType =
            creatingIds.firstWhere((d) => d.docId == id, orElse: () => null);
        if (docIdType != null) {
          await _addCreatedChange(docIdType.docId, docIdType.docType, newRev,
              processedObject, _smodDb);
        } else {
          final changeDoc = changeDocs[id];
          if (updateingIds.contains(id)) {
            await _addUpdatedChange(
                changeDoc, newRev, processedObject, _smodDb);
          } else if (deletingIds.contains(id)) {
            await _addDeletedChange(
                changeDoc, newRev, processedObject, _smodDb);
          } else {
            assert(false, '$id is not in either 3 of the operations, alien?');
          }
        }
      }

      //TODO: Should we verify the result?
      await _replicateIds([
        ...creatingIds.map((docIdType) => docIdType.docId).toList(),
        ...updateingIds,
        ...deletingIds
      ], _smodDb, _localDb);

      // Revert all the "new document" that have been deleted.
      if (revertingChanges.isNotEmpty) {
        await _revertByChangeDocIds(revertingChanges);
      }

      Respond.to(request, params: results)..sendThrough(_boss);

      await _broadcastChanges();
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// [docIds] can be empty, in that case, we bail directly.
  Future _replicateIds(List docIds, PouchDB source, PouchDB destination,
      {bool isRemoteCall = false}) {
    if (docIds.isEmpty) {
      return null;
    }

    final remotePrefix = isRemoteCall ? 'REMOTE' : 'LOCAL';
    final replicationDirection = '${source.name} -> ${destination.name}';
    _log(docIds.toString(),
        header: '$remotePrefix REPLICATE: $replicationDirection');

    return promiseToFuture(
        source.replicate.to(destination, ReplicateOption(doc_ids: docIds)));
  }

  Future _addReplicatedChange(String id, String type, PouchDB smodDb) async {
    //TODO: This might fail
    final replicatedDoc =
        jsToSerializedString(await promiseToFuture(smodDb.get(id)));

    final changeDoc = ChangeDoc((builder) => builder
      ..id = _uuid.v4()
      ..docId = id
      ..docType = type
      ..lastOperation = OperationType.replicate
      ..changes.add(Change((changeBuilder) => changeBuilder
        ..operationType = OperationType.replicate
        ..serializedDoc = replicatedDoc)));

    return promiseToFuture(
        smodDb.put(dartToJs(standardSerializers.serialize(changeDoc))));
  }

  Future _addCreatedChange(String id, String type, String rev, dynamic document,
      PouchDB smodDb) async {
    final changeDoc = ChangeDoc((builder) => builder
      ..id = _uuid.v4()
      ..docId = id
      ..docType = type
      ..lastOperation = OperationType.create
      ..changes.add(Change((changeBuilder) => changeBuilder
        ..operationType = OperationType.create
        ..rev = rev
        ..serializedDoc = json.encode(document))));

    return promiseToFuture(
        smodDb.put(dartToJs(standardSerializers.serialize(changeDoc))));
  }

  Future _addUpdatedChange(
      ChangeDoc changeDoc, String rev, dynamic document, PouchDB smodDb) async {
    final operationType =
        changeDoc.changes.last.operationType == OperationType.create
            ? OperationType.create
            : OperationType.update;
    final updatedChangeDoc = changeDoc.rebuild((builder) => builder
      ..lastOperation = operationType
      ..changes.add(Change((changeBuilder) => changeBuilder
        ..operationType = operationType
        ..rev = rev
        ..serializedDoc = json.encode(document))));

    return promiseToFuture(
        smodDb.put(dartToJs(standardSerializers.serialize(updatedChangeDoc))));
  }

  Future _addDeletedChange(
      ChangeDoc changeDoc, String rev, dynamic document, PouchDB smodDb) async {
    final updatedChangeDoc = changeDoc.rebuild((builder) => builder
      ..lastOperation = OperationType.delete
      ..changes.add(Change((changeBuilder) => changeBuilder
        ..operationType = OperationType.delete
        ..rev = rev
        ..serializedDoc = json.encode(document))));

    return promiseToFuture(
        smodDb.put(dartToJs(standardSerializers.serialize(updatedChangeDoc))));
  }

  Future _addConflictedChange(
      ChangeDoc changeDoc, String rev, dynamic document) async {
    final updatedChangeDoc = changeDoc.rebuild((builder) => builder
      ..lastOperation = OperationType.conflict
      ..changes.add(Change((changeBuilder) => changeBuilder
        ..operationType = OperationType.conflict
        ..rev = rev
        ..serializedDoc = document != null ? json.encode(document) : null)));

    return promiseToFuture(
        _smodDb.put(dartToJs(standardSerializers.serialize(updatedChangeDoc))));
  }

  Future _addResolvedChange(
      ChangeDoc changeDoc, String rev, dynamic document) async {
    final updatedChangeDoc = changeDoc.rebuild((builder) => builder
      ..lastOperation = OperationType.resolved
      ..changes.add(Change((changeBuilder) => changeBuilder
        ..operationType = OperationType.resolved
        ..rev = rev
        ..serializedDoc = json.encode(document))));

    return promiseToFuture(
        _smodDb.put(dartToJs(standardSerializers.serialize(updatedChangeDoc))));
  }

  /// Find the [ChangeDoc] with the document [docId].
  Future<ChangeDoc> _findChangeDocForDoc(String docId, PouchDB smodDb) async {
    final query = {
      'selector': {'type': ChangeDoc.serializer.wireName, 'docId': docId}
    };

    final results =
        jsToDart(await promiseToFuture(smodDb.find(dartToJs(query))));
    final List docs = results['docs'];

    if (docs?.isEmpty ?? true) {
      // Not found, just return null
      return null;
    }

    return standardSerializers.deserialize(docs[0]);
  }

  /// Return all changed document's ID base on all current `ChangeDoc`
  Future<List> _getAllChangedIds() async {
    //TODO: Should we limit the count eventually? Now is hardcoded to 250
    final allChangeQuery = {
      'selector': {'type': ChangeDoc.serializer.wireName},
      'fields': ['docId'],
      'limit': 250
    };
    final changeDocFindResults =
        jsToDart(await promiseToFuture(_smodDb.find(dartToJs(allChangeQuery))));
    return changeDocFindResults['docs'].map((doc) => doc['docId']).toList();
  }

  Future _broadcastChanges() async {
    //TODO: This cause event sent back before the Request Respond loop done. Is this a problem?
    Respond.sendEvent('changes')..sendThrough(_boss);
  }

  /// Revert by request. [request.params] is assume to be a list of [ChangeDoc] Id.
  void _revert(Request request) async {
    await _mutex.acquireWrite();
    try {
      await _revertByChangeDocIds(request.params);

      Respond.to(request)..sendThrough(_boss);

      await _broadcastChanges();
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Revert by existing [ChangeDoc].
  ///
  /// Because of the nature of PouchDB that never really remove a trace of document.
  /// Revert becomes a big hairly and heavy operation to perform. Here's what we do:
  ///
  /// Based on the [ChangeDoc], fishs out the document. [_bulkGetOf()] is used
  /// for getting deleted document.
  ///
  /// For all documents, flag it with `'smod_reverted'` and then delete it.
  /// For all changeDoc, delete them.
  /// Save these new changes back to [_smodDb].
  ///
  /// Then. Clean up the [_smodDb] with [_flushSmodDB()]. Removing traces of
  /// reverted document and it's [ChangeDoc].
  ///
  /// Lastly, Flush the [_localDb]. Removing traces of reverted document there too.
  Future _revertByChangeDocIds(changeDocIds) async {
    final changeDocsResult =
        await _allDocsOf(_smodDb, keys: changeDocIds, includeDocs: true);
    final changeDocs =
        changeDocsResult['rows'].map((row) => row['doc']).toList();

    // Find the document and create the parameters(Map) that feed directly into _bulkGetOf()
    final idRevPairs = changeDocs.map((changeDoc) {
      final lastChange = changeDoc['changes'].last;
      return {'id': changeDoc['docId'], 'rev': lastChange['rev']};
    }).toList();

    final docsResult = jsToDart(await _bulkGetOf(_smodDb, pairs: idRevPairs));
    //TODO: There are 'error' in place of 'ok' if the get fail
    final docs =
        docsResult['results'].map((row) => row['docs'][0]['ok']).toList();

    //Delete them and mark reverted
    for (var doc in docs) {
      doc['_deleted'] = true;
      doc['smod_reverted'] = true;
    }

    for (var changeDoc in changeDocs) {
      changeDoc['_deleted'] = true;
      changeDoc['smod_reverted'] = true;
    }

    // Put these changes into Smod DB
    await promiseToFuture(_smodDb.bulkDocs(dartToJs([...docs, ...changeDocs])));

    // Clan up smodDb
    await _flushSmodDB();

    // Clan up localDb
    await _flushLocalDb();
  }

  /// A shorthand function to `allDocs` the [db] with [keys].
  Future _allDocsOf(db, {keys, bool includeDocs, bool conflicts}) async {
    final allDocParam = {
      'keys': keys,
      'include_docs': includeDocs,
      'conflicts': conflicts
    };

    return jsToDart(await promiseToFuture(db.allDocs(dartToJs(allDocParam))));
  }

  /// This is used when deleted document is needed.
  ///
  ///  [_allDocsOf()] and [db.allDocs()] do not return deleted document.
  Future _bulkGetOf(db, {pairs}) async {
    final bulkGetParam = {'docs': pairs};

    return promiseToFuture(db.bulkGet(dartToJs(bulkGetParam)));
  }

  void _smodCreateIndex(Request request) async {
    await _mutex.acquireWrite();
    try {
      await promiseToFuture(_smodDb.createIndex(dartToJs(request.params)));
      Respond.to(request)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  void _smodFind(Request request) async {
    await _mutex.acquireRead();
    try {
      var doc = jsToDart(
          await promiseToFuture(_smodDb.find(dartToJs(request.params))));
      Respond.to(request, params: doc)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  void _smodAllDocs(Request request) async {
    await _mutex.acquireRead();
    try {
      var result = jsToDart(
          await promiseToFuture(_smodDb.allDocs(dartToJs(request.params))));
      Respond.to(request, params: result)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  void _smodBulkGet(Request request) async {
    await _mutex.acquireRead();
    try {
      var result = jsToDart(
          await promiseToFuture(_smodDb.bulkGet(dartToJs(request.params))));
      Respond.to(request, params: result)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Clean the [_smodDb] entirely, destroying all local changes.
  ///
  /// Happen when user want to revert all local changes.
  ///
  /// Or during special testing that we wipe clean the local changes
  /// for all the session.
  void _smodReset(Request request) async {
    await _mutex.acquireWrite();
    try {
      await promiseToFuture(_smodDb.destroy());
      _smodDb = PouchDB(_smodDbName);

      // Flush the local DB, to get rid of all previous SMod changes.
      await _flushLocalDb();

      Respond.to(request)..sendThrough(_boss);

      await _broadcastChanges();
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Query both the remote and local DB.
  ///
  /// This combines the results, without merging entries with same keys, and pass it back.
  /// It is up to the user of this query to decide what to do with the identical key entries.
  void _query(Request request) async {
    final url = request.params['url'];
    final params = request.params['params'];
    final remoteResults =
        jsToDart(await promiseToFuture(_remoteDb.query(url, dartToJs(params))));
    final localResults =
        jsToDart(await promiseToFuture(_localDb.query(url, dartToJs(params))));

    // Add all local results into remote's
    remoteResults['rows'].addAll(localResults['rows']);

    Respond.to(request, params: remoteResults)..sendThrough(_boss);
  }

  void _smodPull(Request request) async {
    try {
      await _mutex.acquireWrite();

      // Create restore DB before pull
      var restoreDb = PouchDB(_smodRestoreDbName);
      // Clean it up
      await promiseToFuture(restoreDb.destroy());
      restoreDb = PouchDB(_smodRestoreDbName);

      // Replicate everything from smod DB to restore
      await promiseToFuture(_smodDb.replicate.to(restoreDb));

      // Find all changes
      final docIds = await _getAllChangedIds();

      if (docIds.isNotEmpty) {
        // Replicate the document of the same ID from remote into smod DB
        // With this replication result, we should be able to
        final replicationResult = jsToDart(await _replicateIds(
            docIds, _remoteDb, _smodDb,
            isRemoteCall: true));

        final int potentialConflictCount =
            replicationResult != null ? replicationResult['docs_written'] : 0;

        if (potentialConflictCount > 0) {
          // Look for conflict
          final allDocWithConflicts = await _allDocsOf(_smodDb,
              keys: docIds, includeDocs: true, conflicts: true);

          _log(beautify(allDocWithConflicts), header: 'All Conflicted doc');

          final rows = allDocWithConflicts['rows'];
          final docsWithoutConflicts = List();
          var addedConflictCount = 0;
          for (final row in rows) {
            final doc = row['doc'];
            final docId = doc['_id'];
            final List conflictIds = doc['_conflicts'];
            if (conflictIds != null) {
              assert(conflictIds.length == 1,
                  'We got more than 1 conflict in sMod DB. Not expected!');

              final changeDoc = await _findChangeDocForDoc(docId, _smodDb);
              assert(changeDoc != null,
                  'Fail to find ChangeDoc for ${docId} when looking for conflict');

              // Get the latest rev of changed document
              final latestRevInSmod = changeDoc.changes.last.rev;
              // Find the remote's change rev
              final conflictedRev = conflictIds.single;

              var remoteDoc;
              if (latestRevInSmod != conflictedRev) {
                remoteDoc = jsToDart(await promiseToFuture(
                    _smodDb.get(docId, GetOption(rev: conflictedRev))));
              } else {
                remoteDoc = doc;
              }

              _log(beautify(remoteDoc), header: 'Conflicted doc');

              await _addConflictedChange(
                  changeDoc, remoteDoc['_rev'], remoteDoc);
              addedConflictCount++;
            } else {
              docsWithoutConflicts.add(doc);
            }
          }

          if (addedConflictCount < potentialConflictCount) {
            for (final doc in docsWithoutConflicts) {
              final docId = doc['_id'];

              // Use `open_revs` to get all open revision for this doc.
              final List leafNodes = jsToDart(await promiseToFuture(
                  _smodDb.get(docId, GetOption(open_revs: 'all', revs: true))));

              _log(beautify(leafNodes), header: 'No Conflic doc');

              // When there are open revisions but passed the conflict test, very likely is a deletion
              // Either the document is deleted locally in this smod, or deleted remotely by other user.
              // Both will not trigger a conflict situation in CouchDB/PouchDB.
              //TODO: Very likely I made a bad assumption here. When merge come into picture we might have more than 2 open revisions.
              if (leafNodes.length > 1) {
                final changeDoc = await _findChangeDocForDoc(docId, _smodDb);
                assert(changeDoc != null,
                    'Fail to find ChangeDoc for ${docId} when looking for conflict');

                if (changeDoc.lastOperation == OperationType.delete) {
                  // We locally deleted this document, remotely someone changed it
                  var changedDoc;
                  for (final node in leafNodes) {
                    final doc = node['ok'];
                    if (doc['_deleted'] == null) {
                      changedDoc = doc;
                    }
                  }
                  _log(beautify(changedDoc),
                      header: 'what are you putting into conflict');
                  await _addConflictedChange(
                      changeDoc, changedDoc['_rev'], changedDoc);
                  addedConflictCount++;
                } else {
                  // Locally changed the doc, but remotely removed
                  var deletedDoc;
                  for (final node in leafNodes) {
                    final doc = node['ok'];
                    if (doc['_deleted'] != null && doc['_deleted']) {
                      deletedDoc = doc;
                    }
                  }
                  await _addConflictedChange(
                      changeDoc, deletedDoc['_rev'], deletedDoc);
                  addedConflictCount++;
                }
              }

              if (addedConflictCount >= potentialConflictCount) {
                break;
              }
            }
          }
        }
      }

      Respond.to(request)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  void _smodPush(Request request) async {
    await _mutex.acquireWrite();

    try {
      // Replicate all documents to remote, except ChangeDoc
      await promiseToFuture(_smodDb.replicate.to(_remoteDb,
          ReplicateOption(filter: allowInterop((doc, _) {
        // The second parameter `_` in filter() is important for dart2js, in order for it to find the correct function.

        final ddoc = jsToDart(doc);
        return ddoc['type'] != ChangeDoc.serializer.wireName;
      }))));

      // Recreate a new smod DB
      await promiseToFuture(_smodDb.destroy());
      _smodDb = PouchDB(_smodDbName);

      await _flushLocalDb();

      Respond.to(request)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  void _resolve(Request request) async {
    await _mutex.acquireWrite();
    try {
      final changeDocId = request.params['changeDocId'];
      final doc = request.params['doc'];

      final changeDoc = standardSerializers.deserialize(
              jsToDart(await promiseToFuture(_smodDb.get(changeDocId))))
          as ChangeDoc;

      // Delete and mark smod_resolved for the original local document
      final localChange =
          changeDoc.changes.elementAt(changeDoc.changes.length - 2);
      final localDoc = jsToDart(await promiseToFuture(
          _smodDb.get(changeDoc.docId, GetOption(rev: localChange.rev))));
      localDoc['_deleted'] = true;
      localDoc['smod_resolved'] = true;

      // _log(beautify(localDoc), header: 'resolved local doc');

      await promiseToFuture(_smodDb.put(dartToJs(localDoc)));

      // _log(beautify(doc), header: 'resolving final doc');

      // Now put the resolved document, based on remote's document
      final result =
          jsToDart(await promiseToFuture(_smodDb.put(dartToJs(doc))));

      final newRev = result['rev'];
      doc['_rev'] = newRev;
      await _addResolvedChange(changeDoc, newRev, doc);

      await _flushLocalDb();

      Respond.to(request)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  void _bail(Request request) async {
    await _mutex.acquireWrite();

    try {
      final restoreDb = PouchDB(_smodRestoreDbName);
      final dbInfo = jsToDart(await promiseToFuture(restoreDb.info()));
      _log(dbInfo.toString(), header: 'restore db info');

      // Recreate a new smod DB
      await promiseToFuture(_smodDb.destroy());
      _smodDb = PouchDB(_smodDbName);

      // Replicate everything from restore DB to smod
      await promiseToFuture(restoreDb.replicate.to(_smodDb));

      await _flushLocalDb();

      Respond.to(request)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Should use by test `tearDown()` only
  void _destroy(Request request) async {
    await _mutex.acquireWrite();

    try {
      _log("Destroying all 3 Database(local, remote, smod)",
          header: 'Smoucher Destroy');
      await Future.wait([
        promiseToFuture(_remoteDb.destroy()),
        promiseToFuture(_localDb.destroy()),
        promiseToFuture(_smodDb.destroy()),
      ]);
      Respond.to(request)..sendThrough(_boss);
    } catch (e) {
      _handleError(e, request);
    } finally {
      _mutex.release();
    }
  }

  /// Differentiate [e] as a Dart error or PouchDB error.
  ///
  /// Convert a Dart error to a Map that similiar to PouchDB's error.
  void _handleError(e, [Request request]) {
    final jsError = jsToDart(e);
    final error = jsError.isEmpty ? {'message': e?.toString()} : jsError;
    _log('${request.command}: $error', header: 'exception');
    error['request'] = request?.toString();
    Respond.error(request, params: error)..sendThrough(_boss);
  }

  /// Log the message to the [_logger] if it was provided at the beginning.
  void _log(String message, {String header, bool isError = false}) {
    if (_logger?.isActive() ?? false) {
      if (isError) {
        _logger.postMessage.add('!!!ERROR!!!');
      }
      if (header != null) {
        _logger.postMessage.add('== ${header.toUpperCase()} ==');
      }
      _logger.postMessage.add(message);
    }
  }
}
