import 'dart:async';
import 'dart:convert' show json;

import 'package:ana_poucher/poucher.dart';
import 'package:ana_poucher/poucher_service_base.dart';
import 'package:built_value/serializer.dart';
import 'package:dorker/dorker_messaging.dart';
import 'package:dorker/transform_completer.dart';

import 'change_doc.dart';
import 'poucher_command.dart';
import 'poucher_service_base_impl.dart';
import 'serializer.dart';

class SmoucherService extends PoucherServiceBase {
  /// This `Serializers` are passed in from the user of this service.
  ///
  /// It should know how to serialize or deserialize the data store by this.
  final Serializers _serializers;

  SmoucherService(poucherDorker, this._serializers)
      : super(poucherDorker, _serializers);

  @override
  Future init(String dbName,
      {String localDbName,
      String smodDbName,
      String username,
      String password,
      bool isAuxiliary}) {
    final trackingId = 'init';
    final completer = completers[trackingId];
    assert(completer?.isCompleted ?? true);

    if (initialized) {
      return null;
    }

    Request(PoucherCommand.init.name,
        params: {
          'dbName': dbName,
          'localDbName': localDbName,
          'smodDbName': smodDbName,
          'username': username,
          'password': password,
          'isAuxiliary': isAuxiliary
        },
        trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  Future revertAll() {
    final trackingId = 'revert_all';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to remove again');

    Request(SmoucherCommand.smodReset.name, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  Future createIndexForChangeDoc() {
    final trackingId = 'create_index';

    final index = {
      'fields': ['type', 'docType', 'lastOperation', 'docId'],
      'name': 'change_doc_index'
    };

    Request(SmoucherCommand.smodCreateIndex.name,
        params: index, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  Future revertChanges(List<LatestChange> changes) async {
    final trackingId = 'revert_${changes.hashCode}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to find again');

    final changeDocIds = changes.map((change) => change.changeDocId).toList();

    Request(SmoucherCommand.revert.name,
        params: changeDocIds, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  Future<FindResult<LatestChange<T>>> fetchAllChanges<T>(
      {bool includeDoc = false,
      bool includeBase = false,
      bool includeConflict = false,
      int limit,
      int skip}) async {
    final result = await _findChangeDocs(limit: limit, skip: skip);
    final changeDocs = result.results;

    final latestChanges = await _generateLatestChange(changeDocs,
        includeDoc: includeDoc,
        includeBase: includeBase,
        includeConflict: includeConflict);

    final latestChangesResult = FindResult<LatestChange>(latestChanges,
        hasNextPage: result.hasNextPage);

    return latestChangesResult;
  }

  Future<FindResult<LatestChange<T>>> fetchAllConflicts<T>(
      {int limit, int skip}) async {
    final result =
        await _findChangeDocs(limit: limit, skip: skip, conflictOnly: true);
    final changeDocs = result.results;

    final latestChanges = await _generateLatestChange<T>(changeDocs,
        includeDoc: true, includeBase: true, includeConflict: true);

    final latestChangesResult = FindResult<LatestChange<T>>(latestChanges,
        hasNextPage: result.hasNextPage);

    return latestChangesResult;
  }

  Future<FindResult<ChangeDoc>> _findChangeDocs(
      {int limit, int skip, bool conflictOnly = false}) {
    final genericTrackingId = 'find_changeDoc';
    assert(completers.containsKey(genericTrackingId) == false,
        'do not have completer for $genericTrackingId');
    assert(completers[genericTrackingId]?.isCompleted ?? true,
        'completer $genericTrackingId was completed before trying to find again');

    bool doPagination = limit != null;
    int pagesPlusOne;

    if (doPagination) {
      pagesPlusOne = limit + 1;
      limit = pagesPlusOne;
    }

    final query = {
      'selector': {
        'type': ChangeDoc.serializer.wireName,
        // Done on purpose to force a sort on DocId
        'docType': {'\$gt': ''},
        'lastOperation': conflictOnly ? {'\$eq': 'aConflict'} : {'\$gt': ''},
        'docId': {'\$gt': ''}
      },
      if (limit != null) 'limit': limit,
      if (skip != null) 'skip': skip
    };

    Request(SmoucherCommand.smodFind.name,
        params: query, trackingId: genericTrackingId)
      ..sendThrough(poucherDorker);

    return (completers[genericTrackingId] =
            TransformCompleter<FindResult<ChangeDoc>>((payload) {
      final docs = payload['docs'];
      final data = docs.map<ChangeDoc>((doc) =>
          // We use `standardSerializers` here because ChangeDoc is a class defined in this project
          standardSerializers.deserializeWith(ChangeDoc.serializer, doc)).toList();

      /* Remove last item if doing pagination and there's an extra item than requested.
       * If there's no extra item, then there is no next page
       */
      bool hasNextPage = false;
      if ((doPagination) && (pagesPlusOne == data.length)) {
        data.removeAt(data.length - 1);
        hasNextPage = true;
      }

      FindResult findResult = FindResult<ChangeDoc>(data);

      if (doPagination) {
        findResult.hasNextPage = hasNextPage;
      }

      return findResult;
    }))
        .future;
  }

  Future<List<LatestChange<T>>> _generateLatestChange<T>(
    List<ChangeDoc> changeDocs, {
    bool includeDoc = false,
    bool includeBase = false,
    bool includeConflict = false,
  }) async {
    final latestChanges = <LatestChange<T>>[];

    for (var i = 0; i < changeDocs.length; i++) {
      final changeDoc = changeDocs[i];
      final latestChange = LatestChange<T>()
        ..operationType = changeDoc.lastOperation
        ..changeDocId = changeDoc.id
        ..docId = changeDoc.docId
        ..docType = changeDoc.docType;

      if (includeConflict &&
          changeDoc.lastOperation == OperationType.conflict) {
        final baseChange = changeDoc.changes.first;
        final lastLocalChange =
            changeDoc.changes.elementAt(changeDoc.changes.length - 2);
        final conflictedChange = changeDoc.changes.last;

        if (baseChange.operationType != OperationType.create) {
          latestChange.base = _serializers
              .deserialize(json.decode(baseChange.serializedDoc)) as T;
        }
        if (lastLocalChange.serializedDoc != null) {
          latestChange.current = _serializers
              .deserialize(json.decode(lastLocalChange.serializedDoc)) as T;
        }
        if (conflictedChange.serializedDoc != null) {
          latestChange.theirs = _serializers
              .deserialize(json.decode(conflictedChange.serializedDoc)) as T;
        }
      } else {
        if (includeDoc) {
          final lastChange = changeDoc.changes.last;
          latestChange.current = _serializers
              .deserialize(json.decode(lastChange.serializedDoc)) as T;

          if (includeBase) {
            final baseChange = changeDoc.changes.first;
            if (baseChange.operationType != OperationType.create) {
              latestChange.base = _serializers
                  .deserialize(json.decode(baseChange.serializedDoc)) as T;
            }
          }
        }
      }

      latestChanges.add(latestChange);
    }

    return latestChanges;
  }

  Future<int> findChangesCount() {
    final genericTrackingId =
        'find_changesCount_${DateTime.now().microsecondsSinceEpoch}';
    assert(completers.containsKey(genericTrackingId) == false,
        'do not have completer for $genericTrackingId');
    assert(completers[genericTrackingId]?.isCompleted ?? true,
        'completer $genericTrackingId was completed before trying to find again');

    final query = {
      'selector': {'type': ChangeDoc.serializer.wireName},
      'fields': ['_id']
    };

    Request(SmoucherCommand.smodFind.name,
        params: query, trackingId: genericTrackingId)
      ..sendThrough(poucherDorker);

    return (completers[genericTrackingId] = TransformCompleter<int>((payload) {
      final docs = payload['docs'];
      return docs.length;
    }))
        .future;
  }

  Future<int> findConflictCount() {
    final genericTrackingId =
        'find_conflictCount_${DateTime.now().microsecondsSinceEpoch}';
    assert(completers.containsKey(genericTrackingId) == false,
        'do not have completer for $genericTrackingId');
    assert(completers[genericTrackingId]?.isCompleted ?? true,
        'completer $genericTrackingId was completed before trying to find again');

    final query = {
      'selector': {
        'type': ChangeDoc.serializer.wireName,
        'lastOperation': {'\$eq': 'aConflict'},
      },
      'fields': ['_id']
    };

    Request(SmoucherCommand.smodFind.name,
        params: query, trackingId: genericTrackingId)
      ..sendThrough(poucherDorker);

    return (completers[genericTrackingId] = TransformCompleter<int>((payload) {
      final docs = payload['docs'];
      return docs.length;
    }))
        .future;
  }

  // TODO: Need test coverage
  Future<int> findResolvedCount() {
    final genericTrackingId =
        'find_resolveCount_${DateTime.now().microsecondsSinceEpoch}';
    assert(completers.containsKey(genericTrackingId) == false,
        'do not have completer for $genericTrackingId');
    assert(completers[genericTrackingId]?.isCompleted ?? true,
        'completer $genericTrackingId was completed before trying to find again');

    final query = {
      'selector': {
        'type': ChangeDoc.serializer.wireName,
        'lastOperation': {'\$eq': 'adResolved'},
      },
      'fields': ['_id']
    };
    Request(SmoucherCommand.smodFind.name,
        params: query, trackingId: genericTrackingId)
      ..sendThrough(poucherDorker);

    return (completers[genericTrackingId] = TransformCompleter<int>((payload) {
      final docs = payload['docs'];
      return docs.length;
    }))
        .future;
  }

  // pull() corresponds to #Sync with Main DB (7) / Selective Replication
  Future pull() async {
    final trackingId = 'pull_${DateTime.now().microsecondsSinceEpoch}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to find again');

    Request(SmoucherCommand.smodPull.name, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  Future resolve(LatestChange conflict, dynamic document) {
    final trackingId = 'resolve_${conflict.changeDocId}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to find again');

    // Verify there is a conflict in the first place
    assert(conflict.operationType == OperationType.conflict,
        'Should resolve conflict only. But it was a ${conflict.operationType}');

    // Verify the resolving document is base on remote's document
    final theirRev = conflict.theirs?.rev;
    if (document.rev != theirRev) {
      throw BadRequestException(
          'Should resolve base on document from remote DB');
    }

    Request(SmoucherCommand.resolve.name,
        params: {
          'changeDocId': conflict.changeDocId,
          'doc': _serializers.serialize(document)
        },
        trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  Future push() async {
    final trackingId = 'push_${DateTime.now().microsecondsSinceEpoch}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to find again');

    Request(SmoucherCommand.smodPush.name, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  Future bail() async {
    final trackingId = 'bail_${DateTime.now().microsecondsSinceEpoch}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to find again');

    Request(SmoucherCommand.bail.name, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }
}
