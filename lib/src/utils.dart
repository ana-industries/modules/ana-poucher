import 'dart:convert';

import 'js_interop/common.dart';

String jsToSerializedString(dynamic pouchObject) => stringify(pouchObject);
dynamic jsToDart(dynamic pouchObject) => json.decode(stringify(pouchObject));
dynamic dartToJs(dynamic dartObject) => parse(json.encode(dartObject));
