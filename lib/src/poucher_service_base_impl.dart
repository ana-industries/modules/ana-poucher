import 'dart:async' show StreamController;

import 'package:ana_metadata/filter.dart';
import 'package:built_value/serializer.dart';
import 'package:dorker/dorker.dart' show Dorker;
import 'package:dorker/dorker_messaging.dart';
import 'package:dorker/transform_completer.dart';

import 'find_result.dart';
import 'poucher_command.dart';
import 'poucher_exception.dart';
import 'poucher_type.dart';

typedef void mapReduceFunctionTypeDef<T>(T doc);

/// PoucherServiceBase provides future-based asynchonous API that wrap around `Poucher`.
///
/// It is meant to be extend by more specialised data oriented Angular Service in the application.
/// By itself, it is more than enough to handle all usual CRUD and mango query.
///
/// It takes a `poucherDorker`, which should be a `Dorker` wired up with a `Poucher`.
/// It can be a crosslinked `Dorker` or a Web Worker, doesn't matter.
///
/// It then takes a `Serializers` to do all the serialisation.
///
class PoucherServiceBase {
  final Dorker poucherDorker;
  final Serializers _serializers;
  final completers = Map<String, TransformCompleter>();

  final _events = StreamController();
  Stream get events => _events.stream;

  bool initialized = false;

  PoucherServiceBase(this.poucherDorker, this._serializers) {
    poucherDorker.onMessage.listen(_handlePoucherDorker);
  }

  Future init(String dbName, {String username, String password}) {
    final trackingId = 'init';
    final completer = completers[trackingId];
    assert(completer?.isCompleted ?? true);

    if (initialized) {
      return null;
    }

    Request(PoucherCommand.init.name,
        params: {'dbName': dbName, 'username': username, 'password': password},
        trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  /// Fetches a single document with [id].
  ///
  /// Returns a built value of type `T`.
  /// Throws a [NotFoundException] on error.
  /// eg.
  /// ```
  /// var spider = await _service.fetch<Animal>('spider_1');
  /// ```
  Future<T> fetch<T>(String id) {
    final trackingId = 'fetch_${id}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to fetch again');

    Request(PoucherCommand.get.name, params: id, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] =
            TransformCompleter<T>((data) => _serializers.deserialize(data)))
        .future;
  }

  /// Fetches multiple documents given a List of [ids].
  /// Returns a List of built values of type `T`.
  /// eg.
  /// ```
  /// final ids = [
  ///   'spider_1',
  ///   'spider_2'
  /// ];
  ///
  /// final docs = await _service.fetchMultiple<Animal>(ids);
  /// ```
  Future<List<T>> fetchMultiple<T>(List<String> ids) {
    final trackingId = 'fetch_multi_${ids.hashCode}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to fetch multiple again');

    Request(PoucherCommand.allDocs.name,
        params: {
          'include_docs': true,
          'keys': ids,
          'conflicts': true,
        },
        trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter<List<T>>((payload) {
      final docs = payload['rows'];

      return docs.map<T>((doc) {
        if (null == doc['doc']) {
          return null;
        } else {
          return _serializers.deserialize(doc['doc']) as T;
        }
      }).toList();
    }))
        .future;
  }

  /// Fetches multiple document's `rev` fields given a List of [ids].
  ///
  /// Returns a JsLinkedHashMap<String, String> of {id, rev}:
  /// eg.
  /// ```
  /// {spider_1: 1-4c96873101df75bcb640250dc661d118,
  ///  spider_2: 1-2ad422f0e4aeb69a1224cb8a3d4c12b5}
  /// ```
  /// eg use.
  /// ```
  /// final ids = [
  ///   'spider_1',
  ///   'spider_2'
  /// ];
  ///
  /// final docs = await _service.fetchMultipleRevisionOnly(ids);
  /// ```
  Future<Map<String, String>> fetchMultipleRevisionOnly(List<String> ids) {
    final trackingId = 'fetch_multi_${ids.hashCode}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to fetch multiple again');

    Request(PoucherCommand.allDocs.name,
        params: {
          'keys': ids,
        },
        trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] =
            TransformCompleter<Map<String, String>>((payload) {
      final docs = payload['rows'];
      return Map<String, String>.fromIterable(docs,
          key: (entry) => entry['id'].toString(),
          value: (entry) => entry['value']['rev'].toString());
    }))
        .future;
  }

  /// Finds documents given a [selector] filter and the document's type `T`.

  ///
  /// returns a [FindResult]
  ///
  /// eg 1: Find all `_id`'s >= 'dk', sorted by id (default index)
  ///       Note: To sort a field, the field must appear in selector
  /// ```
  /// try {
  ///   final request = {
  ///     'selector': {
  ///       '_id': {'\$gte': 'dk'},
  ///       'type': {'\$eq': 'GameHero'}
  ///     },
  ///     'sort': ['_id']
  ///   };
  ///
  ///   final response = await _service.find<GameHero>(request);
  ///   final results = response.results;
  ///
  /// } catch (e) {
  ///   fail('Error: $e');
  /// }
  /// ```
  /// eg. 2: Sort by name.
  /// ```
  /// try {
  ///   // create a binary tree of name entries
  ///   final index = {
  ///     'index': {'fields': ['name']}
  ///   };
  ///
  ///   await _service.poucherCreateIndex(index);
  ///
  ///   // Using the $gt fails if we don't have all documents with the 'name'
  ///   // field.
  ///   // In this case have to use $gte for null (lowest value for CouchDB
  ///   // collation order)
  ///
  ///   final request = {
  ///     'selector': {
  ///       'name': {'\$gte': null},
  ///       'type': {'\$eq': 'GameHero'},
  ///     },
  ///     'sort': ['name']
  ///   };
  ///
  ///   final response = await _service.find<GameHero>(request);
  ///   final results = response.results;
  /// } catch (e) {
  ///   fail('Error: $e');
  /// }

  /// ```
  /// eg 3: sort by debut > 1990
  /// ```
  /// try {
  ///   // create a binary tree of debut entries
  ///   final index = {
  ///     'index': {'fields': ['debut']}
  ///   };
  ///
  ///   await _service.poucherCreateIndex(index);
  ///
  ///   final request = {
  ///     'selector': {
  ///       'debut': {'\$gt': 1990},
  ///       'type': {'\$eq': 'GameHero'}
  ///     },
  ///     'sort': ['debut']
  ///   };
  ///
  ///   final response = await _service.find<GameHero>(request);
  ///   final results = response.results;
  /// } catch (e) {
  ///   fail('Error: $e');
  /// }
  /// ```
  /// eg. 4 `find()` Mario series only
  /// ```
  /// try {
  ///   final index = {
  ///     'index': {'fields': ['series', 'debut']}
  ///   };
  ///
  ///   await _service.poucherCreateIndex(index);
  ///
  ///   // For databases whose documents do not all have the same fields,
  ///   // make sure to put all fields which appear in sort in selector.
  ///   // In this case, debut needs to be in the selector as well.
  ///   final request = {
  ///     'selector': {
  ///       'series': {'\$eq': 'Mario'},
  ///       'type': {'\$eq': 'GameHero'},
  ///       'debut': {'\$exists': true}
  ///     },
  ///     'sort': [
  ///       {'series': 'desc'},
  ///       {'debut': 'desc'}
  ///     ]
  ///   };
  ///
  ///   final response = await _service.find<GameHero>(request);
  ///
  ///   final results = response.results;
  /// } catch (e) {
  ///   fail('Error: $e');
  /// }
  ///
  /// ```
  /// eg 5 find() with pagination
  ///      provide `limit` in the selector
  /// ```
  /// try {
  ///   // sort by id with pagination
  ///   int pageSize = 5;
  ///   final request = {
  ///     'selector': {
  ///       '_id': {'\$gte': 'dk'},
  ///       'type': {'\$eq': 'GameHero'}
  ///     },
  ///     'limit': pageSize,
  ///     'sort': ['_id']
  ///   };
  ///
  ///   var response;
  ///
  ///   int i = 0;
  ///   bool keepGoing = true;
  ///   while (keepGoing) {
  ///     request['skip'] = pageSize * i; 
  ///     response = await _service.find<GameHero>(request);
  ///
  ///     print('$i. response = $response');
  ///
  ///     keepGoing = response.hasNextPage;
  ///     i++;
  ///   }
  ///    
  /// } catch (e) {
  ///   fail('Error: $e');
  /// }
  /// ```
  Future<FindResult<T>> find<T>(Map<String, dynamic> selector,
      {String trackingId}) {
    final genericTrackingId =
        trackingId ?? 'find_${T.runtimeType}_${selector.hashCode}';
    assert(completers.containsKey(genericTrackingId) == false,
        'do not have completer for $genericTrackingId');
    assert(completers[genericTrackingId]?.isCompleted ?? true,
        'completer $genericTrackingId was completed before trying to find again');

    bool doPagination = selector['limit'] != null;
    int pagesPlusOne;

    if (doPagination) {
      pagesPlusOne = selector['limit'] + 1;
      selector['limit'] = pagesPlusOne;
    }

    Request(PoucherCommand.find.name,
        params: selector, trackingId: genericTrackingId)
      ..sendThrough(poucherDorker);

    return (completers[genericTrackingId] =
            TransformCompleter<FindResult<T>>((payload) {
      final docs = payload['docs'];

      final List<T> data =
          docs.map<T>((doc) => _serializers.deserialize(doc) as T).toList();

      /* Remove last item if doing pagination and there's an extra item than requested.
       * If there's no extra item, then there is no next page
       */
      bool hasNextPage = false;
      if ((doPagination) &&
          (pagesPlusOne == data.length)) {
        data.removeAt(data.length - 1);
        hasNextPage = true;
      }

      FindResult findResult = FindResult<T>(data);

      if (doPagination) {
        findResult.hasNextPage = hasNextPage; 
        // write back the origial value of limit
        selector['limit'] = pagesPlusOne - 1;
      }

      return findResult;
    }))
        .future;
  }

  Map<String, dynamic> generateQueryString(String type, List<Filter> filters,
      {int limit,
      int skip,
      List<Filter> sortingFilters,
      Map<String, dynamic> additionalSelector}) {
    final selector = <String, dynamic>{
      'type': type,
    };
    if (additionalSelector?.isNotEmpty ?? false) {
      selector.addAll(additionalSelector);
    }
    if (filters != null) {
      final propSelector = Map<String, dynamic>();

      filters.forEach((filter) {
        if (filter.value != null) {
          if (filter.type == FilterType.selection) {
            selector[filter.selector] = {'\$eq': filter.value};
          } else if (filter.type == FilterType.number) {
            propSelector[filter.id] = {
              '\$gte': filter.value.item1,
              '\$lte': filter.value.item2
            };
          }
        } else {
          selector[filter.selector] = {'\$gte': null};
        }
      });

      if (propSelector.isNotEmpty) {
        selector['props'] = propSelector;
      }
    }

    var sortingFields;
    if (sortingFilters != null) {
      // PouchDB require all field to be presented in selector if they were used in sort
      bool isValidSort = false;
      if (filters != null && filters.length >= sortingFilters.length) {
        final selectorFields = filters.map((filter) => filter.selector).toSet();
        sortingFields = sortingFilters.map((filter) => filter.selector).toSet();

        isValidSort = selectorFields.containsAll(sortingFields);
      }

      if (!isValidSort) {
        throw BadRequestException(
            'selector must contain all sorting fields. selecting filter: $filters. sorting fitler: $sortingFilters');
      }
    }

    final query = {
      'selector': selector,
      if (limit != null) 'limit': limit,
      if (skip != null) 'skip': skip,
      if (sortingFilters != null) 'sort': sortingFields.toList(),
    };

    return query;
  }

  /// Uploads a document [data] of type `T`.
  ///
  /// Returns the new revision id.
  /// eg 1: Upload an Animal built value
  /// ```
  /// final id = 'bear_1';
  ///
  /// final dog = Animal((builder) => builder
  ///   ..id = id
  ///   ..category = 'Mammal'
  ///   ..isPet = true
  ///   ..props['leg'] = JsonObject(4)
  ///   ..props['tail'] = JsonObject(1));
  ///
  /// try {
  ///   final newRev = await _service.upload<Animal>(dog);
  /// } catch (e) {
  ///   print('Failed to create new doc $dog. $e');
  /// }
  /// ```
  /// eg 2: For use with Map / Reduce quries, please see [query].
  Future<String> upload<T>(T data) {
    final Map<String, dynamic> schemaDoc = _serializers.serialize(data);
    final trackingId = 'upload_${schemaDoc['_id']}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to upload again');

    Request(PoucherCommand.put.name, params: schemaDoc, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] =
            TransformCompleter<String>((d) => d['rev']))
        .future;
  }

  /// Uploads a document [data] of type `T`.
  ///
  /// Returns the full response
  /// eg
  /// ```
  /// final id = 'spider_full_1';
  ///
  /// final spider = Animal((builder) => builder
  ///   ..id = id
  ///   ..category = 'Arachnid'
  ///   ..isPet = true
  ///   ..props['leg'] = JsonObject(4)
  ///   ..props['tail'] = JsonObject(1));
  ///
  /// try {
  ///   final response = await _service.uploadFullResp<Animal>(spider);
  /// } catch (e) {
  ///   print('Failed to create new doc $spider. $e');
  /// }
  /// ```
  /// eg. response
  /// ```
  /// response = {ok: true, id: spider_full_1, rev: 1-49fc55b8e580157606d8b4ff24b26e2b}
  /// ```
  Future uploadFullResp<T>(T data) {
    final Map<String, dynamic> schemaDoc = _serializers.serialize(data);
    final trackingId = 'upload_${schemaDoc['_id']}}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to upload again');

    Request(PoucherCommand.put.name, params: schemaDoc, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter((d) {
      return d;
    }))
        .future;
  }

  /// Bulk create / update documents [data].
  ///
  /// Returns a List of [BulkDocRespond].
  ///
  /// eg. 1: Bulk create documents:
  /// ```
  /// try {
  ///
  ///   final id0 = 'mussel_1';
  ///   final id1 = 'mussel_2';
  ///
  ///   final mussel1 = Animal((builder) => builder
  ///   ..id = id0
  ///   ..category = 'Mollusc'
  ///   ..isPet = false
  ///   ..props['Name'] = JsonObject('apple')
  ///   ..props['Tail'] = JsonObject(0)
  ///   ..props['Leg'] = JsonObject(0));
  ///
  ///   final mussel2 = Animal((builder) => builder
  ///     ..id = id1
  ///     ..category = 'Mollusc'
  ///     ..isPet = false
  ///     ..props['Name'] = JsonObject('banana')
  ///     ..props['Leg'] = JsonObject(0)
  ///     ..props['Tail'] = JsonObject(0));
  ///
  ///   var mussels = [
  ///     mussel1,
  ///     mussel2
  ///   ];
  ///
  ///   var bulkDocResponds = await _service.bulkDocs(mussels);
  ///
  /// } catch (e) {
  ///   fail('Error: $e');
  /// }
  /// ```
  ///
  /// eg 2: Bulk update existing documents:
  /// ```
  /// try {
  ///   // read
  ///   final ids = [
  ///     'spider_4',
  ///     'spider_5'
  ///   ];
  ///
  ///   final docs = await _service.fetchMultiple<Animal>(ids);
  ///
  ///   final spider1New = docs[0].rebuild((builder) => builder
  ///       ..props['Name'] = JsonObject('Cool Wincy')
  ///       ..isPet = true
  ///       ..props['Tail'] = JsonObject(1));
  ///
  ///   final spider2New = docs[1].rebuild((builder) => builder
  ///       ..props['Name'] = JsonObject('Dazed Wincy')
  ///       ..isPet = true
  ///       ..props['Leg'] = JsonObject(16));
  ///
  ///   final newDocs = [
  ///     spider1New,
  ///     spider2New
  ///   ];
  ///
  ///   var bulkDocResponds = await _service.bulkDocs(newDocs);
  /// } catch (e) {
  ///   fail('Error: $e');
  /// }
  /// ```
  ///  eg. 3 Bulk delete
  /// ```
  /// try {
  ///   final id0  = 'spider_4';
  ///   final id1 = 'spider_5';
  ///
  ///   final ids = [id0, id1];
  ///
  ///   var docs = await _service.fetchMultiple<Animal>(ids);
  ///
  ///   final updatedDocs = docs.map((doc) =>
  ///     doc.rebuild((builder) => builder..deleted = true)).toList();
  ///
  ///   // bulk delete
  ///   final bulkDocResponds = await _service.bulkDocs(updatedDocs);
  ///   /*
  ///   [{ok: true, id: spider_4, rev: 2-8d4f27f3d19394633696321b2e1f3067},
  ///    {ok: true, id: spider_5, rev: 2-6c8d91f3539b6ea3bd928f890cc21a8a}]
  ///   */
  /// } catch (e) {
  ///   fail('Error: $e');
  /// }
  /// ```
  Future<List<BulkDocRespond>> bulkDocs<T>(List<T> data) {
    final trackingId = 'bulkdocs-${T}-${data.length}';
    final serializedData =
        data.map((datum) => _serializers.serialize(datum)).toList();

    Request(PoucherCommand.bulkDocs.name,
        params: serializedData, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] =
            TransformCompleter<List<BulkDocRespond>>((payload) {
      return payload.map<BulkDocRespond>((entry) {
        bool ok;

        if (entry['ok'] ?? false) {
          // use the success doc version
          ok = true;
        } else if (entry['error'] ?? false) {
          // use the error doc version
          ok = false;
        }

        return BulkDocRespond(ok: ok, id: entry['id'], rev: entry['rev']);
      }).toList();
    }))
        .future;
  }

  /// Removes an existing document [data].
  ///
  /// Returns null on success.
  /// eg.
  /// ```
  /// try {
  ///   final id = 'spider_3';
  ///
  ///   final spider = await _service.fetch<Animal>(id);
  ///   final response = await _service.remove(spider);
  ///
  /// } catch (e) {
  ///   print('Failed to get existing doc $e');
  /// }
  /// ```
  Future remove<T>(T data) {
    Map<String, dynamic> schemaDoc = _serializers.serialize(data);

    schemaDoc['_deleted'] = true;

    final trackingId = 'remove_${schemaDoc['_id']}}';
    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to remove again');

    Request(PoucherCommand.put.name, params: schemaDoc, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  /// Creates an index of fields for efficient searching in O(log n)
  ///
  /// This is needed for all calls to find<T>() that isn't using the
  /// default `_id` index.
  ///
  /// eg.
  /// ```
  /// // create a binary tree of name entries
  /// final index = {
  ///   'index': {'fields': ['name']}
  /// };
  ///
  /// await _service.poucherCreateIndex(index);
  /// ```
  Future createIndex<T>(String indexName, List<String> fields) {
    final trackingId = 'createIndex_$indexName-${fields.hashCode}';

    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to find again');

    final index = {'fields': fields, 'name': indexName};

    Request(PoucherCommand.createIndex.name,
        params: index, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter()).future;
  }

  /// Map / Reduce query using an existing design document [url] and
  /// [queryParams].
  ///
  /// Returns the results of the query.
  ///
  /// eg:
  /// ```
  /// try {
  ///
  ///   final ddocSerialized = json.decode('''
  ///     {
  ///     "_id": "_design/my_index",
  ///     "views": {
  ///       "viewName": {
  ///         "map": "function mapFun(doc) {emit(doc.name);}"
  ///       }
  ///     }
  ///   }'''
  ///   );
  ///
  ///   final ddoc = standardSerializers.deserializeWith(DesignDocument.serializer, ddocSerialized);
  ///
  ///   final newRev = await _service.upload<DesignDocument>(ddoc);
  ///
  ///   final queryOptions = {
  ///     'key': 'Captain Falcon',
  ///     'include_docs': true
  ///   };
  ///
  ///   final gameHerosFromService = await _service.query(
  ///     'my_index/viewName',
  ///     queryParams
  ///   );
  /// } catch (e) {
  ///   fail('Error: $e');
  /// }
  Future<List<T>> query<T>(String url, Map<String, dynamic> queryOptions) {
    /// ```
    final trackingId = 'query_${T.runtimeType}_${queryOptions.hashCode}';

    assert(completers.containsKey(trackingId) == false,
        'do not have completer for $trackingId');
    assert(completers[trackingId]?.isCompleted ?? true,
        'completer $trackingId was completed before trying to fetch again');

    Request(PoucherCommand.query.name,
        params: {'url': url, 'params': queryOptions}, trackingId: trackingId)
      ..sendThrough(poucherDorker);

    return (completers[trackingId] = TransformCompleter<List<T>>((payload) {
      final docs = payload['rows'];

      // If `T` is not defined, we assumed user is not interested in casting it. We past the original list back.
      if (T == dynamic) {
        return docs;
      }

      return docs.map<T>((doc) {
        if (null == doc['doc']) {
          return null;
        } else {
          return _serializers.deserialize(doc['doc']) as T;
        }
      }).toList();
    }))
        .future;
  }

  Future destroy() {
    final trackingId = 'destroy';

    Request(PoucherCommand.destroy.name, trackingId: trackingId)
      ..sendThrough(poucherDorker);
    return (completers[trackingId] = TransformCompleter()).future;
  }

  /// Call this to trigger a "changes" event, identically to the one `Poucher` or `Smoucher`
  /// will trigger when user perform a write operation.
  ///
  /// This is neccesarily especially for other web worker process that spawn
  /// their own `PoucherService`. In this case, main application's `PoucherService` will
  /// not get the event. Thus, we need a way to tell the main's service about the changes.
  ///
  /// This is useful for displaying the total changes in sMod, for the notifcation.
  ///
  /// Before we have shared web worker working for our Poucher/Smoucher. This is needed.
  void explicitTriggerChangesEvent() {
    _events.add("changes");
  }

  void _handlePoucherDorker(data) {
    final respond = Respond.fromPayload(data);

    if (respond.isEvent) {
      _events.add(respond.params);
    } else {
      final completer = completers[respond.trackingId];
      assert(
          completer != null, 'completer ${respond.trackingId} does not exisst');
      assert(completer.isCompleted == false,
          'completer ${respond.trackingId} should not be completed');

      if (respond.isError) {
        final Map<String, dynamic> err = respond.params;

        var exception = Exception('undefined error: $err');

        if (err.containsKey('status')) {
          final status = err['status'];
          final message =
              'Message:${err['message']}. Request:${err['request']}';
          switch (status) {
            case 400:
              exception = BadRequestException(message);
              break;
            case 404:
              exception = NotFoundException(message);
              break;
            case 409:
              exception = ConflictException(message);
              break;
            default:
              print('Data service unhandled error: $err');
          }
        }

        completer.completeError(exception);
      } else {
        completer.complete(respond.params);
      }
      completers.remove(respond.trackingId);
    }
  }
}
