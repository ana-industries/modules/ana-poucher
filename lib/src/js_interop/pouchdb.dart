@JS()
library pouchdb;

import 'package:js/js.dart' show anonymous, JS;

import 'promise.dart';
export 'promise.dart';

@JS()
@anonymous
class Response {
  external String get ok;
  external String get id;
  external String get rev;
}

@JS()
@anonymous
abstract class AuthOption {
  external String get username;
  external String get password;
  external factory AuthOption({String username, String password});
}

@JS()
@anonymous
abstract class CreateOption {
  external AuthOption get auth;
  external factory CreateOption({AuthOption auth});
}

@JS()
@anonymous
abstract class ReplicateOption {
  external List get doc_ids;
  external dynamic get filter;
  external dynamic get selector;
  external factory ReplicateOption(
      {List doc_ids, dynamic filter, dynamic selector});
}

@JS()
@anonymous
abstract class ChangeOption {
  external bool get live;
  external bool get conflicts;
  external bool get include_docs;
  external dynamic get since;
  external factory ChangeOption(
      {bool live, bool conflicts, bool include_docs, dynamic since});
}

@JS()
@anonymous
abstract class GetOption {
  external bool get conflicts;
  external String get rev;
  external String get open_revs;
  external bool get revs;
  external bool get revs_info;
  external factory GetOption(
      {String rev,
      String open_revs,
      bool conflicts,
      bool revs,
      bool revs_info});
}

@JS()
abstract class ReplicateFunc {
  external Promise to(target, [ReplicateOption option]);
  external Promise from(source, [ReplicateOption option]);
}

@JS()
@anonymous
class EventEmitter {
  external int getMaxListeners();
  external int listenerCount(name);
  external List eventNames();
  external bool emit(name, obj);
  external void addListener(name, callback);
  external void on(name, callback);
}

@JS('PouchDB')
class PouchDB {
  external String get name;
  external Promise info();
  external EventEmitter changes(ChangeOption option);
  external Promise<Response> put(obj);
  external Promise bulkDocs(obj);
  external Promise bulkGet(obj);
  external Promise get(obj, [GetOption option]);
  external Promise allDocs(obj);
  external Promise remove(obj);
  external Promise createIndex(obj);
  external Promise find(obj);
  external Promise query(url, obj);
  external Promise destroy();
  external ReplicateFunc get replicate;
  external PouchDB(String name, [CreateOption option]);
}

@JS('PouchDB.replicate')
external Promise replicate(source, target);
