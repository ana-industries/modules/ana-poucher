@JS()
library js_common;

import 'package:js/js.dart' show JS;
import 'package:js/js_util.dart' show getProperty, setProperty;

@JS("JSON.stringify")
external String stringify(obj);

@JS("JSON.parse")
external dynamic parse(obj);

class JsMap {
  final dynamic _object;

  const JsMap(this._object);

  dynamic operator [](String name) => getProperty(_object, name);
  operator []=(String name, dynamic value) => setProperty(_object, name, value);
}
