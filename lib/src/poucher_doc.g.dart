// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'poucher_doc.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<DesignDocument> _$designDocumentSerializer =
    new _$DesignDocumentSerializer();
Serializer<DesignDocumentView> _$designDocumentViewSerializer =
    new _$DesignDocumentViewSerializer();

class _$DesignDocumentSerializer
    implements StructuredSerializer<DesignDocument> {
  @override
  final Iterable<Type> types = const [DesignDocument, _$DesignDocument];
  @override
  final String wireName = 'DesignDocument';

  @override
  Iterable<Object> serialize(Serializers serializers, DesignDocument object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      '_id',
      serializers.serialize(object.id, specifiedType: const FullType(String)),
    ];
    if (object.views != null) {
      result
        ..add('views')
        ..add(serializers.serialize(object.views,
            specifiedType: const FullType(BuiltMap, const [
              const FullType(String),
              const FullType(DesignDocumentView)
            ])));
    }
    if (object.props != null) {
      result
        ..add('props')
        ..add(serializers.serialize(object.props,
            specifiedType: const FullType(BuiltMap,
                const [const FullType(String), const FullType(JsonObject)])));
    }
    if (object.rev != null) {
      result
        ..add('_rev')
        ..add(serializers.serialize(object.rev,
            specifiedType: const FullType(String)));
    }
    if (object.deleted != null) {
      result
        ..add('_deleted')
        ..add(serializers.serialize(object.deleted,
            specifiedType: const FullType(bool)));
    }
    if (object.conflicts != null) {
      result
        ..add('_conflicts')
        ..add(serializers.serialize(object.conflicts,
            specifiedType:
                const FullType(BuiltList, const [const FullType(String)])));
    }
    return result;
  }

  @override
  DesignDocument deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DesignDocumentBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'views':
          result.views.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(String),
                const FullType(DesignDocumentView)
              ])) as BuiltMap<dynamic, dynamic>);
          break;
        case 'props':
          result.props.replace(serializers.deserialize(value,
              specifiedType: const FullType(BuiltMap, const [
                const FullType(String),
                const FullType(JsonObject)
              ])) as BuiltMap<dynamic, dynamic>);
          break;
        case '_id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_rev':
          result.rev = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case '_deleted':
          result.deleted = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case '_conflicts':
          result.conflicts.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(String)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$DesignDocumentViewSerializer
    implements StructuredSerializer<DesignDocumentView> {
  @override
  final Iterable<Type> types = const [DesignDocumentView, _$DesignDocumentView];
  @override
  final String wireName = 'DesignDocumentView';

  @override
  Iterable<Object> serialize(Serializers serializers, DesignDocumentView object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'map',
      serializers.serialize(object.map, specifiedType: const FullType(String)),
    ];
    if (object.reduce != null) {
      result
        ..add('reduce')
        ..add(serializers.serialize(object.reduce,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  DesignDocumentView deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new DesignDocumentViewBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'map':
          result.map = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'reduce':
          result.reduce = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$DesignDocument extends DesignDocument {
  @override
  final BuiltMap<String, DesignDocumentView> views;
  @override
  final BuiltMap<String, JsonObject> props;
  @override
  final String id;
  @override
  final String rev;
  @override
  final bool deleted;
  @override
  final BuiltList<String> conflicts;

  factory _$DesignDocument([void Function(DesignDocumentBuilder) updates]) =>
      (new DesignDocumentBuilder()..update(updates)).build();

  _$DesignDocument._(
      {this.views, this.props, this.id, this.rev, this.deleted, this.conflicts})
      : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('DesignDocument', 'id');
    }
  }

  @override
  DesignDocument rebuild(void Function(DesignDocumentBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DesignDocumentBuilder toBuilder() =>
      new DesignDocumentBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DesignDocument &&
        views == other.views &&
        props == other.props &&
        id == other.id &&
        rev == other.rev &&
        deleted == other.deleted &&
        conflicts == other.conflicts;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc($jc(0, views.hashCode), props.hashCode), id.hashCode),
                rev.hashCode),
            deleted.hashCode),
        conflicts.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DesignDocument')
          ..add('views', views)
          ..add('props', props)
          ..add('id', id)
          ..add('rev', rev)
          ..add('deleted', deleted)
          ..add('conflicts', conflicts))
        .toString();
  }
}

class DesignDocumentBuilder
    implements Builder<DesignDocument, DesignDocumentBuilder>, PropsyBuilder {
  _$DesignDocument _$v;

  MapBuilder<String, DesignDocumentView> _views;
  MapBuilder<String, DesignDocumentView> get views =>
      _$this._views ??= new MapBuilder<String, DesignDocumentView>();
  set views(MapBuilder<String, DesignDocumentView> views) =>
      _$this._views = views;

  MapBuilder<String, JsonObject> _props;
  MapBuilder<String, JsonObject> get props =>
      _$this._props ??= new MapBuilder<String, JsonObject>();
  set props(MapBuilder<String, JsonObject> props) => _$this._props = props;

  String _id;
  String get id => _$this._id;
  set id(String id) => _$this._id = id;

  String _rev;
  String get rev => _$this._rev;
  set rev(String rev) => _$this._rev = rev;

  bool _deleted;
  bool get deleted => _$this._deleted;
  set deleted(bool deleted) => _$this._deleted = deleted;

  ListBuilder<String> _conflicts;
  ListBuilder<String> get conflicts =>
      _$this._conflicts ??= new ListBuilder<String>();
  set conflicts(ListBuilder<String> conflicts) => _$this._conflicts = conflicts;

  DesignDocumentBuilder();

  DesignDocumentBuilder get _$this {
    if (_$v != null) {
      _views = _$v.views?.toBuilder();
      _props = _$v.props?.toBuilder();
      _id = _$v.id;
      _rev = _$v.rev;
      _deleted = _$v.deleted;
      _conflicts = _$v.conflicts?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(covariant DesignDocument other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DesignDocument;
  }

  @override
  void update(void Function(DesignDocumentBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DesignDocument build() {
    _$DesignDocument _$result;
    try {
      _$result = _$v ??
          new _$DesignDocument._(
              views: _views?.build(),
              props: _props?.build(),
              id: id,
              rev: rev,
              deleted: deleted,
              conflicts: _conflicts?.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'views';
        _views?.build();
        _$failedField = 'props';
        _props?.build();

        _$failedField = 'conflicts';
        _conflicts?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'DesignDocument', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$DesignDocumentView extends DesignDocumentView {
  @override
  final String map;
  @override
  final String reduce;

  factory _$DesignDocumentView(
          [void Function(DesignDocumentViewBuilder) updates]) =>
      (new DesignDocumentViewBuilder()..update(updates)).build();

  _$DesignDocumentView._({this.map, this.reduce}) : super._() {
    if (map == null) {
      throw new BuiltValueNullFieldError('DesignDocumentView', 'map');
    }
  }

  @override
  DesignDocumentView rebuild(
          void Function(DesignDocumentViewBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  DesignDocumentViewBuilder toBuilder() =>
      new DesignDocumentViewBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is DesignDocumentView &&
        map == other.map &&
        reduce == other.reduce;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, map.hashCode), reduce.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('DesignDocumentView')
          ..add('map', map)
          ..add('reduce', reduce))
        .toString();
  }
}

class DesignDocumentViewBuilder
    implements Builder<DesignDocumentView, DesignDocumentViewBuilder> {
  _$DesignDocumentView _$v;

  String _map;
  String get map => _$this._map;
  set map(String map) => _$this._map = map;

  String _reduce;
  String get reduce => _$this._reduce;
  set reduce(String reduce) => _$this._reduce = reduce;

  DesignDocumentViewBuilder();

  DesignDocumentViewBuilder get _$this {
    if (_$v != null) {
      _map = _$v.map;
      _reduce = _$v.reduce;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(DesignDocumentView other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$DesignDocumentView;
  }

  @override
  void update(void Function(DesignDocumentViewBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$DesignDocumentView build() {
    final _$result =
        _$v ?? new _$DesignDocumentView._(map: map, reduce: reduce);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
