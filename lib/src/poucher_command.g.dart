// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'poucher_command.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const PoucherCommand _$init = const PoucherCommand._('init');
const PoucherCommand _$get = const PoucherCommand._('get');
const PoucherCommand _$put = const PoucherCommand._('put');
const PoucherCommand _$remove = const PoucherCommand._('remove');
const PoucherCommand _$createIndex = const PoucherCommand._('createIndex');
const PoucherCommand _$find = const PoucherCommand._('find');
const PoucherCommand _$query = const PoucherCommand._('query');
const PoucherCommand _$replicate = const PoucherCommand._('replicate');
const PoucherCommand _$bulkDocs = const PoucherCommand._('bulkDocs');
const PoucherCommand _$allDocs = const PoucherCommand._('allDocs');
const PoucherCommand _$destroy = const PoucherCommand._('destroy');

PoucherCommand _$poucherMessageValueOf(String name) {
  switch (name) {
    case 'init':
      return _$init;
    case 'get':
      return _$get;
    case 'put':
      return _$put;
    case 'remove':
      return _$remove;
    case 'createIndex':
      return _$createIndex;
    case 'find':
      return _$find;
    case 'query':
      return _$query;
    case 'replicate':
      return _$replicate;
    case 'bulkDocs':
      return _$bulkDocs;
    case 'allDocs':
      return _$allDocs;
    case 'destroy':
      return _$destroy;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<PoucherCommand> _$poucherMessageValues =
    new BuiltSet<PoucherCommand>(const <PoucherCommand>[
  _$init,
  _$get,
  _$put,
  _$remove,
  _$createIndex,
  _$find,
  _$query,
  _$replicate,
  _$bulkDocs,
  _$allDocs,
  _$destroy,
]);

const SmoucherCommand _$smouch_init = const SmoucherCommand._('init');
const SmoucherCommand _$smouch_get = const SmoucherCommand._('get');
const SmoucherCommand _$smouch_put = const SmoucherCommand._('put');
const SmoucherCommand _$smouch_remove = const SmoucherCommand._('remove');
const SmoucherCommand _$smouch_createIndex =
    const SmoucherCommand._('createIndex');
const SmoucherCommand _$smouch_find = const SmoucherCommand._('find');
const SmoucherCommand _$smouch_query = const SmoucherCommand._('query');
const SmoucherCommand _$smouch_bulkDocs = const SmoucherCommand._('bulkDocs');
const SmoucherCommand _$smouch_allDocs = const SmoucherCommand._('allDocs');
const SmoucherCommand _$smouch_destroy = const SmoucherCommand._('destroy');
const SmoucherCommand _$smouch_revert = const SmoucherCommand._('revert');
const SmoucherCommand _$smouch_bail = const SmoucherCommand._('bail');
const SmoucherCommand _$smouch_resolve = const SmoucherCommand._('resolve');
const SmoucherCommand _$smod_createIndex =
    const SmoucherCommand._('smodCreateIndex');
const SmoucherCommand _$smod_find = const SmoucherCommand._('smodFind');
const SmoucherCommand _$smod_allDocs = const SmoucherCommand._('smodAllDocs');
const SmoucherCommand _$smod_bulkGet = const SmoucherCommand._('smodBulkGet');
const SmoucherCommand _$smod_reset = const SmoucherCommand._('smodReset');
const SmoucherCommand _$smod_pull = const SmoucherCommand._('smodPull');
const SmoucherCommand _$smod_push = const SmoucherCommand._('smodPush');

SmoucherCommand _$smoucherMessageValueOf(String name) {
  switch (name) {
    case 'init':
      return _$smouch_init;
    case 'get':
      return _$smouch_get;
    case 'put':
      return _$smouch_put;
    case 'remove':
      return _$smouch_remove;
    case 'createIndex':
      return _$smouch_createIndex;
    case 'find':
      return _$smouch_find;
    case 'query':
      return _$smouch_query;
    case 'bulkDocs':
      return _$smouch_bulkDocs;
    case 'allDocs':
      return _$smouch_allDocs;
    case 'destroy':
      return _$smouch_destroy;
    case 'revert':
      return _$smouch_revert;
    case 'bail':
      return _$smouch_bail;
    case 'resolve':
      return _$smouch_resolve;
    case 'smodCreateIndex':
      return _$smod_createIndex;
    case 'smodFind':
      return _$smod_find;
    case 'smodAllDocs':
      return _$smod_allDocs;
    case 'smodBulkGet':
      return _$smod_bulkGet;
    case 'smodReset':
      return _$smod_reset;
    case 'smodPull':
      return _$smod_pull;
    case 'smodPush':
      return _$smod_push;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<SmoucherCommand> _$smoucherMessageValues =
    new BuiltSet<SmoucherCommand>(const <SmoucherCommand>[
  _$smouch_init,
  _$smouch_get,
  _$smouch_put,
  _$smouch_remove,
  _$smouch_createIndex,
  _$smouch_find,
  _$smouch_query,
  _$smouch_bulkDocs,
  _$smouch_allDocs,
  _$smouch_destroy,
  _$smouch_revert,
  _$smouch_bail,
  _$smouch_resolve,
  _$smod_createIndex,
  _$smod_find,
  _$smod_allDocs,
  _$smod_bulkGet,
  _$smod_reset,
  _$smod_pull,
  _$smod_push,
]);

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
