import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';

import 'package:ana_metadata/metadata.dart';

part 'change_doc.g.dart';

class OperationType extends EnumClass {
  // Use short and in order wirename to have operation type sort accordingly
  @BuiltValueEnumConst(wireName: 'aConflict')
  static const OperationType conflict = _$conflict;
  @BuiltValueEnumConst(wireName: 'adResolved')
  static const OperationType resolved = _$resolved;
  @BuiltValueEnumConst(wireName: 'aReplicate')
  static const OperationType replicate = _$replicate;
  @BuiltValueEnumConst(wireName: 'bCreate')
  static const OperationType create = _$create;
  @BuiltValueEnumConst(wireName: 'cUpdate')
  static const OperationType update = _$update;
  @BuiltValueEnumConst(wireName: 'dDelete')
  static const OperationType delete = _$delete;

  static BuiltSet<OperationType> get values => _$operationTypeValues;
  static OperationType valueOf(String name) => _$operationTypeValueOf(name);

  static Serializer<OperationType> get serializer => _$operationTypeSerializer;
  const OperationType._(String name) : super(name);
}

abstract class Change implements Built<Change, ChangeBuilder> {
  OperationType get operationType;

  @nullable
  String get rev;
  @nullable
  String get serializedDoc;

  static Serializer<Change> get serializer => _$changeSerializer;
  factory Change([updates(ChangeBuilder b)]) = _$Change;
  Change._();
}

abstract class ChangeDoc implements Couchy, Built<ChangeDoc, ChangeDocBuilder> {
  String get docId;
  String get docType;
  OperationType get lastOperation;
  BuiltList<Change> get changes;

  static Serializer<ChangeDoc> get serializer => _$changeDocSerializer;
  factory ChangeDoc([updates(ChangeDocBuilder b)]) = _$ChangeDoc;
  ChangeDoc._();
}

/// LatestChange is a user facing class to pass around the latest document
class LatestChange<T> {
  OperationType operationType;
  String changeDocId;
  String docId;
  String docType;
  T current;
  T base;
  T theirs;

  String toString() =>
      '{operationType: $operationType, changeDocId: $changeDocId, docId: $docId, docType: $docType, current: $current, base: $base, theirs: $theirs}';
}

class DocIdType {
  final String docId;
  final String docType;
  const DocIdType(this.docId, this.docType);
}
