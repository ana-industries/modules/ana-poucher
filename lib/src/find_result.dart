/// FindResult is a wrapper class for common response data from CouchDB.
class FindResult<T> {
  final List<T> results;

  /// bookmark let the user of the FindResult to continue the same find but to next page.
  /// This is a CouchDB only feature. PouchDB's internal find do not support this. Use with care.
  /// Use bookmark for mango query into a direct remote CouchDB adapter only.
  final String bookmark;

  bool hasNextPage;

  FindResult(this.results, {this.hasNextPage, this.bookmark});

  String toString() => '{results: $results, hasNextPage: $hasNextPage, bookmark: $bookmark}';
}
