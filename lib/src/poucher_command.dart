import 'package:built_value/built_value.dart';
import 'package:built_collection/built_collection.dart';

part 'poucher_command.g.dart';

/// PoucherCommand defines all the possible operation to perform with Poucher.
///
/// `EnumClass` is used because we need a standard way to convert enum to String and back to enum.
/// This is to serialise the command correctly back and forth dorker.
class PoucherCommand extends EnumClass {
  static const PoucherCommand init = _$init;
  static const PoucherCommand get = _$get;
  static const PoucherCommand put = _$put;
  static const PoucherCommand remove = _$remove;
  static const PoucherCommand createIndex = _$createIndex;
  static const PoucherCommand find = _$find;
  static const PoucherCommand query = _$query;
  static const PoucherCommand replicate = _$replicate;
  static const PoucherCommand bulkDocs = _$bulkDocs;
  static const PoucherCommand allDocs = _$allDocs;
  static const PoucherCommand destroy = _$destroy;

  static BuiltSet<PoucherCommand> get values => _$poucherMessageValues;
  static PoucherCommand valueOf(String name) => _$poucherMessageValueOf(name);
  const PoucherCommand._(String name) : super(name);
}

/// SmoucherCommand extended `PoucherCommand` (but too bad I can't really make it just extend it)
///
/// Most of the command name is the same to keep the compatibility between these two.
/// e.g. A 'init' String will match each other `init` command
///
/// SmoucherCommand added a few more command that is specific to the use case of SMOD
class SmoucherCommand extends EnumClass {
  static const SmoucherCommand init = _$smouch_init;
  static const SmoucherCommand get = _$smouch_get;
  static const SmoucherCommand put = _$smouch_put;
  static const SmoucherCommand remove = _$smouch_remove;
  static const SmoucherCommand createIndex = _$smouch_createIndex;
  static const SmoucherCommand find = _$smouch_find;
  static const SmoucherCommand query = _$smouch_query;
  static const SmoucherCommand bulkDocs = _$smouch_bulkDocs;
  static const SmoucherCommand allDocs = _$smouch_allDocs;
  static const SmoucherCommand destroy = _$smouch_destroy;

  /// Smoucher specifics
  static const SmoucherCommand revert = _$smouch_revert;
  static const SmoucherCommand bail = _$smouch_bail;
  static const SmoucherCommand resolve = _$smouch_resolve;

  /// SMod DB specifics
  static const SmoucherCommand smodCreateIndex = _$smod_createIndex;
  static const SmoucherCommand smodFind = _$smod_find;
  static const SmoucherCommand smodAllDocs = _$smod_allDocs;
  static const SmoucherCommand smodBulkGet = _$smod_bulkGet;
  static const SmoucherCommand smodReset = _$smod_reset;
  static const SmoucherCommand smodPull = _$smod_pull;
  static const SmoucherCommand smodPush = _$smod_push;

  static BuiltSet<SmoucherCommand> get values => _$smoucherMessageValues;
  static SmoucherCommand valueOf(String name) => _$smoucherMessageValueOf(name);
  const SmoucherCommand._(String name) : super(name);
}
