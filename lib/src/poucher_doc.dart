import 'package:built_value/serializer.dart';

import 'package:ana_metadata/metadata.dart';

part 'poucher_doc.g.dart';

abstract class DesignDocument
    implements Propsy, Built<DesignDocument, DesignDocumentBuilder> {
  @nullable
  BuiltMap<String, DesignDocumentView> get views;

  static Serializer<DesignDocument> get serializer =>
      _$designDocumentSerializer;

  factory DesignDocument([updates(DesignDocumentBuilder b)]) = _$DesignDocument;
  DesignDocument._();
}

abstract class DesignDocumentView
    implements Built<DesignDocumentView, DesignDocumentViewBuilder> {
  String get map;

  @nullable
  String get reduce;

  static Serializer<DesignDocumentView> get serializer =>
      _$designDocumentViewSerializer;

  factory DesignDocumentView([updates(DesignDocumentViewBuilder b)]) =
      _$DesignDocumentView;
  DesignDocumentView._();
}
