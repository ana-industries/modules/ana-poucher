class NotFoundException implements Exception {
  final String _msg;
  const NotFoundException(this._msg);
  String toString() => 'NotFoundException: $_msg';
}

class ConflictException implements Exception {
  final String _msg;
  const ConflictException(this._msg);
  String toString() => 'ConflictException: $_msg';
}

class BadRequestException implements Exception {
  final String _msg;
  const BadRequestException(this._msg);
  String toString() => 'BadRequestException: $_msg';
}
