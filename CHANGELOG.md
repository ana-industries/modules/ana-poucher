## 0.1.9

- Pull: Sync changes from remote DB.
- Conflict and resolution.
- Push: Push changes to remote DB.
- Bail: Restore sMod back to the point of before a Pull performed.

## 0.1.8

- Pagination for finding ChangeDoc.

## 0.1.7

- Standardize poucher export.
- Expose poucher doc to public.
- Fixed serializer for test.
- Map/reduce test both document and reducing.
- Refactored DesignDocument and its test.
- Use selector directly in find().
- Combining remote and local query. Combined query results.
- Query API will return original list if type is underfined.

## 0.1.6

- Pagination for `find()`.
- Query of Smoucher.

## 0.1.5

- Support `find()`'s sort with `Filter`.
- Refactored `createIndex()`.

## 0.1.4

- Fixed the find()'s selector to properly support PouchDB's find()
- Support PouchDB's pagination.

## 0.1.3

- Return "base" document correctly even after a flush.
- Smoucher now has ability to trigger event when there is changes add/removed/updated.
- Found and safe guard againts race condition where smod is being access during destroying and recreating.

## 0.1.2

- Removed the usage of `JsMap` inside `Smoucher`. It overwrites couple of JS native function in Web Worker context.
- Introduce the idea of "auxiliary" `Smoucher`, another instance use by non-main process.
- Documented `smoucher_impl.dart`.

## 0.1.1

- Added more tests for Poucher.
- Updated Poucher API:

  - `bulkDocs()` returns `<List<BulkDocRespond>>`.
  - added `poucherCreateIndex()` to allow indexing of document fields than `_id`.
  - added `query()`.
  - added ability to upload design documents.

## 0.1.0

- [Smoucher](https://sites.google.com/view/ana-wiki/tech-stack/how-it-works/smoucher):

  - The main tech behind [SMod](https://sites.google.com/view/ana-wiki/uiux/strategy-mod).
  - Covered all basic database functionality.
  - Still missing Sync, Merge, Push.

## 0.0.2

- Adapt ana-metadata changes

## 0.0.1

- Initial version, created by Stagehand
