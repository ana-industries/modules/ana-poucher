import 'dart:async';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher_service_base.dart';
import 'package:ana_poucher/src/js_interop/pouchdb.dart';
import 'package:ana_poucher/src/smoucher_service.dart';
import 'package:ana_poucher/src/utils.dart';
import 'package:path/path.dart' as p;

import 'serializer.dart';
import 'test_data.dart';
import 'testing_db_config.dart';

class TestingDb {
  static const int manipulation_delay_ms = 250;

  String dbPath;
  PouchDB _db;
  List<Animal> initialAnimals;
  List<Building> initialBuildings;
  List<GameHero> initialGameHeros;

  TestingDb(String testName) {
    dbPath = '${db_prefix}_$testName';
  }

  Future initService(dynamic service) {
    if (service is SmoucherService) {
      return service.init(dbPath,
          localDbName: 'local_${p.basename(dbPath)}',
          smodDbName: 'smod_${p.basename(dbPath)}',
          username: username,
          password: password);
    } else if (service is PoucherServiceBase) {
      return service.init(dbPath, username: username, password: password);
    } else {
      assert(
          false, 'unkonwn type passed into initService ${service.runtimeType}');
    }
    return null;
  }

  void populateBuiltHeros(List<Map<String, dynamic>> gameHeros) {
    initialGameHeros = [];

    gameHeros.forEach((gameHero) {
      var builtGameHero = GameHero((builder) => builder
        ..name = gameHero['name']
        ..series = gameHero['series']
        ..debut = gameHero['debut']
        ..id = gameHero['_id']);
      initialGameHeros.add(builtGameHero);
    });
  }

  Future prepareDb() async {
    CreateOption createOption;
    if (username != null && password != null) {
      createOption = CreateOption(
          auth: AuthOption(username: username, password: password));
    }
    _db = PouchDB(dbPath, createOption);

    initialAnimals = [
      Animal((builder) => builder
        ..id = 'fish_1'
        ..category = 'sea'
        ..isPet = true
        ..props['Name'] = JsonObject('Golden')
        ..props['Tail'] = JsonObject(1)
        ..props['Leg'] = JsonObject(0)),
      Animal((builder) => builder
        ..id = 'fish_2'
        ..category = 'sea'
        ..isPet = false
        ..props['Name'] = JsonObject('Baby Shark')
        ..props['Tail'] = JsonObject(1)
        ..props['Leg'] = JsonObject(0)),
      Animal((builder) => builder
        ..id = 'fish_3'
        ..category = 'sea'
        ..isPet = true
        ..props['Name'] = JsonObject('Nemo')
        ..props['Tail'] = JsonObject(1)
        ..props['Leg'] = JsonObject(0)),
      Animal((builder) => builder
        ..id = 'dog_1'
        ..category = 'land'
        ..isPet = true
        ..props['Name'] = JsonObject('TongTong')
        ..props['Tail'] = JsonObject(1)
        ..props['Leg'] = JsonObject(4)),
      Animal((builder) => builder
        ..id = 'dog_2'
        ..category = 'land'
        ..isPet = false
        ..props['Name'] = JsonObject('Big Bad Wolf')
        ..props['Tail'] = JsonObject(1)
        ..props['Leg'] = JsonObject(4)),
      Animal((builder) => builder
        ..id = 'elephant_3'
        ..category = 'land'
        ..isPet = false
        ..props['Name'] = JsonObject('Dumbo')
        ..props['Tail'] = JsonObject(1)
        ..props['Leg'] = JsonObject(4)),
      Animal((builder) => builder
        ..id = 'lion_4'
        ..category = 'land'
        ..isPet = false
        ..props['Name'] = JsonObject('Simba')
        ..props['Tail'] = JsonObject(1)
        ..props['Leg'] = JsonObject(4)),
      Animal((builder) => builder
        ..id = 'lion_5'
        ..category = 'land'
        ..isPet = true
        ..props['Name'] = JsonObject('Fake Cat')
        ..props['Tail'] = JsonObject(1)
        ..props['Leg'] = JsonObject(4)),
      Animal((builder) => builder
        ..id = 'spider_1'
        ..category = 'Arachnid'
        ..isPet = false
        ..props['Name'] = JsonObject('Ancy Wincy')
        ..props['Tail'] = JsonObject(0)
        ..props['Leg'] = JsonObject(8)),
      Animal((builder) => builder
        ..id = 'spider_2'
        ..category = 'Arachnid'
        ..isPet = false
        ..props['Name'] = JsonObject('Blazy Wincy')
        ..props['Tail'] = JsonObject(0)
        ..props['Leg'] = JsonObject(8)),
      Animal((builder) => builder
        ..id = 'spider_3'
        ..category = 'Arachnid'
        ..isPet = false
        ..props['Name'] = JsonObject('Crazy Wincy')
        ..props['Tail'] = JsonObject(0)
        ..props['Leg'] = JsonObject(8)),
      Animal((builder) => builder
        ..id = 'spider_4'
        ..category = 'Arachnid'
        ..isPet = false
        ..props['Name'] = JsonObject('Dozy Wincy')
        ..props['Tail'] = JsonObject(0)
        ..props['Leg'] = JsonObject(8)),
      Animal((builder) => builder
        ..id = 'spider_5'
        ..category = 'Arachnid'
        ..isPet = false
        ..props['Name'] = JsonObject('Easy Wincy')
        ..props['Tail'] = JsonObject(0)
        ..props['Leg'] = JsonObject(8))
    ];

    initialBuildings = [
      Building((builder) => builder
        ..id = 'house_1'
        ..floor = 1),
      Building((builder) => builder
        ..id = 'house_2'
        ..floor = 2),
    ];

    // sample characters taken directly from
    // https://nolanlawson.github.io/pouchdb-find/
    final gameHeros = [
      {
        "name": "Donkey Kong",
        "series": "Mario",
        "debut": 1981,
        "_id": "dk",
      },
      {
        "name": "Captain Falcon",
        "series": "F-Zero",
        "debut": 1990,
        "_id": "falcon",
      },
      {
        "name": "Fox",
        "series": "Star Fox",
        "debut": 1993,
        "_id": "fox",
      },
      {
        "name": "Kirby",
        "series": "Kirby",
        "debut": 1992,
        "_id": "kirby",
      },
      {
        "name": "Link",
        "series": "Zelda",
        "debut": 1986,
        "_id": "link",
      },
      {
        "name": "Luigi",
        "series": "Mario",
        "debut": 1983,
        "_id": "luigi",
      },
      {
        "name": "Mario",
        "series": "Mario",
        "debut": 1981,
        "_id": "mario",
      },
      {
        "name": "Ness",
        "series": "Earthbound",
        "debut": 1994,
        "_id": "ness",
      },
      {
        "name": "Pikachu",
        "series": "Pokemon",
        "debut": 1996,
        "_id": "pikachu",
      },
      {
        "name": "Jigglypuff",
        "series": "Pokemon",
        "debut": 1996,
        "_id": "puff",
      },
      {
        "name": "Samus",
        "series": "Metroid",
        "debut": 1986,
        "_id": "samus",
      },
      {
        "name": "Yoshi",
        "series": "Mario",
        "debut": 1990,
        "_id": "yoshi",
      }
    ];

    populateBuiltHeros(gameHeros);

    final initialData = [
      ...initialAnimals,
      ...initialBuildings,
      ...initialGameHeros
    ];

    return promiseToFuture(_db.bulkDocs(_jsObjectify(initialData
        .map((data) => standardSerializers.serialize(data))
        .toList())));
  }

  Future<Animal> manipulateFish1() async {
    var fish = jsToDart(await promiseToFuture(_db.get('fish_1')));
    fish['props']['Name'] = 'Remote Manipulated 1';
    await promiseToFuture(_db.put(dartToJs(fish)));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return standardSerializers.deserialize(fish);
  }

  Future<Animal> manipulateFish2() async {
    var fish = jsToDart(await promiseToFuture(_db.get('fish_2')));
    fish['props']['Name'] = 'Remote Manipulated 2';
    fish['props']['Tail'] = 2;
    fish['props']['Leg'] = 4;
    await promiseToFuture(_db.put(dartToJs(fish)));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return standardSerializers.deserialize(fish);
  }

  Future<Animal> manipulateFish3() async {
    var fish = jsToDart(await promiseToFuture(_db.get('fish_3')));
    fish['props']['Name'] = 'Remote Manipulated 3';
    fish['props']['Tail'] = 1;
    fish['props']['Leg'] = 3;
    await promiseToFuture(_db.put(dartToJs(fish)));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return standardSerializers.deserialize(fish);
  }

  Future<Animal> deleteFish1() async {
    var fish = jsToDart(await promiseToFuture(_db.get('fish_1')));
    fish['props']['Name'] = 'Remote Deleted 1';
    fish['_deleted'] = true;
    await promiseToFuture(_db.put(dartToJs(fish)));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return standardSerializers.deserialize(fish);
  }

  Future<Animal> deleteFish2() async {
    var fish = jsToDart(await promiseToFuture(_db.get('fish_2')));
    fish['props']['Name'] = 'Remote Deleted 2';
    fish['_deleted'] = true;
    await promiseToFuture(_db.put(dartToJs(fish)));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return standardSerializers.deserialize(fish);
  }

  Future<Animal> createFish4() async {
    var fish = Animal((builder) => builder
      ..id = 'fish_4'
      ..category = 'sea'
      ..isPet = true
      ..props['Name'] = JsonObject('Remote Created Mermaid')
      ..props['Tail'] = JsonObject(0)
      ..props['Leg'] = JsonObject(2));
    await promiseToFuture(
        _db.put(dartToJs(standardSerializers.serialize(fish))));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return fish;
  }

  Future<Animal> createFish5() async {
    var fish = Animal((builder) => builder
      ..id = 'fish_5'
      ..category = 'sea'
      ..isPet = false
      ..props['Name'] = JsonObject('Remote Created Sebastien')
      ..props['Tail'] = JsonObject(8)
      ..props['Leg'] = JsonObject(8));
    await promiseToFuture(
        _db.put(dartToJs(standardSerializers.serialize(fish))));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return fish;
  }

  Future manipulateDog2() async {
    var dog = jsToDart(await promiseToFuture(_db.get('dog_2')));
    dog['props']['Name'] = "Red Riding Hood";
    await promiseToFuture(_db.put(dartToJs(dog)));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return;
  }

  Future deleteDog1() async {
    var dog = jsToDart(await promiseToFuture(_db.get('dog_1')));
    dog['_deleted'] = true;
    await promiseToFuture(_db.put(dartToJs(dog)));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return;
  }

  Future deleteDog2() async {
    var dog = jsToDart(await promiseToFuture(_db.get('dog_2')));
    dog['_deleted'] = true;
    await promiseToFuture(_db.put(dartToJs(dog)));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return;
  }

  Future createDog3() async {
    var dog = Animal((builder) => builder
      ..id = 'dog_3'
      ..category = 'land'
      ..isPet = true
      ..props['Name'] = JsonObject('Maya')
      ..props['Tail'] = JsonObject(3));
    await promiseToFuture(
        _db.put(dartToJs(standardSerializers.serialize(dog))));
    await Future.delayed(Duration(milliseconds: manipulation_delay_ms));
    return;
  }

  Future createLandSortIndex() async {
    final index = {
      'fields': ['props.Name'],
      'name': 'landNameSort'
    };
    return promiseToFuture(_db.createIndex(dartToJs(index)));
  }

  Future createCategoryMapReduce(String designDocName, String viewName) async {
    final view = {
      "_id": "_design/$designDocName",
      "views": {
        "$viewName": {
          "reduce": "_sum",
          "map": "function (doc) {emit(doc.category, 1)}"
        }
      }
    };

    return promiseToFuture(_db.put(dartToJs(view)));
  }

  Future destroyDb() async {
    return _db.destroy();
  }

  dynamic _jsObjectify(dynamic dartObj) => dartToJs(dartObj);
}
