@TestOn("browser")

import "package:async/async.dart";
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('quick');
  SmoucherService _service;
  Dorker<String> _logProcessingDorker;
  Dorker<String> _loggingDorker;
  StreamQueue<String> _logStream;

  // We do setup and tear down full service for each test in this group.
  setUp(() async {
    _logProcessingDorker = Dorker();
    _loggingDorker = Dorker.CrossLink(_logProcessingDorker);
    _logStream = StreamQueue<String>(_logProcessingDorker.onMessage);

    var dorker = Dorker();
    Smoucher(Dorker.CrossLink(dorker), _loggingDorker);

    _service = SmoucherService(dorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.initService(_service);

    // Uncomment next line to help debugging. It will print the log in console.
    //_logProcessingDorker.onMessage.listen(print);
  });

  tearDown(() async {
    _loggingDorker.dispose();
    _logProcessingDorker.dispose();
    await _service.destroy();
    await _logStream.cancel();
  });

  test('updated a bunch of documents, use SMod version', () async {
    var fishes = await _service.fetchMultiple(['fish_1', 'fish_2']);
    final updatedFishes = fishes
        .map((fish) =>
            fish.rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
        .toList();

    await _service.bulkDocs(updatedFishes);

    await _testingDb.manipulateFish1();
    await _testingDb.manipulateFish2();

    var secondBatch =
        await _service.fetchMultiple<Animal>(['fish_1', 'fish_2']);
    expect(secondBatch[0], AnimalMatcher(updatedFishes[0]));
    expect(secondBatch[1], AnimalMatcher(updatedFishes[1]));
  });
}
