@TestOn("browser")

import "package:async/async.dart";
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher.dart';
import 'package:ana_poucher/poucher_service_base.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('test_change');
  SmoucherService _service;
  Dorker<String> _logProcessingDorker;
  Dorker<String> _loggingDorker;
  StreamQueue<String> _logStream;

  // We do setup and tear down full service for each test in this group.
  setUp(() async {
    _logProcessingDorker = Dorker();
    _loggingDorker = Dorker.CrossLink(_logProcessingDorker);
    _logStream = StreamQueue<String>(_logProcessingDorker.onMessage);

    var dorker = Dorker();
    Smoucher(Dorker.CrossLink(dorker), _loggingDorker);

    _service = SmoucherService(dorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.initService(_service);

    // Uncomment next line to help debugging. It will print the log in console.
    //_logProcessingDorker.onMessage.listen(print);
  });

  tearDown(() async {
    _loggingDorker.dispose();
    _logProcessingDorker.dispose();
    await _service.destroy();
    await _logStream.cancel();
  });

  group('External Changes', () {
    test('The second get after change, will get the external changed document',
        () async {
      var goldFish = await _service.fetch('fish_1');

      // Remote DB changed the document
      await _testingDb.manipulateFish1();
      var secondGoldFish = await _service.fetch('fish_1');

      expect(secondGoldFish, isNot(goldFish));
    });

    test('Second multiple fetch after change, will get the external changes',
        () async {
      var fishes = await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

      // Remote DB changed the document
      await _testingDb.manipulateFish1();
      await _testingDb.manipulateFish2();
      var secondBatch =
          await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

      expect(secondBatch[0], isNot(fishes[0]));
      expect(secondBatch[1], isNot(fishes[1]));
      expect(secondBatch[2], fishes[2]); // Second fish didn't change
    });

    test(
        'Second multiple fetch after change, will get the external deleted changes',
        () async {
      var fishes = await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

      // Remote DB changed the document
      await _testingDb.manipulateFish2();
      await _testingDb.deleteFish2();
      var secondBatch =
          await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

      expect(secondBatch[0], fishes[0]);
      expect(secondBatch[1], isNull);
      expect(secondBatch[2], fishes[2]); // Second fish didn't change
    });

    test('Same find after change, updated result', () async {
      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 2
      });

      await _testingDb.manipulateDog2();

      final secondResults = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 2
      });

      expect(secondResults.results[0], results.results[0]);
      // Same object, but changed property
      expect(secondResults.results[1].id, results.results[1].id);
      expect(secondResults.results[1], isNot(results.results[1]));
    });

    test('Same find after change, newly created result', () async {
      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 3
      });

      await _testingDb.createDog3();

      final secondResults = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 3
      });

      expect(secondResults.results[0], results.results[0]);
      expect(secondResults.results[1], results.results[1]);
      expect(secondResults.results[2], isNot(results.results[2]));
      expect(secondResults.results[2].id, 'dog_3');
    });

    test('Same find after change, deleted result', () async {
      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 3
      });

      await _testingDb.manipulateDog2();
      await _testingDb.deleteDog2();

      final secondResults = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 3
      });

      expect(secondResults.results[0], results.results[0]);
      expect(secondResults.results[1], results.results[2]);
    });

    test('Different but overlapped find after change, updated result',
        () async {
      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 2
      });

      await _testingDb.manipulateDog2();

      final secondResults = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 4
      });

      expect(secondResults.results.length, 4);
      expect(secondResults.results[0], results.results[0]);
      // Same object, but changed property
      expect(secondResults.results[1].id, results.results[1].id);
      expect(secondResults.results[1], isNot(results.results[1]));
    });
  });

  group('External + SMod changes', () {
    group('via fetch: ', () {
      test('updated a document, use SMod version', () async {
        final dog = await _service.fetch<Animal>('dog_2');
        final updatedDog =
            dog.rebuild((builder) => builder..props['Leg'] = JsonObject(100));

        await _service.upload(updatedDog);

        await _testingDb.manipulateDog2();

        // Get it
        var secondDog = await _service.fetch<Animal>('dog_2');
        expect(secondDog, AnimalMatcher(updatedDog));
      });

      test('updated a document, deleted remotely, use SMod version', () async {
        final dog = await _service.fetch<Animal>('dog_2');
        final updatedDog =
            dog.rebuild((builder) => builder..props['Leg'] = JsonObject(100));

        await _service.upload(updatedDog);
        await _testingDb.manipulateDog2();
        await _testingDb.deleteDog2();

        try {
          var secondDog = await _service.fetch<Animal>('dog_2');
          expect(secondDog, AnimalMatcher(updatedDog));
        } catch (e) {
          fail('Caught $e when trying to fetch dog 2 after a remote deleteion');
        }
      },
          skip:
              'false positive: remote deletion did not cause a conflict in localDB');

      test('updated a bunch of documents, use SMod version', () async {
        var fishes = await _service.fetchMultiple(['fish_1', 'fish_2']);
        final updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
            .toList();

        await _service.bulkDocs(updatedFishes);

        await _testingDb.manipulateFish1();
        await _testingDb.manipulateFish2();

        var secondBatch =
            await _service.fetchMultiple<Animal>(['fish_1', 'fish_2']);
        expect(secondBatch[0], AnimalMatcher(updatedFishes[0]));
        expect(secondBatch[1], AnimalMatcher(updatedFishes[1]));
      });

      test(
          'updated a bunch of documents, some of them overlapped with external changes',
          () async {
        var fishes =
            await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
        final updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(3)))
            .toList();

        await _service.bulkDocs(updatedFishes);
        await _testingDb.manipulateFish2();

        var secondBatch = await _service
            .fetchMultiple<Animal>(['fish_1', 'fish_2', 'fish_3']);
        expect(secondBatch[0], AnimalMatcher(updatedFishes[0]));
        expect(secondBatch[1], AnimalMatcher(updatedFishes[1]));
        expect(secondBatch[2], AnimalMatcher(updatedFishes[2]));
      });

      test(
          'updated a bunch of documents, some of them deleted in remote, use SMod version',
          () async {
        var fishes =
            await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
        final updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(5)))
            .toList();

        await _service.bulkDocs(updatedFishes);
        await _testingDb.deleteFish2();

        var secondBatch = await _service
            .fetchMultiple<Animal>(['fish_1', 'fish_2', 'fish_3']);
        expect(secondBatch[0], AnimalMatcher(updatedFishes[0]));
        expect(secondBatch[1], AnimalMatcher(updatedFishes[1]));
        expect(secondBatch[2], AnimalMatcher(updatedFishes[2]));
      },
          skip:
              'false positive: remote deletion did not cause a conflict in localDB');

      test('deleted a document, remained deleted locally', () async {
        final fish = await _service.fetch<Animal>('fish_1');
        await _service.remove(fish);

        // Just guessing: manipulate multiple time to have a better chance at
        //   "remote became the winning revision"
        await _testingDb.manipulateFish1();
        await _testingDb.manipulateFish1();
        await _testingDb.manipulateFish1();

        // Just failed to fetch didn't really tell if there are conflicts,
        // This is why we need the preivous multiple manipulation to game the revisions.
        expect(_service.fetch('fish_1'),
            throwsA(TypeMatcher<NotFoundException>()));
      });

      test('deleted a bunch of documents, remained deleted locally', () async {
        var fishes = await _service.fetchMultiple<Animal>(['fish_1', 'fish_2']);
        final updatedFishes = fishes
            .map((fish) => fish.rebuild((builder) => builder..deleted = true))
            .toList();

        await _service.bulkDocs(updatedFishes);
        await _testingDb.manipulateFish1();
        await _testingDb.manipulateFish2();
        await _testingDb.manipulateFish1();
        await _testingDb.manipulateFish2();

        var secondBatch =
            await _service.fetchMultiple<Animal>(['fish_1', 'fish_2']);
        expect(secondBatch[0], isNull);
        expect(secondBatch[1], isNull);
      });

      test('create an identical document in smod vs remote, use SMod version',
          () async {
        var fish4 = Animal((builder) => builder
          ..id = 'fish_4'
          ..category = 'sea'
          ..isPet = false
          ..props['Name'] = JsonObject('Not a Mermaid')
          ..props['leg'] = JsonObject(4)
          ..props['tail'] = JsonObject(5));

        var newRev = await _service.upload(fish4);
        fish4 = fish4.rebuild((builder) => builder..rev = newRev);

        await _testingDb.createFish4();

        var secondFish4 = await _service.fetch('fish_4');
        expect(secondFish4, fish4);
      });

      test(
          'create a bunch of identical document in smod vs remote, use SMod version',
          () async {
        var fishes = [
          Animal((builder) => builder
            ..id = 'fish_4'
            ..category = 'sea'
            ..isPet = false
            ..props['Name'] = JsonObject('Not a Mermaid')
            ..props['leg'] = JsonObject(4)
            ..props['tail'] = JsonObject(5)),
          Animal((builder) => builder
            ..id = 'fish_5'
            ..category = 'sea'
            ..isPet = true
            ..props['Name'] = JsonObject('Not a Crab')
            ..props['leg'] = JsonObject(2)
            ..props['tail'] = JsonObject(1)),
        ];

        await _service.bulkDocs(fishes);
        await _testingDb.createFish4();
        await _testingDb.createFish5();

        var secondBatch =
            await _service.fetchMultiple<Animal>(['fish_4', 'fish_5']);
        expect(secondBatch[0], AnimalMatcher(fishes[0]));
        expect(secondBatch[1], AnimalMatcher(fishes[1]));
      });
    });

    group('via find: ', () {
      test('found and updated a document, use SMod version', () async {
        final dogs = await _service.find<Animal>({
          'selector': {'category': 'land'},
          'limit': 2
        });
        final updatedDogs = dogs.results
            .map((dog) => dog.rebuild(
                (builder) => builder..props['Name'] = JsonObject('CLonEs')))
            .toList();

        await _service.bulkDocs(updatedDogs);
        await _testingDb.manipulateDog2();

        var secondFind = await _service.find<Animal>({
          'selector': {'category': 'land'},
          'limit': 2,
          'conflicts': true
        });
        expect(secondFind.results[0], AnimalMatcher(updatedDogs[0]));
        expect(secondFind.results[1], AnimalMatcher(updatedDogs[1]));
      });
    });
  });

  group('External + Reverted SMod changes', () {
    Future _revertThese(Iterable<String> ids) async {
      final changes = (await _service.fetchAllChanges()).results;
      final selected = changes.where((change) => ids.contains(change.docId));

      await _service.revertChanges(selected.toList());
    }

    test(
        'revert updates of a bunch of same document, get the remote updated versions.',
        () async {
      var fishIds = ['fish_1', 'fish_2'];
      var fishes = await _service.fetchMultiple(fishIds);
      final updatedFishes = fishes
          .map((fish) =>
              fish.rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
          .toList();

      await _service.bulkDocs(updatedFishes);

      await _testingDb.manipulateFish1();
      await _testingDb.manipulateFish2();

      await _revertThese(fishIds);

      var secondBatch = await _service.fetchMultiple<Animal>(fishIds);

      expect(secondBatch[0].id, fishIds[0]);
      expect(secondBatch[0], isNot(AnimalMatcher(updatedFishes[0])));
      expect(secondBatch[0], isNot(AnimalMatcher(fishes[0])));
      expect(secondBatch[1].id, fishIds[1]);
      expect(secondBatch[1], isNot(AnimalMatcher(updatedFishes[1])));
      expect(secondBatch[1], isNot(AnimalMatcher(fishes[1])));
    });

    test(
        'revert updates of a bunch of same document, remotely deleted, failed to fetch.',
        () async {
      var dogIds = ['dog_1', 'dog_2'];
      var dogss = await _service.fetchMultiple(dogIds);
      final updatedDogs = dogss
          .map((dog) =>
              dog.rebuild((builder) => builder..props['Ear'] = JsonObject(8)))
          .toList();

      await _service.bulkDocs(updatedDogs);

      await _testingDb.deleteDog1();
      await _testingDb.deleteDog2();

      await _revertThese(dogIds);

      var secondBatch = await _service.fetchMultiple<Animal>(dogIds);

      expect(secondBatch[0], isNull);
      expect(secondBatch[1], isNull);
    });

    test(
        'revert deletes of a bunch of same document, get the remote updated versions.',
        () async {
      var fishIds = ['fish_1', 'fish_2'];
      var fishes = await _service.fetchMultiple(fishIds);
      final updatedFishes = fishes
          .map((fish) => fish.rebuild((builder) => builder..deleted = true))
          .toList();

      await _service.bulkDocs(updatedFishes);

      await _testingDb.manipulateFish1();
      await _testingDb.manipulateFish2();

      await _revertThese(fishIds);

      var secondBatch = await _service.fetchMultiple<Animal>(fishIds);

      expect(secondBatch[0].id, fishIds[0]);
      expect(secondBatch[0], isNot(AnimalMatcher(updatedFishes[0])));
      expect(secondBatch[0], isNot(AnimalMatcher(fishes[0])));
      expect(secondBatch[1].id, fishIds[1]);
      expect(secondBatch[1], isNot(AnimalMatcher(updatedFishes[1])));
      expect(secondBatch[1], isNot(AnimalMatcher(fishes[1])));
    });

    test('revert creation of a bunch of same document, get the remote creation',
        () async {
      var fishes = [
        Animal((builder) => builder
          ..id = 'fish_4'
          ..category = 'sea'
          ..isPet = false
          ..props['Name'] = JsonObject('Not a Mermaid')
          ..props['leg'] = JsonObject(4)
          ..props['tail'] = JsonObject(5)),
        Animal((builder) => builder
          ..id = 'fish_5'
          ..category = 'sea'
          ..isPet = true
          ..props['Name'] = JsonObject('Not a Crab')
          ..props['leg'] = JsonObject(2)
          ..props['tail'] = JsonObject(1)),
      ];

      await _service.bulkDocs(fishes);
      await _testingDb.createFish4();
      await _testingDb.createFish5();

      await _revertThese(['fish_4', 'fish_5']);

      var secondBatch =
          await _service.fetchMultiple<Animal>(['fish_4', 'fish_5']);
      expect(secondBatch[0], isNot(AnimalMatcher(fishes[0])));
      expect(secondBatch[1], isNot(AnimalMatcher(fishes[1])));
    });

    test('delete creation of a bunch of same document, get the remote creation',
        () async {
      final fishes = [
        Animal((builder) => builder
          ..id = 'fish_4'
          ..category = 'sea'
          ..isPet = false
          ..props['Name'] = JsonObject('Not a Mermaid')
          ..props['leg'] = JsonObject(4)
          ..props['tail'] = JsonObject(5)),
        Animal((builder) => builder
          ..id = 'fish_5'
          ..category = 'sea'
          ..isPet = true
          ..props['Name'] = JsonObject('Not a Crab')
          ..props['leg'] = JsonObject(2)
          ..props['tail'] = JsonObject(1)),
      ];

      await _service.bulkDocs(fishes);
      await _testingDb.createFish4();
      await _testingDb.createFish5();

      final secondBatch =
          await _service.fetchMultiple<Animal>(['fish_4', 'fish_5']);
      final deletedSecondBatch = secondBatch
          .map((fish) => fish.rebuild((builder) => builder..deleted = true))
          .toList();
      await _service.bulkDocs(deletedSecondBatch);

      final thirdBatch =
          await _service.fetchMultiple<Animal>(['fish_4', 'fish_5']);

      expect(thirdBatch[0], isNotNull);
      expect(thirdBatch[0], isNot(AnimalMatcher(fishes[0])));
      expect(thirdBatch[1], isNotNull);
      expect(thirdBatch[1], isNot(AnimalMatcher(fishes[1])));
    });
  });
}
