@TestOn("browser")
import 'package:test/test.dart';

import 'package:ana_poucher/poucher.dart';
import 'package:built_value/json_object.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('smod_push_test');
  SmoucherService _smoucherService;
  Dorker<String> _logProcessingDorker;
  Dorker<String> _loggingDorker;

  setUp(() async {
    _logProcessingDorker = Dorker();
    _loggingDorker = Dorker.CrossLink(_logProcessingDorker);

    var smoucherDorker = Dorker();
    Smoucher(Dorker.CrossLink(smoucherDorker), _loggingDorker);
    _smoucherService = SmoucherService(smoucherDorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.initService(_smoucherService);
    await _smoucherService.createIndexForChangeDoc();

    // Uncomment next line to help debugging. It will print the log in console.
    //_logProcessingDorker.onMessage.listen(print);
  });

  tearDown(() async {
    await _smoucherService.destroy();
  });

  group('Pushed succeeded.', () {
    test('Smod is cleared', () async {
      final fishes = await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);
      final changingDocs = [
        fishes[0]
            .rebuild((builder) => builder..props['Name'] = JsonObject('Maya')),
        fishes[1].rebuild((builder) => builder..deleted = true),
        Animal((builder) => builder
          ..id = 'fish_4'
          ..category = 'water'
          ..isPet = true)
      ];

      await _smoucherService.bulkDocs(changingDocs);

      await _smoucherService.push();

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.isEmpty, isTrue);
    });

    test('Getting updated documents from remote', () async {
      final fishes =
          await _smoucherService.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
      final updatedFishes = fishes
          .map((fish) =>
              fish.rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
          .toList();

      await _smoucherService.bulkDocs(updatedFishes);

      await _smoucherService.push();

      final againFishes =
          await _smoucherService.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
      for (var i = 0; i < againFishes.length; i++) {
        expect(againFishes[i], AnimalMatcher(updatedFishes[i]));
      }
    });

    test('Getting created documents from remote', () async {
      final newFishes = [
        Animal((builder) => builder
          ..id = 'fish_4'
          ..category = 'water'
          ..isPet = true),
        Animal((builder) => builder
          ..id = 'fish_5'
          ..category = 'water'
          ..isPet = false
          ..props['Name'] = JsonObject('Yin')),
        Animal((builder) => builder
          ..id = 'fish_6'
          ..category = 'water'
          ..isPet = false
          ..props['Name'] = JsonObject('Yang')),
      ];

      await _smoucherService.bulkDocs(newFishes);

      await _smoucherService.push();

      final againFishes =
          await _smoucherService.fetchMultiple(['fish_4', 'fish_5', 'fish_6']);
      for (var i = 0; i < againFishes.length; i++) {
        expect(againFishes[i], AnimalMatcher(newFishes[i]));
      }
    });

    test('Not getting deleted document from remote', () async {
      final fishes = await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);
      final updatedFishes = fishes
          .map((fish) => fish.rebuild((builder) => builder..deleted = true))
          .toList();

      await _smoucherService.bulkDocs(updatedFishes);

      await _smoucherService.push();

      expect(_smoucherService.fetch('fish_1'),
          throwsA(TypeMatcher<NotFoundException>()));
      expect(_smoucherService.fetch('fish_2'),
          throwsA(TypeMatcher<NotFoundException>()));
    });
  });
}
