@TestOn("browser")
import 'package:test/test.dart';

import 'package:ana_poucher/poucher.dart';
import 'package:built_value/json_object.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('smod_resolve_conflict_test');
  SmoucherService _smoucherService;
  Dorker<String> _logProcessingDorker;
  Dorker<String> _loggingDorker;

  setUp(() async {
    _logProcessingDorker = Dorker();
    _loggingDorker = Dorker.CrossLink(_logProcessingDorker);

    var smoucherDorker = Dorker();
    Smoucher(Dorker.CrossLink(smoucherDorker), _loggingDorker);
    _smoucherService = SmoucherService(smoucherDorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.initService(_smoucherService);
    await _smoucherService.createIndexForChangeDoc();

    // Uncomment next line to help debugging. It will print the log in console.
    //_logProcessingDorker.onMessage.listen(print);
  });

  tearDown(() async {
    await _smoucherService.destroy();
  });
  group('Throw error if', () {
    var updatedFishes;

    setUp(() async {
      final fishes = await _smoucherService
          .fetchMultiple<Animal>(['fish_1', 'fish_2', 'fish_3']);
      updatedFishes = [
        fishes[0].rebuild(
            (builder) => builder..props['Name'] = JsonObject('Same old 1')),
        fishes[1].rebuild(
            (builder) => builder..props['Name'] = JsonObject('Same old 2')),
        fishes[2].rebuild((builder) => builder..deleted = true),
        Animal((builder) => builder
          ..id = 'fish_4'
          ..category = 'water'
          ..isPet = true),
      ];

      await _smoucherService.bulkDocs(updatedFishes);

      await _testingDb.manipulateFish1();
      await _testingDb.deleteFish2();
      await _testingDb.manipulateFish3();
      await _testingDb.createFish4();

      await _smoucherService.pull();
    });

    test('resolving update conflict not with their document', () async {
      var conflicts =
          (await _smoucherService.fetchAllConflicts<Animal>()).results;

      var conflict = conflicts.first;
      final resolving = conflict.current.rebuild(
          (builder) => builder..props['Name'] = JsonObject('Resolved Fish 1'));

      expect(() => _smoucherService.resolve(conflict, resolving),
          throwsA(TypeMatcher<BadRequestException>()));
    });

    test('resolving create conflict not with their document', () async {
      var conflicts =
          (await _smoucherService.fetchAllConflicts<Animal>()).results;

      var conflict = conflicts[3];
      final resolving = conflict.current.rebuild(
          (builder) => builder..props['Name'] = JsonObject('Resolved Fish 4'));

      expect(() => _smoucherService.resolve(conflict, resolving),
          throwsA(TypeMatcher<BadRequestException>()));
    });

    test('resolving remote delete conflict not with their document', () async {
      var conflicts =
          (await _smoucherService.fetchAllConflicts<Animal>()).results;

      var conflict = conflicts[1];
      final resolving = conflict.current.rebuild(
          (builder) => builder..props['Name'] = JsonObject('Resolved Fish 2'));

      expect(() => _smoucherService.resolve(conflict, resolving),
          throwsA(TypeMatcher<BadRequestException>()));
    });

    test('resolving local delete conflict not with their document', () async {
      var conflicts =
          (await _smoucherService.fetchAllConflicts<Animal>()).results;

      var conflict = conflicts[2];
      final resolving = conflict.current.rebuild(
          (builder) => builder..props['Name'] = JsonObject('Resolved Fish 3'));

      expect(() => _smoucherService.resolve(conflict, resolving),
          throwsA(TypeMatcher<BadRequestException>()));
    });
  });

  group('Conflicts', () {
    var updatedFishes;

    setUp(() async {
      var fishes =
          await _smoucherService.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
      updatedFishes = fishes
          .map((fish) =>
              fish.rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
          .toList();

      await _smoucherService.bulkDocs(updatedFishes);

      await _testingDb.manipulateFish1();
      await _testingDb.manipulateFish2();

      await _smoucherService.pull();
    });

    test('reducing in count as resolving is done', () async {
      var conflicts =
          (await _smoucherService.fetchAllConflicts<Animal>()).results;
      var conflictCount = await _smoucherService.findConflictCount();

      expect(conflicts.length, 2);
      expect(conflictCount, 2);

      var conflict = conflicts.first;
      final resolving1 = conflict.theirs.rebuild(
          (builder) => builder..props['Name'] = JsonObject('Resolved Fish 1'));
      await _smoucherService.resolve(conflict, resolving1);

      conflicts = (await _smoucherService.fetchAllConflicts<Animal>()).results;
      conflictCount = await _smoucherService.findConflictCount();

      expect(conflicts.length, 1);
      expect(conflictCount, 1);
    });
    test('are gone after resolving all of them', () async {
      var conflicts =
          (await _smoucherService.fetchAllConflicts<Animal>()).results;
      expect(conflicts.length, 2);

      final resolving1 = conflicts[0].theirs.rebuild(
          (builder) => builder..props['Name'] = JsonObject('Resolved Fish 1'));
      final resolving2 = conflicts[1].theirs.rebuild(
          (builder) => builder..props['Name'] = JsonObject('Resolved Fish 2'));

      await _smoucherService.resolve(conflicts[0], resolving1);
      await _smoucherService.resolve(conflicts[1], resolving2);

      conflicts = (await _smoucherService.fetchAllConflicts<Animal>()).results;
      var conflictCount = await _smoucherService.findConflictCount();

      expect(conflicts.isEmpty, isTrue);
      expect(conflictCount, 0);
    });
  });

  group('Resolved', () {
    group('update.', () {
      Animal resolved1;
      Animal resolved2;

      setUp(() async {
        var fishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
        final updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);

        await _testingDb.manipulateFish1();
        await _testingDb.manipulateFish2();

        await _smoucherService.pull();

        var conflicts =
            (await _smoucherService.fetchAllConflicts<Animal>()).results;
        expect(conflicts.length, 2);

        resolved1 = conflicts[0].theirs.rebuild((builder) =>
            builder..props['Name'] = JsonObject('Resolved Fish 1'));
        resolved2 = conflicts[1].theirs.rebuild((builder) =>
            builder..props['Name'] = JsonObject('Resolved Fish 2'));

        await _smoucherService.resolve(conflicts[0], resolved1);
        await _smoucherService.resolve(conflicts[1], resolved2);
      });

      test('changes are resolved operation', () async {
        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, 3);

        var change = changes[0];
        expect(change.operationType, OperationType.resolved);
        expect(change.current, AnimalMatcher(resolved1));

        change = changes[1];
        expect(change.operationType, OperationType.resolved);
        expect(change.current, AnimalMatcher(resolved2));

        change = changes[2];
        expect(change.operationType, OperationType.update);
      });

      test('documents return after resolving conflict with updates', () async {
        final againFishes =
            await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);

        expect(againFishes[0], AnimalMatcher(resolved1));
        expect(againFishes[1], AnimalMatcher(resolved2));
      });
      test('documents return from remote after pushed a merged update',
          () async {
        await _smoucherService.push();

        final againFishes =
            await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);

        expect(againFishes[0], AnimalMatcher(resolved1));
        expect(againFishes[1], AnimalMatcher(resolved2));
      });
    });

    group('create.', () {
      Animal resolved4;
      Animal resolved5;

      setUp(() async {
        final newFishes = [
          Animal((builder) => builder
            ..id = 'fish_4'
            ..category = 'water'
            ..isPet = true),
          Animal((builder) => builder
            ..id = 'fish_5'
            ..category = 'water'
            ..isPet = false
            ..props['Name'] = JsonObject('Yin'))
        ];

        await _smoucherService.bulkDocs(newFishes);

        await _testingDb.createFish4();
        await _testingDb.createFish5();

        await _smoucherService.pull();

        var conflicts =
            (await _smoucherService.fetchAllConflicts<Animal>()).results;
        expect(conflicts.length, 2);

        resolved4 = conflicts[0].theirs.rebuild((builder) =>
            builder..props['Name'] = JsonObject('Resolved Fish 4'));
        resolved5 = conflicts[1].theirs.rebuild((builder) =>
            builder..props['Name'] = JsonObject('Resolved Fish 5'));

        await _smoucherService.resolve(conflicts[0], resolved4);
        await _smoucherService.resolve(conflicts[1], resolved5);
      });

      test('changes are resolved operation', () async {
        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, 2);

        var change = changes[0];
        expect(change.operationType, OperationType.resolved);
        expect(change.current, AnimalMatcher(resolved4));

        change = changes[1];
        expect(change.operationType, OperationType.resolved);
        expect(change.current, AnimalMatcher(resolved5));
      });

      test('documents return after resolving conflict with updates', () async {
        final againFishes =
            await _smoucherService.fetchMultiple(['fish_4', 'fish_5']);

        expect(againFishes[0], AnimalMatcher(resolved4));
        expect(againFishes[1], AnimalMatcher(resolved5));
      });
      test('documents return from remote after pushed a merged update',
          () async {
        await _smoucherService.push();

        final againFishes =
            await _smoucherService.fetchMultiple(['fish_4', 'fish_5']);

        expect(againFishes[0], AnimalMatcher(resolved4));
        expect(againFishes[1], AnimalMatcher(resolved5));
      });
    });

    group('local delete', () {
      Animal resolved1;
      Animal resolved2;

      setUp(() async {
        var fishes = await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);
        final updatedFishes = fishes
            .map((fish) => fish.rebuild((builder) => builder..deleted = true))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);

        await _testingDb.manipulateFish1();
        await _testingDb.manipulateFish2();

        await _smoucherService.pull();
      });

      group('with delete.', () {
        setUp(() async {
          var conflicts =
              (await _smoucherService.fetchAllConflicts<Animal>()).results;
          expect(conflicts.length, 2);

          resolved1 =
              conflicts[0].theirs.rebuild((builder) => builder..deleted = true);
          resolved2 =
              conflicts[1].theirs.rebuild((builder) => builder..deleted = true);

          await _smoucherService.resolve(conflicts[0], resolved1);
          await _smoucherService.resolve(conflicts[1], resolved2);
        });

        test('changes are resolved operation', () async {
          final changes = (await _smoucherService.fetchAllChanges(
                  includeDoc: true, includeConflict: true))
              .results;
          expect(changes.length, 2);

          var change = changes[0];
          expect(change.operationType, OperationType.resolved);
          expect(change.current, AnimalMatcher(resolved1));

          change = changes[1];
          expect(change.operationType, OperationType.resolved);
          expect(change.current, AnimalMatcher(resolved2));
        });

        test('documents return after resolving conflict with updates',
            () async {
          expect(_smoucherService.fetch('fish_1'),
              throwsA(TypeMatcher<NotFoundException>()));
          expect(_smoucherService.fetch('fish_2'),
              throwsA(TypeMatcher<NotFoundException>()));
        });
        test('documents return from remote after pushed a merged update',
            () async {
          await _smoucherService.push();

          expect(_smoucherService.fetch('fish_1'),
              throwsA(TypeMatcher<NotFoundException>()));
          expect(_smoucherService.fetch('fish_2'),
              throwsA(TypeMatcher<NotFoundException>()));
        });
      });

      group('with update.', () {
        setUp(() async {
          var conflicts =
              (await _smoucherService.fetchAllConflicts<Animal>()).results;
          expect(conflicts.length, 2);

          resolved1 = conflicts[0].theirs.rebuild((builder) =>
              builder..props['Name'] = JsonObject('Resolved Fish 1'));
          resolved2 = conflicts[1].theirs.rebuild((builder) =>
              builder..props['Name'] = JsonObject('Resolved Fish 2'));

          await _smoucherService.resolve(conflicts[0], resolved1);
          await _smoucherService.resolve(conflicts[1], resolved2);
        });

        test('changes are resolved operation', () async {
          final changes = (await _smoucherService.fetchAllChanges(
                  includeDoc: true, includeConflict: true))
              .results;
          expect(changes.length, 2);

          var change = changes[0];
          expect(change.operationType, OperationType.resolved);
          expect(change.current, AnimalMatcher(resolved1));

          change = changes[1];
          expect(change.operationType, OperationType.resolved);
          expect(change.current, AnimalMatcher(resolved2));
        });

        test('documents return after resolving conflict with updates',
            () async {
          final againFishes =
              await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);

          expect(againFishes[0], AnimalMatcher(resolved1));
          expect(againFishes[1], AnimalMatcher(resolved2));
        });
        test('documents return from remote after pushed a merged update',
            () async {
          await _smoucherService.push();

          final againFishes =
              await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);

          expect(againFishes[0], AnimalMatcher(resolved1));
          expect(againFishes[1], AnimalMatcher(resolved2));
        });
      });
    });

    group('remote delete', () {
      Animal resolved1;
      Animal resolved2;

      setUp(() async {
        var fishes = await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);
        final updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);

        await _testingDb.deleteFish1();
        await _testingDb.deleteFish2();

        await _smoucherService.pull();
      });

      group('with delete.', () {
        setUp(() async {
          var conflicts =
              (await _smoucherService.fetchAllConflicts<Animal>()).results;
          expect(conflicts.length, 2);

          resolved1 =
              conflicts[0].theirs.rebuild((builder) => builder..deleted = true);
          resolved2 =
              conflicts[1].theirs.rebuild((builder) => builder..deleted = true);

          await _smoucherService.resolve(conflicts[0], resolved1);
          await _smoucherService.resolve(conflicts[1], resolved2);
        });

        test('changes are resolved operation', () async {
          final changes = (await _smoucherService.fetchAllChanges(
                  includeDoc: true, includeConflict: true))
              .results;
          expect(changes.length, 2);

          var change = changes[0];
          expect(change.operationType, OperationType.resolved);
          expect(change.current, AnimalMatcher(resolved1));

          change = changes[1];
          expect(change.operationType, OperationType.resolved);
          expect(change.current, AnimalMatcher(resolved2));
        });

        test('documents return after resolving conflict with updates',
            () async {
          expect(_smoucherService.fetch('fish_1'),
              throwsA(TypeMatcher<NotFoundException>()));
          expect(_smoucherService.fetch('fish_2'),
              throwsA(TypeMatcher<NotFoundException>()));
        });
        test('documents return from remote after pushed a merged update',
            () async {
          await _smoucherService.push();

          expect(_smoucherService.fetch('fish_1'),
              throwsA(TypeMatcher<NotFoundException>()));
          expect(_smoucherService.fetch('fish_2'),
              throwsA(TypeMatcher<NotFoundException>()));
        });
      });

      group('with update.', () {
        setUp(() async {
          var conflicts =
              (await _smoucherService.fetchAllConflicts<Animal>()).results;
          expect(conflicts.length, 2);

          resolved1 = conflicts[0].theirs.rebuild((builder) => builder
            ..deleted = null
            ..props['Name'] = JsonObject('Resolved Fish 1'));
          resolved2 = conflicts[1].theirs.rebuild((builder) => builder
            ..deleted = null
            ..props['Name'] = JsonObject('Resolved Fish 2'));

          await _smoucherService.resolve(conflicts[0], resolved1);
          await _smoucherService.resolve(conflicts[1], resolved2);
        });

        test('changes are resolved operation', () async {
          final changes = (await _smoucherService.fetchAllChanges(
                  includeDoc: true, includeConflict: true))
              .results;
          expect(changes.length, 2);

          var change = changes[0];
          expect(change.operationType, OperationType.resolved);
          expect(change.current, AnimalMatcher(resolved1));

          change = changes[1];
          expect(change.operationType, OperationType.resolved);
          expect(change.current, AnimalMatcher(resolved2));
        });

        test('documents return after resolving conflict with updates',
            () async {
          final againFishes =
              await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);

          expect(againFishes[0], AnimalMatcher(resolved1));
          expect(againFishes[1], AnimalMatcher(resolved2));
        });
        test('documents return from remote after pushed a merged update',
            () async {
          await _smoucherService.push();

          final againFishes =
              await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);

          expect(againFishes[0], AnimalMatcher(resolved1));
          expect(againFishes[1], AnimalMatcher(resolved2));
        });
      });
    });
  });
}
