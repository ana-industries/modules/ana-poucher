@TestOn("browser")
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('test_find');
  SmoucherService _service;

  group('Basic', () {
    setUp(() async {
      var dorker = Dorker();
      Smoucher(Dorker.CrossLink(dorker));

      _service = SmoucherService(dorker, standardSerializers);

      await _testingDb.prepareDb();
      await _testingDb.initService(_service);
    });

    tearDown(() async {
      await _service.destroy();
    });

    test('Under limit find', () async {
      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 2
      });

      expect(results.results.length, 2);
      expect(results.results[0], AnimalMatcher(_testingDb.initialAnimals[3]));
      expect(results.results[1], AnimalMatcher(_testingDb.initialAnimals[4]));
    });

    test('Next page', () async {
      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 2,
        'skip': 2
      });

      expect(results.results.length, 2);
      expect(results.results[0], AnimalMatcher(_testingDb.initialAnimals[5]));
      expect(results.results[1], AnimalMatcher(_testingDb.initialAnimals[6]));
    },
        skip:
            'Pagination issue, we skipped 2 in remote, but local has only 2, doing same find will skipped this only 2.');

    test('Over limit find', () async {
      final results = await _service.find<Animal>({
        'selector': {'category': 'land'}
      });

      expect(results.results.length, 5);
    });

    test('after created a new document', () async {
      var thirdDog = Animal((builder) => builder
        ..id = 'dog_3'
        ..category = 'land'
        ..isPet = false
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));

      await _service.upload(thirdDog);

      final results = await _service.find<Animal>({
        'selector': {'category': 'land'}
      });

      // There's one more in the find all result, and our third dog is sorted to third entry
      expect(results.results.length, 6);
      expect(results.results[2], AnimalMatcher(thirdDog));
    });

    test('after created a bunch of new document', () async {
      var newAnimals = [
        Animal((builder) => builder
          ..id = 'elephant_2'
          ..category = 'land'
          ..isPet = false
          ..props['Name'] = JsonObject('Real Dumbo')
          ..props['leg'] = JsonObject(3)
          ..props['big ear'] = JsonObject(2)),
        Animal((builder) => builder
          ..id = 'zebra_1'
          ..category = 'land'
          ..isPet = false
          ..props['leg'] = JsonObject(5)
          ..props['tail'] = JsonObject(0)),
      ];

      await _service.bulkDocs(newAnimals);

      final results = await _service.find<Animal>({
        'selector': {'category': 'land'}
      });

      expect(results.results.length, 7);
      expect(results.results[2], AnimalMatcher(newAnimals[0]));
      expect(results.results[6], AnimalMatcher(newAnimals[1]));
    });

    test('after updated one document', () async {
      final dog = await _service.fetch<Animal>('dog_1');
      final updatedDog =
          dog.rebuild((builder) => builder..props['Leg'] = JsonObject(100));

      await _service.upload(updatedDog);

      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 1,
      });

      expect(results.results[0], AnimalMatcher(updatedDog));
    });

    test('after updated a bunch of documents', () async {
      final mammals = await _service.fetchMultiple<Animal>(['dog_1', 'dog_2']);
      var updatedMammals = mammals
          .map((mammal) => mammal
              .rebuild((builder) => builder..props['Leg'] = JsonObject(100)))
          .toList();

      await _service.bulkDocs(updatedMammals);

      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 2,
      });

      expect(results.results[0], AnimalMatcher(updatedMammals[0]));
      expect(results.results[1], AnimalMatcher(updatedMammals[1]));
    });

    test('after deleted one document', () async {
      final dog = await _service.fetch<Animal>('dog_2');
      await _service.remove(dog);

      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
      });

      expect(results.results.length, 4);
      for (var result in results.results) {
        expect(result.id, isNot('dog_2'));
      }
    });

    test('after deleted a bunch of documents', () async {
      var animals = await _service.fetchMultiple<Animal>(['dog_1', 'lion_4']);
      animals = animals
          .map((mammal) => mammal.rebuild((builder) => builder..deleted = true))
          .toList();

      await _service.bulkDocs(animals);

      final results = await _service.find<Animal>({
        'selector': {'category': 'land'},
      });

      expect(results.results.length, 3);
      for (var result in results.results) {
        expect(result.id, allOf(isNot('dog_1'), isNot('lion_4')));
      }
    });
  });

  group('With index', () {
    setUp(() async {
      var dorker = Dorker();
      Smoucher(Dorker.CrossLink(dorker));

      _service = SmoucherService(dorker, standardSerializers);

      await _testingDb.prepareDb();
      await _testingDb.createLandSortIndex();
      await _testingDb.initService(_service);
    });

    tearDown(() async {
      await _service.destroy();
    });

    test('Find with sort, index is replicated from remote', () async {
      var newAnimals = [
        Animal((builder) => builder
          ..id = 'elephant_2'
          ..category = 'land'
          ..isPet = false
          ..props['Name'] = JsonObject('Real Dumbo')
          ..props['leg'] = JsonObject(3)
          ..props['big ear'] = JsonObject(2)),
        Animal((builder) => builder
          ..id = 'zebra_1'
          ..category = 'land'
          ..isPet = false
          ..props['Name'] = JsonObject('Zoro')
          ..props['leg'] = JsonObject(5)
          ..props['tail'] = JsonObject(0)),
      ];

      await _service.bulkDocs(newAnimals);

      final results = await _service.find<Animal>({
        'selector': {
          'category': 'land',
          'props.Name': {'\$gte': null}
        },
        'sort': ['props.Name']
      });

      final actualNames = results.results
          .map((animal) => animal.props['Name'].asString)
          .toList();
      final sortedNames = List.from(actualNames)..sort();

      expect(results.results.length, 7);
      expect(actualNames, equals(sortedNames));
    });
  });
}
