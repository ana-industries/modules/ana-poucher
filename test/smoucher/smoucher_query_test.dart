@TestOn("browser")
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('test_query');
  final _ddocName = 'testDDoc';
  final _viewName = 'viewName';
  final _queryUrl = '$_ddocName/$_viewName';

  SmoucherService _service;

  Map _mapResultsAsAddition(List results) {
    final map = {};
    for (final result in results) {
      final key = result['key'];
      final value = result['value'];
      final oldValue = map.putIfAbsent(key, () => 0);
      map[key] = oldValue + value;
    }
    return map;
  }

  setUp(() async {
    var dorker = Dorker();
    Smoucher(Dorker.CrossLink(dorker));

    _service = SmoucherService(dorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.createCategoryMapReduce(_ddocName, _viewName);
    await _testingDb.initService(_service);
  });

  tearDown(() async {
    await _service.destroy();
  });

  test('Query only remote result when there are no local changes', () async {
    final results =
        await _service.query(_queryUrl, {'reduce': true, 'group': true});

    final mappedResults = _mapResultsAsAddition(results);

    expect(mappedResults.length, 4);
    expect(mappedResults.containsKey('Arachnid'), isTrue);
    expect(mappedResults['Arachnid'], 5);
    expect(mappedResults.containsKey('land'), isTrue);
    expect(mappedResults['land'], 5);
    expect(mappedResults.containsKey('sea'), isTrue);
    expect(mappedResults['sea'], 3);
  });

  test('Query both local and remote results when there are local changes',
      () async {
    // New data that introduce a new category and 2 new entries for 'land' category.
    var newAnimals = [
      Animal((builder) => builder
        ..id = 'elephant_2'
        ..category = 'land'
        ..isPet = false
        ..props['Name'] = JsonObject('Real Dumbo')
        ..props['leg'] = JsonObject(3)
        ..props['big ear'] = JsonObject(2)),
      Animal((builder) => builder
        ..id = 'zebra_1'
        ..category = 'land'
        ..isPet = false
        ..props['Name'] = JsonObject('Zoro')
        ..props['leg'] = JsonObject(5)
        ..props['tail'] = JsonObject(0)),
      Animal((builder) => builder
        ..id = 'iPhone_11'
        ..category = 'phone'
        ..isPet = true
        ..props['Name'] = JsonObject('iPhone 11 Max Pro')
        ..props['leg'] = JsonObject(0)
        ..props['tail'] = JsonObject(1)),
    ];

    await _service.bulkDocs(newAnimals);

    // Update old data that introduce a new 'canis' category
    final mammals = await _service.fetchMultiple<Animal>(['dog_1', 'dog_2']);
    var updatedMammals = mammals
        .map((mammal) =>
            mammal.rebuild((builder) => builder..category = 'canis'))
        .toList();

    await _service.bulkDocs(updatedMammals);

    final results =
        await _service.query(_queryUrl, {'reduce': true, 'group': true});

    // For this test, we are doing a "sum" reduce query,
    // thus, we add the value together for entries with same key.
    final mappedResults = _mapResultsAsAddition(results);

    expect(mappedResults.length, 6);
    expect(mappedResults.containsKey('Arachnid'), isTrue);
    expect(mappedResults['Arachnid'], 5);
    expect(mappedResults.containsKey('land'), isTrue);
    // Do note that, despite changing the 2 dogs category to 'canis',
    // 'land' category still included the changed dogs,
    // why? Because remote doesn't know what local changed.
    expect(mappedResults['land'], 7);
    expect(mappedResults.containsKey('sea'), isTrue);
    expect(mappedResults['sea'], 3);
    expect(mappedResults.containsKey('phone'), isTrue);
    expect(mappedResults['phone'], 1);
    expect(mappedResults.containsKey('canis'), isTrue);
    expect(mappedResults['canis'], 2);
  });
}
