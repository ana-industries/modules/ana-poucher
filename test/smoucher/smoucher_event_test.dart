@TestOn("browser")
import "package:async/async.dart";
import 'package:pedantic/pedantic.dart';
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('smoder_functional_test');
  SmoucherService _smoucherService;

  setUp(() async {
    var smoucherDorker = Dorker();
    Smoucher(Dorker.CrossLink(smoucherDorker));
    _smoucherService = SmoucherService(smoucherDorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.initService(_smoucherService);
    await _smoucherService.createIndexForChangeDoc();
  });

  tearDown(() async {
    await _smoucherService.destroy();
  });

  group('Changes event', () {
    StreamQueue _eventQueue;

    Future _revertThese(Iterable<String> ids) async {
      final changes = (await _smoucherService.fetchAllChanges()).results;
      final selected = changes.where((change) => ids.contains(change.docId));

      await _smoucherService.revertChanges(selected.toList());
    }

    setUp(() {
      _eventQueue = StreamQueue(_smoucherService.events);
      unawaited(_eventQueue.hasNext);
    });

    test('triggered after each create, update and delete a document.',
        () async {
      final bird = Animal((builder) => builder
        ..id = 'bird_1'
        ..category = 'air'
        ..isPet = true);

      final goldFish = await _smoucherService.fetch<Animal>('fish_1');
      var updatedGoldFish = goldFish
          .rebuild((builder) => builder..props['Name'] = JsonObject('Bronzy'));
      final deleting = await _smoucherService.fetch<Animal>('fish_2');

      await _smoucherService.upload(bird);
      await _smoucherService.upload(updatedGoldFish);
      await _smoucherService.remove(deleting);

      expect(_eventQueue, emitsInAnyOrder(["changes", "changes", "changes"]));
    });

    test('triggered once after update a bunch of document', () async {
      final fishes =
          await _smoucherService.fetchMultiple<Animal>(['fish_1', 'fish_2']);
      var updatedFishes = fishes
          .map((fish) =>
              fish.rebuild((builder) => builder..props['Tail'] = JsonObject(2)))
          .toList();

      await _smoucherService.bulkDocs(updatedFishes);

      expect(_eventQueue, emits("changes"));
    });

    test('triggered after revert a bunch of document', () async {
      final animals = await _smoucherService.find<Animal>({
        'selector': {'category': 'land'},
        'limit': 2
      });
      var updatedAnimals = animals.results
          .map((fish) =>
              fish.rebuild((builder) => builder..props['Tail'] = JsonObject(2)))
          .toList();

      await _smoucherService.bulkDocs(updatedAnimals);
      // Consume the event by previous bulkDocs
      await _eventQueue.next;

      await _revertThese(animals.results.map((animal) => animal.id));

      expect(_eventQueue, emits("changes"));
    });
  });
}
