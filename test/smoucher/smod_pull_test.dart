@TestOn("browser")
import 'package:test/test.dart';

import 'package:ana_poucher/poucher.dart';
import 'package:built_value/json_object.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('smod_pull_test');
  SmoucherService _smoucherService;
  Dorker<String> _logProcessingDorker;
  Dorker<String> _loggingDorker;

  setUp(() async {
    _logProcessingDorker = Dorker();
    _loggingDorker = Dorker.CrossLink(_logProcessingDorker);

    var smoucherDorker = Dorker();
    Smoucher(Dorker.CrossLink(smoucherDorker), _loggingDorker);
    _smoucherService = SmoucherService(smoucherDorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.initService(_smoucherService);
    await _smoucherService.createIndexForChangeDoc();

    // Uncomment next line to help debugging. It will print the log in console.
    //_logProcessingDorker.onMessage.listen(print);
  });

  tearDown(() async {
    await _smoucherService.destroy();
  });

  Future _revertThese(Iterable<String> ids) async {
    final changes = (await _smoucherService.fetchAllChanges()).results;
    final selected = changes.where((change) => ids.contains(change.docId));

    await _smoucherService.revertChanges(selected.toList());
  }

  Future _verifyThereIsNoConflictInAllChanges() async {
    final changes = (await _smoucherService.fetchAllChanges(
            includeDoc: true, includeConflict: true))
        .results;
    for (final change in changes) {
      expect(change.operationType, isNot(OperationType.conflict));
    }
  }

  Future _verifyThereIsNoConflict() async {
    final conflicts = (await _smoucherService.fetchAllConflicts()).results;
    expect(conflicts.isEmpty, isTrue);

    final conflictCount = await _smoucherService.findConflictCount();
    expect(conflictCount, 0);
  }

  group('Conflicts', () {
    var updatedFishes;
    var conflictFish1;
    var conflictFish2;

    group('for updated changes', () {
      setUp(() async {
        var fishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
        updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);

        // Change fish_1 and fish_2, submit the changes to remote
        conflictFish1 = await _testingDb.manipulateFish1();
        conflictFish2 = await _testingDb.manipulateFish2();

        await _smoucherService.pull();
      });

      test('within all changes.', () async {
        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, updatedFishes.length);

        var change = changes[0];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[0]));
        expect(change.theirs, AnimalMatcher(conflictFish1));

        change = changes[1];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[1]));
        expect(change.theirs, AnimalMatcher(conflictFish2));

        change = changes[2];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[2]));
        expect(change.theirs, isNull);
      });

      test('from conflicts only.', () async {
        final conflicts = (await _smoucherService.fetchAllConflicts()).results;
        expect(conflicts.length, 2);

        var conflict = conflicts[0];
        expect(conflict.operationType, OperationType.conflict);
        expect(conflict.current, AnimalMatcher(updatedFishes[0]));
        expect(conflict.theirs, AnimalMatcher(conflictFish1));

        conflict = conflicts[1];
        expect(conflict.operationType, OperationType.conflict);
        expect(conflict.current, AnimalMatcher(updatedFishes[1]));
        expect(conflict.theirs, AnimalMatcher(conflictFish2));
      });

      test('bailed. No conflict, restore to pre-pull', () async {
        await _smoucherService.bail();

        await _verifyThereIsNoConflict();

        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, updatedFishes.length);

        var change = changes[0];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[0]));

        change = changes[1];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[1]));

        change = changes[2];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[2]));

        final againFishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

        expect(againFishes[0], AnimalMatcher(updatedFishes[0]));
        expect(againFishes[1], AnimalMatcher(updatedFishes[1]));
        expect(againFishes[2], AnimalMatcher(updatedFishes[2]));
      });
    });

    group('for local deleted changes', () {
      setUp(() async {
        var fishes = await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);
        updatedFishes = fishes
            .map((fish) => fish.rebuild((builder) => builder..deleted = true))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);

        // Change fish_1 and fish_2, submit the changes to remote
        conflictFish1 = await _testingDb.manipulateFish1();
        conflictFish2 = await _testingDb.manipulateFish2();

        await _smoucherService.pull();
      });

      test('within all changes.', () async {
        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;

        var change = changes[0];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[0]));
        expect(change.theirs, AnimalMatcher(conflictFish1));

        change = changes[1];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[1]));
        expect(change.theirs, AnimalMatcher(conflictFish2));
      });

      test('from conflicts only.', () async {
        final conflicts = (await _smoucherService.fetchAllConflicts()).results;

        var conflict = conflicts[0];
        expect(conflict.operationType, OperationType.conflict);
        expect(conflict.current, AnimalMatcher(updatedFishes[0]));
        expect(conflict.theirs, AnimalMatcher(conflictFish1));

        conflict = conflicts[1];
        expect(conflict.operationType, OperationType.conflict);
        expect(conflict.current, AnimalMatcher(updatedFishes[1]));
        expect(conflict.theirs, AnimalMatcher(conflictFish2));
      });

      test('bailed. No conflict, restore to pre-pull', () async {
        await _smoucherService.bail();

        await _verifyThereIsNoConflict();

        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, updatedFishes.length);

        var change = changes[0];
        expect(change.operationType, OperationType.delete);

        change = changes[1];
        expect(change.operationType, OperationType.delete);

        expect(_smoucherService.fetch('fish_1'),
            throwsA(TypeMatcher<NotFoundException>()));
        expect(_smoucherService.fetch('fish_2'),
            throwsA(TypeMatcher<NotFoundException>()));
      });
    });

    group('for remote deleted changes', () {
      var deletedFish2;

      setUp(() async {
        var fishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
        updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);
        deletedFish2 = await _testingDb.deleteFish2();
        await _smoucherService.pull();
      });

      test('within all changes.', () async {
        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, updatedFishes.length);

        // Beware of the reordering of the changes
        var change = changes[0];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[1]));
        expect(change.theirs, AnimalMatcher(deletedFish2));

        change = changes[1];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[0]));
        expect(change.theirs, isNull);

        change = changes[2];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[2]));
        expect(change.theirs, isNull);
      });
      test('from conflicts only.', () async {
        final conflicts = (await _smoucherService.fetchAllConflicts()).results;
        expect(conflicts.length, 1);

        var conflict = conflicts[0];
        expect(conflict.operationType, OperationType.conflict);
        expect(conflict.current, AnimalMatcher(updatedFishes[1]));
        expect(conflict.theirs, AnimalMatcher(deletedFish2));
      });

      test('bailed. No conflict, restore to pre-pull', () async {
        await _smoucherService.bail();

        await _verifyThereIsNoConflict();

        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, updatedFishes.length);

        var change = changes[0];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[0]));

        change = changes[1];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[1]));

        change = changes[2];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[2]));

        final againFishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

        expect(againFishes[0], AnimalMatcher(updatedFishes[0]));
        expect(againFishes[1], AnimalMatcher(updatedFishes[1]));
        expect(againFishes[2], AnimalMatcher(updatedFishes[2]));
      });
    });

    group('for creation', () {
      final newFishes = [
        Animal((builder) => builder
          ..id = 'fish_4'
          ..category = 'water'
          ..isPet = true),
        Animal((builder) => builder
          ..id = 'fish_5'
          ..category = 'water'
          ..isPet = false
          ..props['Name'] = JsonObject('Yin')),
        Animal((builder) => builder
          ..id = 'fish_6'
          ..category = 'water'
          ..isPet = false
          ..props['Name'] = JsonObject('Yang')),
      ];
      var fish4;
      var fish5;

      setUp(() async {
        await _smoucherService.bulkDocs(newFishes);

        fish4 = await _testingDb.createFish4();
        fish5 = await _testingDb.createFish5();

        await _smoucherService.pull();
      });

      test('within all changes.', () async {
        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, newFishes.length);

        var change = changes[0];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(newFishes[0]));
        expect(change.theirs, AnimalMatcher(fish4));

        change = changes[1];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(newFishes[1]));
        expect(change.theirs, AnimalMatcher(fish5));

        change = changes[2];
        expect(change.operationType, OperationType.create);
        expect(change.current, AnimalMatcher(newFishes[2]));
        expect(change.theirs, isNull);
      });

      test('from conflicts only.', () async {
        final conflicts = (await _smoucherService.fetchAllConflicts()).results;
        expect(conflicts.length, 2);

        var conflict = conflicts[0];
        expect(conflict.operationType, OperationType.conflict);
        expect(conflict.current, AnimalMatcher(newFishes[0]));
        expect(conflict.theirs, AnimalMatcher(fish4));

        conflict = conflicts[1];
        expect(conflict.operationType, OperationType.conflict);
        expect(conflict.current, AnimalMatcher(newFishes[1]));
        expect(conflict.theirs, AnimalMatcher(fish5));
      });

      test('bailed. No conflict, restore to pre-pull', () async {
        await _smoucherService.bail();

        await _verifyThereIsNoConflict();

        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, newFishes.length);

        var change = changes[0];
        expect(change.operationType, OperationType.create);
        expect(change.current, AnimalMatcher(newFishes[0]));

        change = changes[1];
        expect(change.operationType, OperationType.create);
        expect(change.current, AnimalMatcher(newFishes[1]));

        change = changes[2];
        expect(change.operationType, OperationType.create);
        expect(change.current, AnimalMatcher(newFishes[2]));

        final againFishes = await _smoucherService
            .fetchMultiple(['fish_4', 'fish_5', 'fish_6']);

        expect(againFishes[0], AnimalMatcher(newFishes[0]));
        expect(againFishes[1], AnimalMatcher(newFishes[1]));
        expect(againFishes[2], AnimalMatcher(newFishes[2]));
      });
    });

    group('for mixed operations', () {
      var fish1;
      var fish2;
      var fish3;
      var fish4;
      var updatedFishes;

      setUp(() async {
        final fishes = await _smoucherService
            .fetchMultiple<Animal>(['fish_1', 'fish_2', 'fish_3']);
        updatedFishes = [
          fishes[0].rebuild(
              (builder) => builder..props['Name'] = JsonObject('Same old 1')),
          fishes[1].rebuild(
              (builder) => builder..props['Name'] = JsonObject('Same old 2')),
          fishes[2].rebuild((builder) => builder..deleted = true),
          Animal((builder) => builder
            ..id = 'fish_4'
            ..category = 'water'
            ..isPet = true),
        ];

        await _smoucherService.bulkDocs(updatedFishes);

        fish1 = await _testingDb.manipulateFish1();
        fish2 = await _testingDb.deleteFish2();
        fish3 = await _testingDb.manipulateFish3();
        fish4 = await _testingDb.createFish4();

        await _smoucherService.pull();
      });

      test('within all changes.', () async {
        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, updatedFishes.length);

        var change = changes[0];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[0]));
        expect(change.theirs, AnimalMatcher(fish1));

        change = changes[1];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[1]));
        expect(change.theirs, AnimalMatcher(fish2));

        change = changes[2];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[2]));
        expect(change.theirs, AnimalMatcher(fish3));

        change = changes[3];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[3]));
        expect(change.theirs, AnimalMatcher(fish4));
      });

      test('from conflicts only.', () async {
        final changes = (await _smoucherService.fetchAllConflicts()).results;

        var change = changes[0];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[0]));
        expect(change.theirs, AnimalMatcher(fish1));

        change = changes[1];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[1]));
        expect(change.theirs, AnimalMatcher(fish2));

        change = changes[2];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[2]));
        expect(change.theirs, AnimalMatcher(fish3));

        change = changes[3];
        expect(change.operationType, OperationType.conflict);
        expect(change.current, AnimalMatcher(updatedFishes[3]));
        expect(change.theirs, AnimalMatcher(fish4));
      });

      test('bailed. No conflict, restore to pre-pull', () async {
        await _smoucherService.bail();

        await _verifyThereIsNoConflict();

        final changes = (await _smoucherService.fetchAllChanges(
                includeDoc: true, includeConflict: true))
            .results;
        expect(changes.length, updatedFishes.length);

        //NOTE: Order matters
        var change = changes[0];
        expect(change.operationType, OperationType.create);
        expect(change.current, AnimalMatcher(updatedFishes[3]));

        change = changes[1];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[0]));

        change = changes[2];
        expect(change.operationType, OperationType.update);
        expect(change.current, AnimalMatcher(updatedFishes[1]));

        change = changes[3];
        expect(change.operationType, OperationType.delete);

        final againFishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_4']);

        expect(againFishes[0], AnimalMatcher(updatedFishes[0]));
        expect(againFishes[1], AnimalMatcher(updatedFishes[1]));
        expect(againFishes[2], AnimalMatcher(updatedFishes[3]));

        expect(_smoucherService.fetch('fish_3'),
            throwsA(TypeMatcher<NotFoundException>()));
      });
    });

    group('do not exist', () {
      var fishes = <Animal>[];

      setUp(() async {
        fishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
        final updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);
      });

      group('for unrelated remote change', () {
        setUp(() async {
          await _testingDb.manipulateDog2();
          await _testingDb.createFish4();
          await _smoucherService.pull();
        });

        test('in all changes.', () async {
          await _verifyThereIsNoConflictInAllChanges();
        });
        test('in conflicts.', () async {
          await _verifyThereIsNoConflict();
        });
      });

      group('for reverted smod before pull', () {
        setUp(() async {
          await _testingDb.manipulateFish1();
          await _testingDb.manipulateFish2();

          await _revertThese(fishes.map((fish) => fish.id));

          await _smoucherService.pull();
        });

        test('in all changes.', () async {
          await _verifyThereIsNoConflictInAllChanges();
        });
        test('in conflicts.', () async {
          await _verifyThereIsNoConflict();
        });
      });

      group('for reverted related changes after pull', () {
        setUp(() async {
          await _testingDb.manipulateFish2();

          await _smoucherService.pull();
          await _revertThese(['fish_2']);
        });

        test('in all changes.', () async {
          await _verifyThereIsNoConflictInAllChanges();
        });
        test('in conflicts.', () async {
          await _verifyThereIsNoConflict();
        });
      });
    });
  });

  group('Data consistency:', () {
    group('Revert after pull,', () {
      test('updated document became remote version', () async {
        final fishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
        final updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);

        // Change fish_1 and fish_2, submit the changes to remote
        final manipulatedFish1 = await _testingDb.manipulateFish1();
        final manipulatedFish2 = await _testingDb.manipulateFish2();

        await _smoucherService.pull();

        await _revertThese(['fish_1', 'fish_2']);

        final againFishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

        expect(againFishes[0], AnimalMatcher(manipulatedFish1));
        expect(againFishes[1], AnimalMatcher(manipulatedFish2));
        // Fish_3 remained the updated version
        expect(againFishes[2], AnimalMatcher(updatedFishes[2]));
      });

      test('created document became remote version', () async {
        final newFishes = [
          Animal((builder) => builder
            ..id = 'fish_4'
            ..category = 'water'
            ..isPet = true),
          Animal((builder) => builder
            ..id = 'fish_5'
            ..category = 'water'
            ..isPet = false
            ..props['Name'] = JsonObject('Yin')),
          Animal((builder) => builder
            ..id = 'fish_6'
            ..category = 'water'
            ..isPet = false
            ..props['Name'] = JsonObject('Yang')),
        ];
        await _smoucherService.bulkDocs(newFishes);

        final newFish4 = await _testingDb.createFish4();
        final newFish5 = await _testingDb.createFish5();

        await _smoucherService.pull();

        await _revertThese(['fish_4', 'fish_5']);

        final againFishes =
            await _smoucherService.fetchMultiple(['fish_4', 'fish_5']);

        expect(againFishes[0], AnimalMatcher(newFish4));
        expect(againFishes[1], AnimalMatcher(newFish5));
      });

      test('locally deleted document became avaiable and is remote version',
          () async {
        var fishes = await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);
        final updatedFishes = fishes
            .map((fish) => fish.rebuild((builder) => builder..deleted = true))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);

        // Change fish_1 and fish_2, submit the changes to remote
        final manipulatedFish1 = await _testingDb.manipulateFish1();
        final manipulatedFish2 = await _testingDb.manipulateFish2();

        await _smoucherService.pull();

        await _revertThese(['fish_1', 'fish_2']);

        var againFishes =
            await _smoucherService.fetchMultiple(['fish_1', 'fish_2']);

        expect(againFishes[0], AnimalMatcher(manipulatedFish1));
        expect(againFishes[1], AnimalMatcher(manipulatedFish2));
      });

      test('remotely deleted document became unavaible', () async {
        var fishes = await _smoucherService
            .fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
        final updatedFishes = fishes
            .map((fish) => fish
                .rebuild((builder) => builder..props['Tail'] = JsonObject(8)))
            .toList();

        await _smoucherService.bulkDocs(updatedFishes);
        await _testingDb.deleteFish2();
        await _smoucherService.pull();

        await _revertThese(['fish_2']);

        expect(_smoucherService.fetch('fish_2'),
            throwsA(TypeMatcher<NotFoundException>()));
      });
    });
  });
}
