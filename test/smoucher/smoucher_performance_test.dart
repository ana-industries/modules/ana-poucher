/// To test `Smoucher` performance.
/// Not neccesary the time used to perform an action,
/// but did we reduce the unneccesary remote call whenever possible.
@TestOn("browser")

import "package:async/async.dart";
import 'package:pedantic/pedantic.dart';
import 'package:test/test.dart';

import 'package:ana_poucher/poucher.dart';
import 'package:ana_poucher/poucher_service_base.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

final smoucherRequestLog = '== SMOUCHER PROCESSING REQUEST ==';

void main() {
  final _testingDb = TestingDb('smoucher_performance_test');
  PoucherServiceBase _service;
  Dorker<String> _logProcessingDorker;
  Dorker<String> _loggingDorker;
  StreamQueue<String> _logStream;

  setUp(() async {
    _logProcessingDorker = Dorker();
    _loggingDorker = Dorker.CrossLink(_logProcessingDorker);
    _logStream = StreamQueue<String>(_logProcessingDorker.onMessage);

    var poucherDorker = Dorker();
    Smoucher(Dorker.CrossLink(poucherDorker), _loggingDorker);

    _service = SmoucherService(poucherDorker, standardSerializers);
    await _testingDb.prepareDb();
    await _testingDb.initService(_service);

    // Trick `StreamQueue` to call `_ensureListening`
    // If we don't do this, dorker's broadcast stream will fire off everything
    // and our StreamQueue missed all of them because it hasn't start listening.
    unawaited(_logStream.hasNext);

    // Uncomment next line to help debugging. It will print the log in console.
    //_logProcessingDorker.onMessage.listen(print);
  });

  tearDown(() async {
    // Close both end of the dorker to clean up completely
    _loggingDorker.dispose();
    _logProcessingDorker.dispose();
    await _service.destroy();
    await _logStream.cancel();
  });

  /// This group of tests are testing did `Smoucher` made unneccesary remote calls.
  ///
  /// Most of the cases: there are valid document locally,
  /// so we should not hit the remote for the same document, in GET/ALLDOCs.
  /// Or there are nothing changes since the last identical FIND, so we don't hit remote.
  ///
  /// Do note that, we do not test for NECCESARY remote calls.
  /// e.g. A change occur and `Smoucher` should hit remote.
  /// These scenario should be tested in basic functionality tests, which we
  /// actively check for data consistency. Calling the remote does not
  /// guarentee `Smoucher` will return the correct result at the end.
  ///

  group('Skip remote calls', () {
    test('when doing the same get', () async {
      await _service.fetch('fish_1');

      // First GET will always hit the remote
      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      expect(await _logStream.next, contains('fish_1'));

      await _service.fetch('fish_1');

      // Dispose the log processing dorker to close the stream.
      // Only applicale during testing which we know this is the end,
      // and we need the stream to end for checking "neverEmits"
      _logProcessingDorker.dispose();

      // Run the stream all the way to the next request.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      expect(_logStream, neverEmits(contains('REMOTE')));
    });

    test('when doing the same alldocs', () async {
      await _service.fetchMultiple(['fish_1', 'fish_3']);

      // First GET will always hit the remote
      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('fish_1'));
      expect(log, contains('fish_3'));

      await _service.fetchMultiple(['fish_1', 'fish_3']);

      _logProcessingDorker.dispose();

      expect(_logStream, emitsThrough(smoucherRequestLog));
      expect(_logStream, neverEmits(contains('REMOTE')));
    });

    test('when doing the same find', () async {
      final query = {
        'selector': {'category': 'land'},
        'limit': 2
      };
      await _service.find<Animal>(query);

      // First FIND will always hit the remote
      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('dog_1'));
      expect(log, contains('dog_2'));

      await _service.find<Animal>(query);

      _logProcessingDorker.dispose();

      expect(_logStream, emitsThrough(smoucherRequestLog));
      expect(_logStream, neverEmits(contains('REMOTE')));
    });

    test('when doing new get but overlapped IDs with previous alldoc',
        () async {
      await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

      // First GET will always hit the remote
      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('fish_1'));
      expect(log, contains('fish_2'));
      expect(log, contains('fish_3'));

      await _service.fetch('fish_2');
      await _service.fetch('fish_1');

      _logProcessingDorker.dispose();

      expect(_logStream, emitsThrough(smoucherRequestLog));
      expect(_logStream, neverEmits(contains('REMOTE')));
    });

    test('when doing new alldocs but overlapped IDs with previous get',
        () async {
      await _service.fetch('fish_2');
      await _service.fetch('fish_1');
      await _service.fetch('fish_3');

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));

      await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

      _logProcessingDorker.dispose();

      expect(_logStream, emitsThrough(smoucherRequestLog));
      expect(_logStream, neverEmits(contains('REMOTE')));
    });

    test('when doing new get but overlapped IDs from previous find', () async {
      final query = {
        'selector': {'category': 'land'},
        'limit': 2
      };
      await _service.find<Animal>(query);

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('dog_1'));
      expect(log, contains('dog_2'));

      await _service.fetch('dog_2');

      _logProcessingDorker.dispose();

      expect(_logStream, emitsThrough(smoucherRequestLog));
      expect(_logStream, neverEmits(contains('REMOTE')));
    });

    test('when doing new alldoc but overlapped IDs from previous find',
        () async {
      final query = {
        'selector': {'category': 'land'},
        'limit': 3
      };
      await _service.find<Animal>(query);

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('dog_1'));
      expect(log, contains('dog_2'));
      expect(log, contains('elephant_3'));

      await _service.fetchMultiple(['dog_1', 'dog_2', 'elephant_3']);

      _logProcessingDorker.dispose();

      expect(_logStream, emitsThrough(smoucherRequestLog));
      expect(_logStream, neverEmits(contains('REMOTE')));
    });

    test('when doing find with overlapped alldocs', () async {
      await _service.fetchMultiple(['dog_1', 'dog_2', 'elephant_3']);

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('dog_1'));
      expect(log, contains('dog_2'));
      expect(log, contains('elephant_3'));

      final query = {
        'selector': {'category': 'land'},
        'limit': 2
      };
      await _service.find<Animal>(query);

      _logProcessingDorker.dispose();

      expect(_logStream, emitsThrough(smoucherRequestLog));

      // There will be REMOTE call for FIND, but it should not result
      // in any REMOTE REPLICATE call since we had all the documents.
      expect(_logStream, neverEmits(contains('REMOTE REPLICATE')));
    });

    test('when doing same get after unrelated change', () async {
      await _service.fetch('fish_2');
      expect(_logStream, emitsThrough(smoucherRequestLog));

      // External changes made to fish_1
      await _testingDb.manipulateFish1();

      await _service.fetch('fish_2');
      _logProcessingDorker.dispose();

      // Run the stream all the way to the next request.
      expect(_logStream, emitsThrough(smoucherRequestLog));
      expect(_logStream, neverEmits(contains('REMOTE')));
    }, skip: 'Smoucher now wipe the local with any remote change');

    test('when doing same alldocs after unrelated change', () async {
      await _service.fetchMultiple(['dog_1', 'dog_2', 'elephant_3']);
      expect(_logStream, emitsThrough(smoucherRequestLog));

      // External changes made to fish_1 and fish_2
      await _testingDb.manipulateFish1();
      await _testingDb.manipulateFish2();

      await _service.fetchMultiple(['dog_1', 'dog_2', 'elephant_3']);
      _logProcessingDorker.dispose();

      // Run the stream all the way to the next request.
      expect(_logStream, emitsThrough(smoucherRequestLog));
      expect(_logStream, neverEmits(contains('REMOTE')));
    }, skip: 'Smoucher now wipe the local with any remote change');
  });

  group('Minimum replicate doc id', () {
    test('when doing alldocs with partial overlapped with previous get',
        () async {
      await _service.fetch('fish_1');
      await _service.fetch('fish_3');

      // Run the stream all the way to the skip both requests.
      expect(_logStream, emitsThrough(smoucherRequestLog));
      expect(_logStream, emitsThrough(smoucherRequestLog));

      await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
      _logProcessingDorker.dispose();

      // Run the stream all the way to the next request.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('fish_2'));
      expect(log, isNot(contains('fish_1')));
      expect(log, isNot(contains('fish_3')));
    });

    test('when doing alldocs with partial overlapped with previous allodocs',
        () async {
      await _service.fetchMultiple(['fish_1', 'fish_3']);

      // Run the stream all the way to the skip both requests.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
      _logProcessingDorker.dispose();

      // Run the stream all the way to the next request.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('fish_2'));
      expect(log, isNot(contains('fish_1')));
      expect(log, isNot(contains('fish_3')));
    });

    test('when doing alldocs with partial overlapped with previous find',
        () async {
      final query = {
        'selector': {'category': 'sea'},
        'limit': 2
      };
      await _service.find<Animal>(query);

      // Run the stream all the way to the skip both requests.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
      _logProcessingDorker.dispose();

      // Run the stream all the way to the next request.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('fish_3'));
      expect(log, isNot(contains('fish_1')));
      expect(log, isNot(contains('fish_2')));
    }, skip:'broken by new pagination');

    test(
        'when doing alldocs after changes partially updated few overlapped doc',
        () async {
      await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);

      // Run the stream all the way to the skip both requests.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      await _testingDb.manipulateFish1();
      await _testingDb.manipulateFish2();

      await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
      _logProcessingDorker.dispose();

      // Run the stream all the way to the next request.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('fish_1'));
      expect(log, contains('fish_2'));
      expect(log, isNot(contains('fish_3')));
    }, skip: 'Smoucher now wipe the local with any remote change');

    test(
        'when doing alldocs after overlapping find and changes partially updated few overlapped doc',
        () async {
      final query = {
        'selector': {'category': 'sea'},
        'limit': 3
      };
      await _service.find<Animal>(query);

      // Run the stream all the way to the skip both requests.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      await _testingDb.manipulateFish2();

      await _service.fetchMultiple(['fish_1', 'fish_2', 'fish_3']);
      _logProcessingDorker.dispose();

      // Run the stream all the way to the next request.
      expect(_logStream, emitsThrough(smoucherRequestLog));

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('fish_2'));
      expect(log, isNot(contains('fish_1')));
      expect(log, isNot(contains('fish_3')));
    }, skip: 'Smoucher now wipe the local with any remote change');

    test('when doing find after partially overlapped alldocs', () async {
      await _service.fetchMultiple(['dog_1', 'dog_2']);

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      var log = await _logStream.next;
      expect(log, contains('dog_1'));
      expect(log, contains('dog_2'));

      final query = {
        'selector': {'category': 'land'},
        'limit': 4
      };
      await _service.find<Animal>(query);

      _logProcessingDorker.dispose();

      expect(_logStream, emitsThrough(smoucherRequestLog));

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      log = await _logStream.next;
      expect(log, contains('elephant_3'));
      expect(log, contains('lion_4'));
      expect(log, isNot(contains('dog_1')));
      expect(log, isNot(contains('dog_2')));
    });

    test('when doing same find after changes partially updated few documents',
        () async {
      final query = {
        'selector': {'category': 'sea'},
        'limit': 3
      };
      await _service.find<Animal>(query);
      expect(_logStream, emitsThrough(smoucherRequestLog));

      await _testingDb.manipulateFish1();
      await _testingDb.manipulateFish2();

      await _service.find<Animal>(query);

      _logProcessingDorker.dispose();

      expect(_logStream, emitsThrough(smoucherRequestLog));

      expect(_logStream, emitsThrough(contains('REMOTE REPLICATE')));
      final log = await _logStream.next;
      expect(log, contains('fish_1'));
      expect(log, contains('fish_2'));
      expect(log, isNot(contains('fish_3')));
    }, skip: 'Smoucher now wipe the local with any remote change');
  });
}
