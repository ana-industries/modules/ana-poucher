@TestOn("browser")
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('module_test_race');
  SmoucherService _smoucherService;

  setUp(() async {
    var dorker = Dorker();
    Smoucher(Dorker.CrossLink(dorker));

    _smoucherService = SmoucherService(dorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.initService(_smoucherService);
  });

  tearDown(() async {
    await _smoucherService.destroy();
  });

  test('Create 10 data separately in one go', () async {
    await Future.wait(List.generate(10, (i) {
      var tiger = Animal((builder) => builder
        ..id = 'tiger_$i'
        ..category = 'tiger'
        ..isPet = true
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));
      return _smoucherService.upload(tiger);
    }));

    final results = await _smoucherService.find<Animal>({
      'selector': {'category': 'tiger'}
    });

    expect(results.results.length, 10);
  });

  test('Interlace creation, update and read', () async {
    final newTigers = List.generate(10, (i) {
      return Animal((builder) => builder
        ..id = 'tiger_$i'
        ..category = 'tiger'
        ..isPet = true
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));
    });

    final fishIds = ['fish_1', 'fish_2', 'fish_3'];
    final fishes = await _smoucherService.fetchMultiple<Animal>(fishIds);
    final updatedFishes = [
      fishes[0].rebuild((builder) => builder..isPet = false),
      fishes[1].rebuild(
          (builder) => builder..props['Name'] = JsonObject('Giant Shark'))
    ];

    try {
      await Future.wait([
        _smoucherService.upload(newTigers[0]),
        _smoucherService.fetch('house_1'),
        _smoucherService.bulkDocs(updatedFishes),
        _smoucherService.bulkDocs(newTigers.skip(1).take(3).toList()),
        _smoucherService.find<Animal>({
          'selector': {'category': 'land'}
        }),
      ]);

      final results = await _smoucherService.find<Animal>({
        'selector': {'category': 'tiger'}
      });

      expect(results.results.length, 4);
    } catch (e) {
      fail(e.toString());
    }
  });

  test('Interlace creation with reverts (cause by deleting creation)',
      () async {
    final tigers = List.generate(10, (i) {
      return Animal((builder) => builder
        ..id = 'tiger_$i'
        ..category = 'tiger'
        ..isPet = true
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));
    });

    // Create 4 first
    final newRevs = await Future.wait([
      _smoucherService.upload(tigers[0]),
      _smoucherService.upload(tigers[1]),
      _smoucherService.upload(tigers[2]),
      _smoucherService.upload(tigers[3]),
    ]);

    for (var i = 0; i < 4; i++) {
      tigers[i] = tigers[i].rebuild((b) => b.rev = newRevs[i]);
    }

    try {
      // Now interlace deletion of the previous 4 and creation of the rest
      await Future.wait([
        _smoucherService.upload(tigers[4]),
        _smoucherService.remove(tigers[0]),
        _smoucherService.upload(tigers[5]),
        _smoucherService.upload(tigers[6]),
        _smoucherService.remove(tigers[1]),
        _smoucherService.remove(tigers[2]),
        _smoucherService.upload(tigers[7]),
        _smoucherService.upload(tigers[8]),
        _smoucherService.upload(tigers[9]),
        _smoucherService.remove(tigers[3]),
      ]);

      final results = await _smoucherService.find<Animal>({
        'selector': {'category': 'tiger'}
      });

      expect(results.results.length, 6);
    } catch (e) {
      fail(e.toString());
    }
  });

  test('Interlace create, delete and read', () async {
    final tigers = List.generate(10, (i) {
      return Animal((builder) => builder
        ..id = 'tiger_$i'
        ..category = 'tiger'
        ..isPet = true
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));
    });

    // Create 4 first
    final newRevs = await Future.wait([
      _smoucherService.upload(tigers[0]),
      _smoucherService.upload(tigers[1]),
      _smoucherService.upload(tigers[2]),
      _smoucherService.upload(tigers[3]),
    ]);

    for (var i = 0; i < 4; i++) {
      tigers[i] = tigers[i].rebuild((b) => b.rev = newRevs[i]);
    }

    try {
      // Now interlace deletion of the previous 4 and creation of the rest
      await Future.wait([
        _smoucherService.upload(tigers[4]),
        _smoucherService.remove(tigers[0]),
        _smoucherService.fetch('fish_1'),
        _smoucherService.upload(tigers[5]),
        _smoucherService.upload(tigers[6]),
        _smoucherService.fetchMultiple(['fish_2', 'fish_3']),
        _smoucherService.remove(tigers[1]),
        _smoucherService.remove(tigers[2]),
        _smoucherService.upload(tigers[7]),
        _smoucherService.upload(tigers[8]),
        _smoucherService.find<Animal>({
          'selector': {'category': 'land'}
        }),
        _smoucherService.upload(tigers[9]),
        _smoucherService.remove(tigers[3]),
      ]);

      final results = await _smoucherService.find<Animal>({
        'selector': {'category': 'tiger'}
      });

      expect(results.results.length, 6);
    } catch (e) {
      fail(e.toString());
    }
  });

  test('Interlace revert and read', () async {
    // Create some changes for revert
    final fishIds = ['fish_1', 'fish_2', 'fish_3'];
    final fishes = await _smoucherService.fetchMultiple<Animal>(fishIds);
    final updatedFishes = [
      fishes[0].rebuild((builder) => builder..isPet = false),
      fishes[1].rebuild(
          (builder) => builder..props['Name'] = JsonObject('Giant Shark')),
      fishes[2].rebuild((builder) => builder..props['Leg'] = JsonObject(99))
    ];

    await _smoucherService.bulkDocs(updatedFishes);

    final changes = (await _smoucherService.fetchAllChanges()).results;

    try {
      await Future.wait([
        _smoucherService.fetchMultiple<Animal>(['dog_1', 'dog_2']),
        _smoucherService.revertChanges([changes[0]]),
        _smoucherService.revertChanges([changes[1]]),
        _smoucherService.find<Animal>({
          'selector': {'category': 'land'}
        }),
        _smoucherService.revertChanges([changes[2]]),
      ]);
    } catch (e) {
      fail(e.toString());
    }
  });

  // test('Interlace bulkDocs of mixed operation, with revert', () async {
  //   // Create some tigers
  //   final newTigers = List.generate(3, (i) {
  //     return Animal((builder) => builder
  //       ..id = 'tiger_$i'
  //       ..category = 'tiger'
  //       ..isPet = true
  //       ..props['leg'] = JsonObject(4)
  //       ..props['tail'] = JsonObject(1));
  //   });

  //   final newRevs = await Future.wait([
  //     _service.upload(newTigers[0]),
  //     _service.upload(newTigers[1]),
  //     _service.upload(newTigers[2]),
  //   ]);

  //   for (var i = 0; i < 3; i++) {
  //     newTigers[i] = newTigers[i].rebuild((b) => b.rev = newRevs[i]);
  //   }

  //   // Edit some fish
  //   final fishIds = ['fish_1', 'fish_2', 'fish_3'];
  //   final fishes = await _service.fetchMultiple<Animal>(fishIds);
  //   final updatedFishes = [
  //     fishes[0].rebuild((builder) => builder..isPet = false),
  //     fishes[1].rebuild(
  //         (builder) => builder..props['Name'] = JsonObject('Giant Shark')),
  //     fishes[2].rebuild((builder) => builder..props['Leg'] = JsonObject(99))
  //   ];

  //   await _service.bulkDocs(updatedFishes);

  //   final existingAnimals =
  //       await _service.fetchMultiple<Animal>(['dog_1', 'dog_2']);
  //   final existingBuildings =
  //       await _service.fetchMultiple<Building>(['house_1', 'house_2']);

  //   // Mix up all the operation in one go
  //   final firstChangingDocs = [
  //     existingAnimals[0]
  //         .rebuild((builder) => builder..props['Name'] = JsonObject('Maya')),
  //     existingBuildings[1].rebuild((builder) => builder..floor = 2),
  //     Building((builder) => builder
  //       ..id = 'hotel_3'
  //       ..floor = 1000),
  //     existingBuildings[0].rebuild((builder) => builder..deleted = true),
  //     existingAnimals[1].rebuild((builder) => builder..deleted = true),
  //     Animal((builder) => builder
  //       ..id = 'bird_1'
  //       ..category = 'air'
  //       ..isPet = true),
  //   ];
  // });

  // Future _revertThese(Iterable<String> ids) async {
  //   final changes = await _service.fetchAllChanges();
  //   final selected = changes.where((change) => ids.contains(change.docId));

  //   await _service.revertChanges(selected.toList());
  // }
}
