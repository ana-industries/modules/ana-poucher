@TestOn("browser")
import "package:async/async.dart";
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher.dart';
import 'package:ana_poucher/poucher_service_base.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

typedef bool WhereFunc<LatestChange>(change);

void main() {
  final _testingDb = TestingDb('test_change');
  SmoucherService _service;
  Dorker<String> _logProcessingDorker;
  Dorker<String> _loggingDorker;
  StreamQueue<String> _logStream;

  Future _revertThese(Iterable<String> ids) async {
    final changes = (await _service.fetchAllChanges()).results;
    final selected = changes.where((change) => ids.contains(change.docId));

    await _service.revertChanges(selected.toList());
  }

  // We do setup and tear down full service for each test in this group.
  setUp(() async {
    _logProcessingDorker = Dorker();
    _loggingDorker = Dorker.CrossLink(_logProcessingDorker);
    _logStream = StreamQueue<String>(_logProcessingDorker.onMessage);

    var dorker = Dorker();
    Smoucher(Dorker.CrossLink(dorker), _loggingDorker);

    _service = SmoucherService(dorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.initService(_service);

    // Uncomment next line to help debugging. It will print the log in console.
    //_logProcessingDorker.onMessage.listen(print);
  });

  tearDown(() async {
    _loggingDorker.dispose();
    _logProcessingDorker.dispose();
    await _service.destroy();
    await _logStream.cancel();
  });

  group('Revert create', () {
    final singleDocId = 'ant_1';
    final multiDocIds = [
      'bee_1',
      'bee_2',
      'bee_3',
    ];

    final insects = [
      Animal((builder) => builder
        ..id = singleDocId
        ..category = 'insect'
        ..isPet = false
        ..props['name'] = JsonObject('omega')
        ..props['leg'] = JsonObject(6)
        ..props['tail'] = JsonObject(1)),
      Animal((builder) => builder
        ..id = multiDocIds[0]
        ..category = 'insect'
        ..isPet = true
        ..props['name'] = JsonObject('alpha')
        ..props['leg'] = JsonObject(5)
        ..props['tail'] = JsonObject(4)),
      Animal((builder) => builder
        ..id = multiDocIds[1]
        ..category = 'insect'
        ..isPet = true
        ..props['name'] = JsonObject('bravo')
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(2)),
      Animal((builder) => builder
        ..id = multiDocIds[2]
        ..category = 'insect'
        ..isPet = true
        ..props['name'] = JsonObject('delta')
        ..props['leg'] = JsonObject(3)
        ..props['tail'] = JsonObject(0)),
    ];

    setUp(() async {
      // Create all the animals
      await _service.bulkDocs(insects);
    });

    test('then fail to fetch it', () async {
      await _revertThese([singleDocId]);

      expect(_service.fetch(singleDocId),
          throwsA(TypeMatcher<NotFoundException>()));
    });

    test('then fail to fetch the bunch', () async {
      await _revertThese(multiDocIds);

      for (var id in multiDocIds) {
        expect(_service.fetch(id), throwsA(TypeMatcher<NotFoundException>()));
      }
    });

    test('after doing one new update, then fail to fetch the bunch', () async {
      final bees = await _service.fetchMultiple<Animal>(multiDocIds);
      var updatedBees = bees
          .map((z) =>
              z.rebuild((builder) => builder..props['leg'] = JsonObject(2)))
          .toList();

      await _service.bulkDocs(updatedBees);

      await _revertThese(multiDocIds);

      for (var id in multiDocIds) {
        expect(_service.fetch(id), throwsA(TypeMatcher<NotFoundException>()));
      }
    });

    test('of a few, then only fail to fetch the those', () async {
      await _revertThese([multiDocIds[0], multiDocIds[2]]);

      expect(await _service.fetch(singleDocId), AnimalMatcher(insects[0]));
      expect(await _service.fetch(multiDocIds[1]), AnimalMatcher(insects[2]));
      expect(_service.fetch(multiDocIds[0]),
          throwsA(TypeMatcher<NotFoundException>()));
      expect(_service.fetch(multiDocIds[2]),
          throwsA(TypeMatcher<NotFoundException>()));
    });

    test('then fail to find it', () async {
      await _revertThese([singleDocId]);

      final results = await _service.find<Animal>({
        'selector': {'category': 'insect'}
      });

      expect(results.results.length, 3);
      for (var insect in results.results) {
        expect(insect, isNot(AnimalMatcher(insects[0])));
      }
    });

    test('then fail to find the bunch', () async {
      await _revertThese(multiDocIds);

      final results = await _service.find<Animal>({
        'selector': {'category': 'insect'}
      });

      expect(results.results.length, 1);
      expect(results.results.first, AnimalMatcher(insects[0]));
    });

    test('of a few, then only fail to find those', () async {
      await _revertThese([multiDocIds[0], multiDocIds[2]]);

      final results = await _service.find<Animal>({
        'selector': {'category': 'insect'}
      });

      expect(results.results.length, 2);
      expect(results.results[0], AnimalMatcher(insects[0]));
      expect(results.results[1], AnimalMatcher(insects[2]));
    });

    test('then create it again', () async {
      final reverted = await _service.fetch(singleDocId);

      await _revertThese([singleDocId]);

      await _service.upload(insects[0]);
      final newInsect = await _service.fetch<Animal>(singleDocId);

      expect(newInsect, AnimalMatcher(reverted));
    });

    test('then create the bunch again', () async {
      final reverted = await _service.fetchMultiple(multiDocIds);

      await _revertThese(multiDocIds);

      await _service.bulkDocs(insects.skip(1).toList());
      final newInsects = await _service.fetchMultiple(multiDocIds);

      for (var i = 0; i < newInsects.length; i++) {
        expect(newInsects[i], AnimalMatcher(reverted[i]));
      }
    });

    group('or delete a newly creation', () {
      Future _removeThese(List<String> ids) async {
        if (ids.length == 1) {
          final insect = await _service.fetch(ids.first);
          await _service.remove(insect);
        } else {
          final insects = await _service.fetchMultiple(ids);
          var updatedInsects = insects
              .map((z) => z.rebuild((builder) => builder..deleted = true))
              .toList();
          await _service.bulkDocs(updatedInsects);
        }
      }

      test('then fail to fetch it', () async {
        await _removeThese([singleDocId]);

        expect(_service.fetch(singleDocId),
            throwsA(TypeMatcher<NotFoundException>()));
      });

      test('then fail to fetch the bunch', () async {
        await _removeThese(multiDocIds);

        for (var id in multiDocIds) {
          expect(_service.fetch(id), throwsA(TypeMatcher<NotFoundException>()));
        }
      });

      test('after doing one new update, then fail to fetch the bunch',
          () async {
        final bees = await _service.fetchMultiple<Animal>(multiDocIds);
        var updatedBees = bees
            .map((z) =>
                z.rebuild((builder) => builder..props['leg'] = JsonObject(2)))
            .toList();

        await _service.bulkDocs(updatedBees);

        await _removeThese(multiDocIds);

        for (var id in multiDocIds) {
          expect(_service.fetch(id), throwsA(TypeMatcher<NotFoundException>()));
        }
      });

      test('of a few, then only fail to fetch the those', () async {
        await _removeThese([multiDocIds[0], multiDocIds[2]]);

        expect(await _service.fetch(singleDocId), AnimalMatcher(insects[0]));
        expect(await _service.fetch(multiDocIds[1]), AnimalMatcher(insects[2]));
        expect(_service.fetch(multiDocIds[0]),
            throwsA(TypeMatcher<NotFoundException>()));
        expect(_service.fetch(multiDocIds[2]),
            throwsA(TypeMatcher<NotFoundException>()));
      });

      test('then fail to find it', () async {
        await _removeThese([singleDocId]);

        final results = await _service.find<Animal>({
          'selector': {'category': 'insect'}
        });

        expect(results.results.length, 3);
        for (var insect in results.results) {
          expect(insect, isNot(AnimalMatcher(insects[0])));
        }
      });

      test('then fail to find the bunch', () async {
        await _removeThese(multiDocIds);

        final results = await _service.find<Animal>({
          'selector': {'category': 'insect'}
        });

        expect(results.results.length, 1);
        expect(results.results.first, AnimalMatcher(insects[0]));
      });

      test('of a few, then only fail to find those', () async {
        await _removeThese([multiDocIds[0], multiDocIds[2]]);

        final results = await _service.find<Animal>({
          'selector': {'category': 'insect'}
        });

        expect(results.results.length, 2);
        expect(results.results[0], AnimalMatcher(insects[0]));
        expect(results.results[1], AnimalMatcher(insects[2]));
      });

      test('then create it again', () async {
        final reverted = await _service.fetch(singleDocId);

        await _removeThese([singleDocId]);

        await _service.upload(insects[0]);
        final newInsect = await _service.fetch<Animal>(singleDocId);

        expect(newInsect, AnimalMatcher(reverted));
      });

      test('then create the bunch again', () async {
        final reverted = await _service.fetchMultiple(multiDocIds);

        await _removeThese(multiDocIds);

        await _service.bulkDocs(insects.skip(1).toList());
        final newInsects = await _service.fetchMultiple(multiDocIds);

        for (var i = 0; i < newInsects.length; i++) {
          expect(newInsects[i], AnimalMatcher(reverted[i]));
        }
      });
    });
  });

  group('Revert update', () {
    final singleDocId = 'elephant_3';
    final multiDocIds = [
      'dog_1',
      'dog_2',
      'lion_4',
      'lion_5',
    ];
    Animal originalSingle;
    List<Animal> originalMulti;

    setUp(() async {
      originalSingle = await _service.fetch(singleDocId);
      originalMulti = await _service.fetchMultiple(multiDocIds);

      var updatedSingle = originalSingle.rebuild((builder) => builder
        ..props['Tail'] = JsonObject(0)
        ..props['Head'] = JsonObject(2));

      var updatedMulti = originalMulti
          .map((animal) => animal.rebuild((builder) => builder
            ..props['Tail'] = JsonObject(2)
            ..props['Head'] = JsonObject(7)))
          .toList();

      await Future.wait(
          [_service.bulkDocs(updatedMulti), _service.upload(updatedSingle)]);
    });

    test('then fetch the remote copy', () async {
      await _revertThese([singleDocId]);

      final singleAgain = await _service.fetch(singleDocId);
      expect(singleAgain, AnimalMatcher(originalSingle));
    });

    test('then fetch the remote bunch', () async {
      await _revertThese(multiDocIds);

      final multiAgain = await _service.fetchMultiple(multiDocIds);
      expect(multiAgain, AnimalListMatcher(originalMulti));
    });

    test('after a couple of updates, then fetch the remote bunch', () async {
      final secondMulti = await _service.fetchMultiple(multiDocIds);
      var updatedMulti = secondMulti
          .map((z) =>
              z.rebuild((builder) => builder..props['leg'] = JsonObject(888)))
          .toList();

      await _service.bulkDocs(updatedMulti);

      await _revertThese(multiDocIds);

      final multiAgain = await _service.fetchMultiple(multiDocIds);
      expect(multiAgain, AnimalListMatcher(originalMulti));
    });

    test('of a few, then fetch the the correct bunch', () async {
      final secondMulti = await _service.fetchMultiple(multiDocIds);

      await _revertThese([multiDocIds[0], multiDocIds[2]]);

      final multiAgain = await _service.fetchMultiple(multiDocIds);
      expect(
          multiAgain,
          AnimalListMatcher([
            originalMulti[0],
            secondMulti[1],
            originalMulti[2],
            secondMulti[3]
          ]));
    });

    test('then find the remote copy', () async {
      await _revertThese([singleDocId]);

      final results = await _service.find<Animal>({
        'selector': {'category': 'land'}
      });

      expect(results.results.length, 5);
      expect(results.results[2], AnimalMatcher(originalSingle));
    });

    test('then find the remote bunch', () async {
      await _revertThese(multiDocIds);

      final untouchedSingle = await _service.fetch(singleDocId);
      final results = await _service.find<Animal>({
        'selector': {'category': 'land'}
      });

      expect(results.results.length, 5);
      expect(
          results.results,
          AnimalListMatcher([
            originalMulti[0],
            originalMulti[1],
            untouchedSingle,
            originalMulti[2],
            originalMulti[3],
          ]));
    });

    test('after a couple of updates, then find the correct bunch', () async {
      final secondMulti = await _service.fetchMultiple(multiDocIds);
      var updatedMulti = secondMulti
          .map((z) =>
              z.rebuild((builder) => builder..props['leg'] = JsonObject(888)))
          .toList();

      await _service.bulkDocs(updatedMulti);

      await _revertThese(multiDocIds);

      final untouchedSingle = await _service.fetch(singleDocId);
      final results = await _service.find<Animal>({
        'selector': {'category': 'land'}
      });

      expect(results.results.length, 5);
      expect(
          results.results,
          AnimalListMatcher([
            originalMulti[0],
            originalMulti[1],
            untouchedSingle,
            originalMulti[2],
            originalMulti[3],
          ]));
    });

    test('then do the same update again', () async {
      await _revertThese([singleDocId]);

      final refetchedSingle = await _service.fetch(singleDocId);

      var sameUpdate = refetchedSingle.rebuild((builder) => builder
        ..props['Tail'] = JsonObject(0)
        ..props['Head'] = JsonObject(2));
      await _service.upload(sameUpdate);

      final revertThenUpdatedSingle = await _service.fetch(singleDocId);
      expect(revertThenUpdatedSingle, AnimalMatcher(sameUpdate));
    });

    test('then do a different update', () async {
      await _revertThese([singleDocId]);

      var revertedSingle = await _service.fetch(singleDocId);
      var differentUpdate = revertedSingle.rebuild((builder) => builder
        ..isPet = false
        ..props['Name'] = JsonObject('Doggy'));
      await _service.upload(differentUpdate);

      final revertThenUpdatedSingle = await _service.fetch(singleDocId);
      expect(revertThenUpdatedSingle, AnimalMatcher(differentUpdate));
    });

    test('then do the same bunch of update again', () async {
      await _revertThese(multiDocIds);

      var refetchedMulti = await _service.fetchMultiple<Animal>(multiDocIds);
      var sameUpdates = refetchedMulti
          .map((animal) => animal.rebuild((builder) => builder
            ..props['Tail'] = JsonObject(2)
            ..props['Head'] = JsonObject(7)))
          .toList();
      await _service.bulkDocs(sameUpdates);

      final revertThenUpdatedMulti = await _service.fetchMultiple(multiDocIds);
      expect(revertThenUpdatedMulti, AnimalListMatcher(sameUpdates));
    });

    test('then do the a different bunch of update', () async {
      await _revertThese(multiDocIds);

      var refetchedMulti = await _service.fetchMultiple<Animal>(multiDocIds);
      var differentUpdates = refetchedMulti
          .map((animal) => animal
              .rebuild((builder) => builder..props['Leg'] = JsonObject(77)))
          .toList();
      await _service.bulkDocs(differentUpdates);

      final revertThenUpdatedMulti = await _service.fetchMultiple(multiDocIds);
      expect(revertThenUpdatedMulti, AnimalListMatcher(differentUpdates));
    });
  });

  group('Revert delete', () {
    final singleDocId = 'elephant_3';
    final multiDocIds = [
      'dog_1',
      'dog_2',
      'lion_4',
      'lion_5',
    ];
    Animal originalSingle;
    List<Animal> originalMulti;

    setUp(() async {
      originalSingle = await _service.fetch(singleDocId);
      originalMulti = await _service.fetchMultiple(multiDocIds);

      var updatedMulti = originalMulti
          .map((animal) => animal.rebuild((builder) => builder..deleted = true))
          .toList();

      await Future.wait(
          [_service.bulkDocs(updatedMulti), _service.remove(originalSingle)]);
    });

    test('then fetch the remote copy', () async {
      await _revertThese([singleDocId]);

      final singleAgain = await _service.fetch(singleDocId);
      expect(singleAgain, AnimalMatcher(originalSingle));
    });

    test('then fetch the remote bunch', () async {
      await _revertThese(multiDocIds);

      final multiAgain = await _service.fetchMultiple(multiDocIds);
      expect(multiAgain, AnimalListMatcher(originalMulti));
    });

    test('of a few, then fetch the the correct bunch', () async {
      await _revertThese([multiDocIds[0], multiDocIds[2]]);

      final multiAgain = await _service.fetchMultiple(multiDocIds);
      expect(multiAgain[0], AnimalMatcher(originalMulti[0]));
      expect(multiAgain[1], isNull);
      expect(multiAgain[2], AnimalMatcher(originalMulti[2]));
      expect(multiAgain[3], isNull);
    });

    test('then find the remote copy', () async {
      await _revertThese([singleDocId]);

      final results = await _service.find<Animal>({
        'selector': {'category': 'land'}
      });

      expect(results.results.length, 1);
      expect(results.results.first, AnimalMatcher(originalSingle));
    });

    test('then find the remote bunch', () async {
      await _revertThese(multiDocIds);

      final results = await _service.find<Animal>({
        'selector': {'category': 'land'}
      });

      expect(results.results.length, 4);
      expect(
          results.results,
          AnimalListMatcher([
            originalMulti[0],
            originalMulti[1],
            originalMulti[2],
            originalMulti[3],
          ]));
    });

    test('then do the same delete again', () async {
      await _revertThese([singleDocId]);

      final refetchedSingle = await _service.fetch(singleDocId);
      await _service.remove(refetchedSingle);

      expect(_service.fetch(singleDocId),
          throwsA(TypeMatcher<NotFoundException>()));
    });

    test('then do the same bunch of delete again', () async {
      await _revertThese(multiDocIds);

      final multiAgain = await _service.fetchMultiple(multiDocIds);
      var deletedMulti = multiAgain
          .map((animal) => animal.rebuild((builder) => builder..deleted = true))
          .toList();
      await _service.bulkDocs(deletedMulti);

      final revertThenUpdatedMulti = await _service.fetchMultiple(multiDocIds);
      for (var doc in revertThenUpdatedMulti) {
        expect(doc, isNull);
      }
    });
  });
}
