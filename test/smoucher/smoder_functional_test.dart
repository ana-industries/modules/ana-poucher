/// To test `Smoder` functionality.
/// Staging changes, providing diff with base
///

@TestOn("browser")
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  final _testingDb = TestingDb('smoder_functional_test');
  SmoucherService _smoucherService;
  Dorker<String> _logProcessingDorker;
  Dorker<String> _loggingDorker;

  setUp(() async {
    _logProcessingDorker = Dorker();
    _loggingDorker = Dorker.CrossLink(_logProcessingDorker);

    var smoucherDorker = Dorker();
    Smoucher(Dorker.CrossLink(smoucherDorker), _loggingDorker);
    _smoucherService = SmoucherService(smoucherDorker, standardSerializers);

    await _testingDb.prepareDb();
    await _testingDb.initService(_smoucherService);
    await _smoucherService.createIndexForChangeDoc();

    // Uncomment next line to help debugging. It will print the log in console.
    //_logProcessingDorker.onMessage.listen(print);
  });

  tearDown(() async {
    await _smoucherService.destroy();
  });

  group('Stage changes', () {
    test('when creating a new document', () async {
      final bird = Animal((builder) => builder
        ..id = 'bird_1'
        ..category = 'air'
        ..isPet = true);

      await _smoucherService.upload(bird);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, 1);
      expect(changes.first.operationType, OperationType.create);
      expect(changes.first.current, AnimalMatcher(bird));
    });

    test('when creating a bunch of new documents', () async {
      final birds = [
        Animal((builder) => builder
          ..id = 'bird_1'
          ..category = 'air'
          ..isPet = true),
        Animal((builder) => builder
          ..id = 'bird_2'
          ..category = 'air'
          ..isPet = false
          ..props['Name'] = JsonObject('Chocobo')),
        Animal((builder) => builder
          ..id = 'bird_3'
          ..category = 'air'
          ..isPet = true
          ..props['Name'] = JsonObject('Rio')),
      ];

      await _smoucherService.bulkDocs(birds);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, birds.length);
      for (var i = 0; i < birds.length; i++) {
        final change = changes[i];
        expect(change.operationType, OperationType.create);
        expect(change.current, AnimalMatcher(birds[i]));
      }
    });

    test('when updating a document', () async {
      var fish = await _smoucherService.fetch<Animal>('fish_1');
      fish = fish.rebuild((builder) => builder
        ..isPet = false
        ..props['Name'] = JsonObject('new fish'));

      await _smoucherService.upload(fish);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, 1);
      expect(changes.first.operationType, OperationType.update);
      expect(changes.first.current, AnimalMatcher(fish));
    });

    test('when updating a bunch of documents', () async {
      final fishes = await _smoucherService
          .fetchMultiple<Animal>(['fish_1', 'fish_2', 'fish_3']);
      final updatedFishes = [
        fishes[0].rebuild((builder) => builder..isPet = false),
        fishes[1].rebuild(
            (builder) => builder..props['Name'] = JsonObject('Giant Shark')),
        fishes[2].rebuild((builder) => builder..props['Leg'] = JsonObject(99))
      ];

      await _smoucherService.bulkDocs(updatedFishes);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, updatedFishes.length);
      for (var i = 0; i < updatedFishes.length; i++) {
        final change = changes[i];
        expect(change.operationType, OperationType.update);
        expect(changes[i].current, AnimalMatcher(updatedFishes[i]));
      }
    });

    test('when delete a document', () async {
      var fish = await _smoucherService.fetch<Animal>('fish_1');

      await _smoucherService.remove(fish);

      final changes = (await _smoucherService.fetchAllChanges()).results;
      final change = changes.first;

      expect(changes.length, 1);
      expect(change.operationType, OperationType.delete);
    });

    test('when delete a bunch of documents', () async {
      final fishes = await _smoucherService
          .fetchMultiple<Animal>(['fish_1', 'fish_2', 'fish_3']);
      final updatedFishes = fishes.map((fish) {
        return fish.rebuild((builder) => builder..deleted = true);
      }).toList();

      await _smoucherService.bulkDocs(updatedFishes);

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.length, updatedFishes.length);
      for (var change in changes) {
        expect(change.operationType, OperationType.delete);
      }
    });

    group('Pagination', () {
      Future _createABunchOfNewDoc(int count) async {
        final birds = List.generate(
            count,
            (index) => Animal((builder) => builder
              ..id = 'bird_$index'
              ..category = 'air'
              ..isPet = true));

        return _smoucherService.bulkDocs(birds);
      }

      test('No next page if total changes is less than limit', () async {
        final changeCount = 3;
        final limit = 5;
        await _createABunchOfNewDoc(changeCount);

        final result = await _smoucherService.fetchAllChanges(
            includeDoc: true, limit: limit);
        final changes = result.results;

        expect(changes.length, changeCount);
        expect(result.hasNextPage, isFalse);
      });

      test('Has 2 pages', () async {
        final changeCount = 8;
        final limit = 5;
        await _createABunchOfNewDoc(changeCount);

        var result = await _smoucherService.fetchAllChanges(
            includeDoc: true, limit: limit);
        var changes = result.results;

        expect(changes.length, limit);
        expect(result.hasNextPage, isTrue);

        // Next Page
        result = await _smoucherService.fetchAllChanges(
            includeDoc: true, limit: limit, skip: limit);
        changes = result.results;

        expect(changes.length, changeCount - limit);
        expect(result.hasNextPage, isFalse);
      });
    });
  });

  group('Order changes', () {
    test('by operation when all document type is the same', () async {
      // Get some existing document
      final existingDocs = await _smoucherService
          .fetchMultiple<Animal>(['dog_1', 'dog_2', 'elephant_3', 'lion_4']);

      // Mix up all the operation in one go
      final changingDocs = [
        existingDocs[0]
            .rebuild((builder) => builder..props['Name'] = JsonObject('Maya')),
        existingDocs[2].rebuild((builder) => builder..deleted = true),
        Animal((builder) => builder
          ..id = 'bird_1'
          ..category = 'air'
          ..isPet = true),
        existingDocs[1].rebuild((builder) => builder..deleted = true),
        Animal((builder) => builder
          ..id = 'bird_2'
          ..category = 'air'
          ..isPet = false
          ..props['Name'] = JsonObject('Chocobo')),
        existingDocs[3].rebuild((builder) => builder..isPet = true),
      ];

      await _smoucherService.bulkDocs(changingDocs);

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.length, changingDocs.length);
      expect(changes[0].operationType, OperationType.create);
      expect(changes[1].operationType, OperationType.create);
      expect(changes[2].operationType, OperationType.update);
      expect(changes[3].operationType, OperationType.update);
      expect(changes[4].operationType, OperationType.delete);
      expect(changes[5].operationType, OperationType.delete);
    });

    test('by type first then operation second', () async {
      final existingAnimals =
          await _smoucherService.fetchMultiple<Animal>(['dog_1', 'dog_2']);
      final existingBuildings = await _smoucherService
          .fetchMultiple<Building>(['house_1', 'house_2']);

      // Mix up all the operation in one go
      final changingDocs = [
        existingAnimals[0]
            .rebuild((builder) => builder..props['Name'] = JsonObject('Maya')),
        existingBuildings[1].rebuild((builder) => builder..floor = 2),
        Building((builder) => builder
          ..id = 'hotel_3'
          ..floor = 1000),
        existingBuildings[0].rebuild((builder) => builder..deleted = true),
        existingAnimals[1].rebuild((builder) => builder..deleted = true),
        Animal((builder) => builder
          ..id = 'bird_1'
          ..category = 'air'
          ..isPet = true),
      ];

      await _smoucherService.bulkDocs(changingDocs);

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.length, changingDocs.length);
      expect(changes[0].operationType, OperationType.create);
      expect(changes[1].operationType, OperationType.update);
      expect(changes[2].operationType, OperationType.delete);
      expect(changes[3].operationType, OperationType.create);
      expect(changes[4].operationType, OperationType.update);
      expect(changes[5].operationType, OperationType.delete);
    });
  });

  group('Stage subsequent changes', () {
    test('when create A, update B then delete C. 3 Changes.', () async {
      final bird = Animal((builder) => builder
        ..id = 'bird_1'
        ..category = 'air'
        ..isPet = true);

      await _smoucherService.upload(bird);

      var fish = await _smoucherService.fetch<Animal>('fish_1');
      fish = fish.rebuild((builder) => builder
        ..isPet = false
        ..props['Name'] = JsonObject('new fish'));

      await _smoucherService.upload(fish);

      var deletingFish = await _smoucherService.fetch<Animal>('fish_2');

      await _smoucherService.remove(deletingFish);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, 3);
      expect(changes[0].operationType, OperationType.create);
      expect(changes[0].current, AnimalMatcher(bird));
      expect(changes[1].operationType, OperationType.update);
      expect(changes[1].current, AnimalMatcher(fish));
      expect(changes[2].operationType, OperationType.delete);
    });

    test('when create A then update A, Still 1 Create change', () async {
      final bird = Animal((builder) => builder
        ..id = 'bird_1'
        ..category = 'air'
        ..isPet = true);

      final newRev = await _smoucherService.upload(bird);

      final secondBird = bird.rebuild((builder) => builder
        ..rev = newRev
        ..isPet = false);

      await _smoucherService.upload(secondBird);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, 1);
      expect(changes[0].operationType, OperationType.create);
      expect(changes[0].current, AnimalMatcher(secondBird));
    });

    test('when update A B, then update A delete B. 2 Changes', () async {
      final fishes =
          await _smoucherService.fetchMultiple<Animal>(['fish_1', 'fish_2']);
      final changingFishes = [
        fishes[0].rebuild((builder) => builder..isPet = false),
        fishes[1].rebuild(
            (builder) => builder..props['Name'] = JsonObject('Giant Shark'))
      ];

      await _smoucherService.bulkDocs(changingFishes);

      final updateFishes =
          await _smoucherService.fetchMultiple<Animal>(['fish_1', 'fish_2']);
      final changingFishes2 = [
        updateFishes[0]
            .rebuild((builder) => builder..props['Name'] = JsonObject('Saw')),
        updateFishes[1].rebuild((builder) => builder..deleted = true)
      ];

      await _smoucherService.bulkDocs(changingFishes2);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, 2);
      expect(changes[0].operationType, OperationType.update);
      expect(changes[0].current, AnimalMatcher(changingFishes2[0]));
      expect(changes[1].operationType, OperationType.delete);
    });

    test('when create A then delete A. 0 changes', () async {
      final bird = Animal((builder) => builder
        ..id = 'bird_1'
        ..category = 'air'
        ..isPet = true);

      await _smoucherService.upload(bird);
      final birdA = await _smoucherService.fetch(bird.id);
      await _smoucherService.remove(birdA);

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.isEmpty, isTrue);
    });

    test('when create A & B, then delete A. 1 changes', () async {
      final birds = [
        Animal((builder) => builder
          ..id = 'bird_1'
          ..category = 'air'
          ..isPet = true),
        Animal((builder) => builder
          ..id = 'bird_2'
          ..category = 'air'
          ..isPet = false
          ..props['Name'] = JsonObject('Chocobo'))
      ];

      await _smoucherService.bulkDocs(birds);
      final birdA = await _smoucherService.fetch('bird_1');
      await _smoucherService.remove(birdA);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, 1);
      expect(changes.first.operationType, OperationType.create);
      expect(changes.first.current, AnimalMatcher(birds[1]));
    });

    test('when create ABCD, then delete B & D. 2 changes', () async {
      final birds = [
        Animal((builder) => builder
          ..id = 'bird_1'
          ..category = 'air'
          ..isPet = true),
        Animal((builder) => builder
          ..id = 'bird_2'
          ..category = 'air'
          ..isPet = false
          ..props['Name'] = JsonObject('Chocobo')),
        Animal((builder) => builder
          ..id = 'bird_3'
          ..category = 'air'
          ..isPet = false
          ..props['Name'] = JsonObject('That bird')),
        Animal((builder) => builder
          ..id = 'bird_4'
          ..category = 'air'
          ..isPet = true
          ..props['Name'] = JsonObject('Angry'))
      ];

      await _smoucherService.bulkDocs(birds);

      final updatedBirds =
          await _smoucherService.fetchMultiple<Animal>(['bird_2', 'bird_4']);
      final deletedBirds = updatedBirds
          .map((bird) => bird.rebuild((builder) => builder..deleted = true))
          .toList();
      await _smoucherService.bulkDocs(deletedBirds);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, 2);
      expect(changes.first.operationType, OperationType.create);
      expect(changes.first.current, AnimalMatcher(birds[0]));
      expect(changes.last.operationType, OperationType.create);
      expect(changes.last.current, AnimalMatcher(birds[2]));
    });
  });

  group('Base document', () {
    test('is null for creation', () async {
      final bird = Animal((builder) => builder
        ..id = 'bird_1'
        ..category = 'air'
        ..isPet = true);

      await _smoucherService.upload(bird);

      final changes = (await _smoucherService.fetchAllChanges(
              includeDoc: true, includeBase: true))
          .results;
      expect(changes.first.base, isNull);
    });

    test(
        'is the same with the original from remote DB, different from latest, after few edition',
        () async {
      final originalFish = await _smoucherService.fetch<Animal>('fish_1');
      var newFish = originalFish.rebuild((builder) => builder
        ..isPet = false
        ..props['Name'] = JsonObject('new fish'));

      final newRev = await _smoucherService.upload(newFish);

      newFish = newFish.rebuild((builder) => builder
        ..rev = newRev
        ..category = "Sea animal"
        ..props['Name'] = JsonObject('newer fish'));

      await _smoucherService.upload(newFish);

      final changes = (await _smoucherService.fetchAllChanges(
              includeDoc: true, includeBase: true))
          .results;

      expect(changes.first.base, isNot(AnimalMatcher(changes.first.current)));
      expect(changes.first.base, AnimalMatcher(originalFish));
    });

    test('is the same with the original from remote DB, after first deletion',
        () async {
      var fish = await _smoucherService.fetch<Animal>('fish_1');

      await _smoucherService.remove(fish);

      final changes = (await _smoucherService.fetchAllChanges(
              includeDoc: true, includeBase: true))
          .results;
      final change = changes.first;

      expect(change.base, AnimalMatcher(fish));
    });

    group('after bulkDocs', () {
      var existingDocs;

      Future _revertThese(Iterable<String> ids) async {
        final changes = (await _smoucherService.fetchAllChanges()).results;
        final selected = changes.where((change) => ids.contains(change.docId));

        await _smoucherService.revertChanges(selected.toList());
      }

      setUp(() async {
        existingDocs = await _smoucherService
            .fetchMultiple<Animal>(['dog_1', 'dog_2', 'elephant_3', 'lion_4']);

        final changingDocs = [
          Animal((builder) => builder
            ..id = 'bird_1'
            ..category = 'air'
            ..isPet = true),
          Animal((builder) => builder
            ..id = 'bird_2'
            ..category = 'air'
            ..isPet = false
            ..props['Name'] = JsonObject('Chocobo')),
          existingDocs[0].rebuild(
              (builder) => builder..props['Name'] = JsonObject('Maya')),
          existingDocs[1].rebuild((builder) => builder..isPet = true),
          existingDocs[2].rebuild((builder) => builder..deleted = true),
          existingDocs[3].rebuild((builder) => builder..deleted = true),
        ];

        await _smoucherService.bulkDocs(changingDocs);
      });

      test('are all the same with the original from remote DB', () async {
        final changes = (await _smoucherService.fetchAllChanges(
                includeBase: true, includeDoc: true))
            .results;
        expect(changes[0].base, isNull);
        expect(changes[1].base, isNull);
        expect(changes[2].base, AnimalMatcher(existingDocs[0]));
        expect(changes[3].base, AnimalMatcher(existingDocs[1]));
        expect(changes[4].base, AnimalMatcher(existingDocs[2]));
        expect(changes[5].base, AnimalMatcher(existingDocs[3]));
      });

      test('are the same for those not reverted, and stable after a smod flush',
          () async {
        // Revert cause the smod DB to flush, hence will get rid of older version of doc
        // A bug found during smod UI cycle #1.
        await _revertThese([existingDocs[0].id, existingDocs[2].id]);

        final changes = (await _smoucherService.fetchAllChanges(
                includeBase: true, includeDoc: true))
            .results;
        expect(changes[0].base, isNull);
        expect(changes[1].base, isNull);
        expect(changes[2].base, AnimalMatcher(existingDocs[1]));
        expect(changes[3].base, AnimalMatcher(existingDocs[3]));
      });

      test('are the same for those not deleted, and stable after a smod flush',
          () async {
        // Revert cause the smod DB to flush, hence will get rid of older version of doc
        // A bug found during smod UI cycle #1.
        await _revertThese(['bird_2']);

        final changes = (await _smoucherService.fetchAllChanges(
                includeBase: true, includeDoc: true))
            .results;
        expect(changes[0].base, isNull);
        expect(changes[1].base, AnimalMatcher(existingDocs[0]));
        expect(changes[2].base, AnimalMatcher(existingDocs[1]));
        expect(changes[3].base, AnimalMatcher(existingDocs[2]));
        expect(changes[4].base, AnimalMatcher(existingDocs[3]));
      });
    });
  });

  group('Revert', () {
    Future _revertThese(Iterable<String> ids) async {
      final changes = (await _smoucherService.fetchAllChanges()).results;
      final selected = changes.where((change) => ids.contains(change.docId));

      await _smoucherService.revertChanges(selected.toList());
    }

    test('the new creation, no changes.', () async {
      final bird = Animal((builder) => builder
        ..id = 'bird_1'
        ..category = 'air'
        ..isPet = true);

      await _smoucherService.upload(bird);
      await _revertThese(['bird_1']);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.isEmpty, isTrue);
    });

    test('all new creation, no changes', () async {
      final birds = [
        Animal((builder) => builder
          ..id = 'bird_1'
          ..category = 'air'
          ..isPet = true),
        Animal((builder) => builder
          ..id = 'bird_2'
          ..category = 'air'
          ..isPet = false
          ..props['Name'] = JsonObject('Chocobo')),
        Animal((builder) => builder
          ..id = 'bird_3'
          ..category = 'air'
          ..isPet = true
          ..props['Name'] = JsonObject('Rio')),
      ];

      await _smoucherService.bulkDocs(birds);
      await _revertThese(birds.map((bird) => bird.id).toList());

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.isEmpty, isTrue);
    });

    test('some new creation, left other changes', () async {
      final birds = [
        Animal((builder) => builder
          ..id = 'bird_1'
          ..category = 'air'
          ..isPet = true),
        Animal((builder) => builder
          ..id = 'bird_2'
          ..category = 'air'
          ..isPet = false
          ..props['Name'] = JsonObject('Chocobo')),
        Animal((builder) => builder
          ..id = 'bird_3'
          ..category = 'air'
          ..isPet = true
          ..props['Name'] = JsonObject('Rio')),
      ];

      await _smoucherService.bulkDocs(birds);
      await _revertThese(['bird_1', 'bird_3']);

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, 1);
      expect(changes.first.current, AnimalMatcher(birds[1]));
    });

    test('updates of a document, 0 changes', () async {
      var fish = await _smoucherService.fetch<Animal>('fish_1');
      fish = fish.rebuild((builder) => builder
        ..isPet = false
        ..props['Name'] = JsonObject('new fish'));

      await _smoucherService.upload(fish);
      await _revertThese([fish.id]);

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.isEmpty, isTrue);
    });

    test('updates of a bunch of documents, 0 changes', () async {
      final fishIds = ['fish_1', 'fish_2', 'fish_3'];
      final fishes = await _smoucherService.fetchMultiple<Animal>(fishIds);
      final updatedFishes = [
        fishes[0].rebuild((builder) => builder..isPet = false),
        fishes[1].rebuild(
            (builder) => builder..props['Name'] = JsonObject('Giant Shark')),
        fishes[2].rebuild((builder) => builder..props['Leg'] = JsonObject(99))
      ];

      await _smoucherService.bulkDocs(updatedFishes);
      await _revertThese(fishIds);

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.isEmpty, isTrue);
    });

    test('some updates of a bunch of documents, left other changes', () async {
      final fishIds = ['fish_1', 'fish_2', 'fish_3'];
      final fishes = await _smoucherService.fetchMultiple<Animal>(fishIds);
      final updatedFishes = [
        fishes[0].rebuild((builder) => builder..isPet = false),
        fishes[1].rebuild(
            (builder) => builder..props['Name'] = JsonObject('Giant Shark')),
        fishes[2].rebuild((builder) => builder..props['Leg'] = JsonObject(99))
      ];

      await _smoucherService.bulkDocs(updatedFishes);
      await _revertThese(fishIds.skip(1));

      final changes =
          (await _smoucherService.fetchAllChanges(includeDoc: true)).results;
      expect(changes.length, 1);
      expect(changes.first.operationType, OperationType.update);
      expect(changes.first.current, AnimalMatcher(updatedFishes[0]));
    });

    test('the deleted, 0 changes', () async {
      var fish = await _smoucherService.fetch<Animal>('fish_1');

      await _smoucherService.remove(fish);
      await _revertThese([fish.id]);

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.isEmpty, isTrue);
    });

    test('all deletion, 0 changes', () async {
      final fishIds = ['fish_1', 'fish_2', 'fish_3'];
      final fishes = await _smoucherService.fetchMultiple<Animal>(fishIds);
      final updatedFishes = fishes.map((fish) {
        return fish.rebuild((builder) => builder..deleted = true);
      }).toList();

      await _smoucherService.bulkDocs(updatedFishes);
      await _revertThese(fishIds);

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.isEmpty, isTrue);
    });

    test('some deletion, left other changes', () async {
      final fishIds = ['fish_1', 'fish_2', 'fish_3'];
      final fishes = await _smoucherService.fetchMultiple<Animal>(fishIds);
      final updatedFishes = fishes.map((fish) {
        return fish.rebuild((builder) => builder..deleted = true);
      }).toList();

      await _smoucherService.bulkDocs(updatedFishes);
      await _revertThese(fishIds.take(2));

      final changes = (await _smoucherService.fetchAllChanges()).results;
      expect(changes.length, 1);
      expect(changes.first.operationType, OperationType.delete);
    });
  });
}
