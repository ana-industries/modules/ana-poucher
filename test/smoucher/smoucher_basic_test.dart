/// To test the basic functionality of `SmoucherService` with `Smoucher` as backend.
@TestOn("browser")

import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_poucher/poucher.dart';
import 'package:ana_poucher/poucher_service_base.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart';
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  test('Init and destroy', () async {
    var dorker = Dorker();
    Smoucher(Dorker.CrossLink(dorker));

    var service = SmoucherService(dorker, standardSerializers);
    try {
      await service.init('smoucher_test_init_remote',
          localDbName: 'smoucher_test_init_local',
          smodDbName: 'smoucher_test_init_smod');
    } catch (e) {
      fail('Error initing smoucher service. $e');
    }

    try {
      await service.destroy();
    } catch (e) {
      fail('Error destroying smoucher service. $e');
    }
  });

  group('CRUD', () {
    final _testingDb = TestingDb('module_test_crud');
    SmoucherService _service;

    setUp(() async {
      var dorker = Dorker();
      Smoucher(Dorker.CrossLink(dorker));

      _service = SmoucherService(dorker, standardSerializers);

      await _testingDb.prepareDb();
      await _testingDb.initService(_service);
    });

    tearDown(() async {
      await _service.destroy();
    });

    test('Create a new doc', () async {
      var tiger = Animal((builder) => builder
        ..id = 'tiger_1'
        ..category = 'land'
        ..isPet = true
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));

      try {
        var newRev = await _service.upload(tiger);
        expect(newRev, isNotNull);
      } catch (e) {
        fail('Failed to create new doc $tiger. $e');
      }
    });

    test('Create then Get back the doc', () async {
      var tiger = Animal((builder) => builder
        ..id = 'tiger_1'
        ..category = 'land'
        ..isPet = false
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));

      var newRev = await _service.upload(tiger);
      tiger = tiger.rebuild((builder) => builder..rev = newRev);

      var tigerFromService = await _service.fetch('tiger_1');

      expect(tigerFromService, equals(tiger));
    });

    test('Create the same thing again should throw Conflict', () async {
      var tiger = Animal((builder) => builder
        ..id = 'tiger_1'
        ..category = 'land'
        ..isPet = false
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));

      await _service.upload(tiger);

      expect(_service.upload(tiger), throwsA(TypeMatcher<ConflictException>()));
    });

    test('Create a bunch then get them back', () async {
      var tigers = [
        Animal((builder) => builder
          ..id = 'tiger-1'
          ..category = 'Mammal'
          ..isPet = false
          ..props['leg'] = JsonObject(4)
          ..props['tail'] = JsonObject(1)),
        Animal((builder) => builder
          ..id = 'tiger-2'
          ..category = 'Mammal'
          ..isPet = true
          ..props['leg'] = JsonObject(3)
          ..props['tail'] = JsonObject(2)),
      ];

      final bulkDocRespond = await _service.bulkDocs(tigers);
      for (var i = 0; i < tigers.length; i++) {
        tigers[i] = tigers[i]
            .rebuild((builder) => builder..rev = bulkDocRespond[i].rev);
      }

      final fetchAgainTigers =
          await _service.fetchMultiple<Animal>(['tiger-1', 'tiger-2']);

      expect(fetchAgainTigers, tigers);
    });

    test('Get an existing document', () async {
      var goldFish = await _service.fetch('fish_1');

      expect(
          goldFish,
          AnimalMatcher(_testingDb.initialAnimals
              .firstWhere((data) => data.id == 'fish_1')));
    });

    test('Get a bunch of existing documents', () async {
      var fishes = await _service.fetchMultiple(['fish_1', 'fish_2']);

      fishes.forEach((fish) => expect(
          fish,
          AnimalMatcher(_testingDb.initialAnimals
              .firstWhere((data) => data.id == fish.id))));
    });

    test('Get non-existing document will throw NotFoundException', () async {
      expect(_service.fetch('missing_doc_id'),
          throwsA(TypeMatcher<NotFoundException>()));
    });

    test('Update an existing document', () async {
      final goldFish = await _service.fetch<Animal>('fish_1');
      var updatedGoldFish = goldFish
          .rebuild((builder) => builder..props['Name'] = JsonObject('Bronzy'));

      final newRev = await _service.upload(updatedGoldFish);
      updatedGoldFish =
          updatedGoldFish.rebuild((builder) => builder..rev = newRev);

      final fetchAgainGoldFish = await _service.fetch('fish_1');

      expect(fetchAgainGoldFish, updatedGoldFish);
    });

    test('Update a bunch of existing document', () async {
      final fishes = await _service.fetchMultiple<Animal>(['fish_1', 'fish_2']);
      var updatedFishes = fishes
          .map((fish) =>
              fish.rebuild((builder) => builder..props['Tail'] = JsonObject(2)))
          .toList();

      final bulkDocRespond = await _service.bulkDocs(updatedFishes);
      for (var i = 0; i < updatedFishes.length; i++) {
        updatedFishes[i] = updatedFishes[i]
            .rebuild((builder) => builder..rev = bulkDocRespond[i].rev);
      }

      final fetchAgainFishes =
          await _service.fetchMultiple<Animal>(['fish_1', 'fish_2']);

      expect(fetchAgainFishes, updatedFishes);
    });

    test('Delete an existing document', () async {
      final goldFish = await _service.fetch<Animal>('fish_1');
      await _service.remove(goldFish);

      expect(
          _service.fetch('fish_1'), throwsA(TypeMatcher<NotFoundException>()));
    });

    test('Delete a bunch of existing documents', () async {
      final fishes = await _service.fetchMultiple<Animal>(['fish_1', 'fish_2']);
      var updatedFishes = fishes
          .map((fish) => fish.rebuild((builder) => builder..deleted = true))
          .toList();

      await _service.bulkDocs(updatedFishes);

      expect(
          _service.fetch('fish_1'), throwsA(TypeMatcher<NotFoundException>()));
      expect(
          _service.fetch('fish_2'), throwsA(TypeMatcher<NotFoundException>()));
    });
  });
}
