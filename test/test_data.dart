import 'package:built_value/serializer.dart';

import 'package:ana_metadata/metadata.dart';

import 'package:test/test.dart';

part 'test_data.g.dart';

abstract class Animal implements Propsy, Built<Animal, AnimalBuilder> {
  String get category;
  bool get isPet;

  static Serializer<Animal> get serializer => _$animalSerializer;
  factory Animal([updates(AnimalBuilder b)]) = _$Animal;
  Animal._();
}

abstract class GameHero implements Propsy, Built<GameHero, GameHeroBuilder> {
  String get name;
  String get series;
  int get debut;

  static Serializer<GameHero> get serializer => _$gameHeroSerializer;

  factory GameHero([updates(GameHeroBuilder b)]) = _$GameHero;
  GameHero._();
}

/// AnimalMatcher match Animal document without taking `rev` into account
class AnimalMatcher extends Matcher {
  final Animal _expected;

  AnimalMatcher(this._expected);

  Description describe(Description description) =>
      description.addDescriptionOf(_expected);

  bool matches(item, Map matchState) {
    final actual = item as Animal;

    final noRevConflictActual = actual.rebuild((builder) => builder
      ..rev = null
      ..conflicts = null);
    final noRevConflictExpected = _expected.rebuild((builder) => builder
      ..rev = null
      ..conflicts = null);

    return noRevConflictExpected == noRevConflictActual;
  }
}

class AnimalListMatcher extends Matcher {
  final List<Animal> _expected;

  AnimalListMatcher(this._expected);

  Description describe(Description description) =>
      description.addDescriptionOf(_expected);

  bool matches(item, Map matchState) {
    final actual = item.cast<Animal>();

    if (_expected.length != actual.length) {
      return false;
    }

    for (var i = 0; i < _expected.length; i++) {
      final matcher = AnimalMatcher(_expected[i]);
      if (!matcher.matches(actual[i], matchState)) {
        return false;
      }
    }

    return true;
  }
}

abstract class Building implements Propsy, Built<Building, BuildingBuilder> {
  int get floor;

  static Serializer<Building> get serializer => _$buildingSerializer;
  factory Building([updates(BuildingBuilder b)]) = _$Building;
  Building._();
}
