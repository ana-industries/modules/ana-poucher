/// To test the basic functionality of `PoucherServiceBase`
@TestOn("browser")

import 'package:built_value/standard_json_plugin.dart';
import 'package:test/test.dart';

import 'package:ana_metadata/metadata.dart';
import 'package:ana_metadata/filter.dart';

import 'package:ana_poucher/src/poucher_doc.dart';
import 'package:ana_poucher/src/serializer.dart' as poucher_serializer;
import 'package:ana_poucher/poucher.dart';
import 'package:ana_poucher/poucher_service_base.dart';
import 'package:dorker/dorker.dart';

import '../serializer.dart' as test_serializer;
import '../test_data.dart';
import '../testing_db.dart';

void main() {
  // 1. Get Poucher `Serializers` `poucher_serializer.serializers` (from `poucher_doc.dart`).
  //    Get a `SerializersBuilder` (using `toBuilder()`. Now we can add on other serializers!
  // 2. Next get the test_data `serializers` (`test_serializer.serializers`).
  //    Use the `serializers` property to get an `Iterable<Serializer>`s
  // 3. Pass #2's result this to addAll().
  // 4. Finally add plugins and call `build()` to get back to the `Serializers` type.
  final standardSerializers = (test_serializer.serializers.toBuilder()
        ..addAll(poucher_serializer.serializers.serializers)
        ..addPlugin(StandardJsonPlugin(discriminator: 'type')))
      .build();

  test('Init and destroy', () async {
    var dorker = Dorker();
    Poucher(Dorker.CrossLink(dorker));

    var service = PoucherServiceBase(dorker, standardSerializers);
    try {
      await service.init('poucher_module_test_init');
    } catch (e) {
      fail('Error initing poucher service. $e');
    }

    try {
      await service.destroy();
    } catch (e) {
      fail('Error destroying poucher service. $e');
    }
  });

  group('CRUD', () {
    final _testingDb = TestingDb('test_poucher');

    PoucherServiceBase _service;

    setUp(() async {
      var dorker = Dorker();
      Poucher(Dorker.CrossLink(dorker));

      _service = PoucherServiceBase(dorker, standardSerializers);

      await _testingDb.prepareDb();
      await _testingDb.initService(_service);
    });

    tearDown(() async {
      await _service.destroy();
    });

    test('Create a new document', () async {
      final id = 'bear_1';

      final dog = Animal((builder) => builder
        ..id = id
        ..category = 'Mammal'
        ..isPet = true
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));

      try {
        final newRev = await _service.upload<Animal>(dog);
        expect(newRev, isNotNull);
      } catch (e) {
        fail('Failed to create new doc $dog. $e');
      }
    });

    test('Create a new document with full response', () async {
      final id = 'spider_full_1';

      final spider = Animal((builder) => builder
        ..id = id
        ..category = 'Arachnid'
        ..isPet = true
        ..props['leg'] = JsonObject(4)
        ..props['tail'] = JsonObject(1));

      try {
        await _service.uploadFullResp<Animal>(spider);
      } catch (e) {
        fail('Failed to create new with full response doc $spider. $e');
      }
    });

    test('Get an existing document', () async {
      final id = 'spider_1';
      try {
        final spider = await _service.fetch<Animal>(id);

        expect(spider, isNotNull);

        expect(
            spider,
            AnimalMatcher(
                _testingDb.initialAnimals.firstWhere((data) => data.id == id)));
      } catch (e) {
        fail('Failed to get existing doc $e');
      }
    });

    test('Update an existing document', () async {
      final id = 'spider_2';
      var updatedSpider;
      try {
        final spider = await _service.fetch<Animal>(id);

        updatedSpider = spider.rebuild(
            (builder) => builder..props['Name'] = JsonObject('Hairy Wincy'));
      } catch (e) {
        fail('Failed to get existing doc $e');
      }

      try {
        final newRev = await _service.upload<Animal>(updatedSpider);
        expect(newRev, isNotNull);

        updatedSpider =
            updatedSpider.rebuild((builder) => builder..rev = newRev);
      } catch (e) {
        fail('Failed to update existing doc $e');
      }

      try {
        final fetchedAgainSpider = await _service.fetch<Animal>(id);
        expect(fetchedAgainSpider, updatedSpider);
      } catch (e) {
        fail('Failed to get existing doc $e');
      }
    });

    test('Bad id for conflict exception', () async {
      final id = 'spider_1';
      try {
        final spiderFromService = await _service.fetch<Animal>(id);

        final spider = spiderFromService.rebuild((builder) => builder
          ..id = 'bad id'
          ..props['Tail'] = JsonObject(55));

        expect(_service.upload<Animal>(spider),
            throwsA(TypeMatcher<ConflictException>()));
      } catch (e) {
        fail('Failed Bad id for conflict exception $e');
      }
    });

    test('Delete an existing document', () async {
      try {
        final id = 'spider_3';

        final spider = await _service.fetch<Animal>(id);
        final response = await _service.remove(spider);
        expect(response, isNull);

        expect(_service.fetch(id), throwsA(TypeMatcher<NotFoundException>()));
      } catch (e) {
        fail('Failed Delete an existing document $e');
      }
    });

    test('Remove a doc (BadRequestException)', () async {
      try {
        final id = 'spider_1';
        final spiderFromService = await _service.fetch<Animal>(id);

        // Test for BadRequestException
        final spiderToRemove =
            spiderFromService.rebuild((builder) => builder..rev = '124321');

        expect(_service.remove(spiderToRemove),
            throwsA(TypeMatcher<BadRequestException>()));
      } catch (e) {
        fail('Failed Remove a doc $e');
      }
    });

    test('remove a doc (ConflictException)', () async {
      final id = 'spider_1';

      try {
        final spiderFromService = await _service.fetch<Animal>(id);

        // Test for ConflictException
        final spiderToRemove = spiderFromService.rebuild(
            (builder) => builder..rev = '11-1b455bd612490ebc056e01e94b24400d');

        expect(_service.remove(spiderToRemove),
            throwsA(TypeMatcher<ConflictException>()));
      } catch (e) {
        fail('Failed to fetch doc $e');
      }
    });

    test('bulk create (bulkdDocs())', () async {
      try {
        final id0 = 'mussel_1';
        final id1 = 'mussel_2';
        final id2 = 'mussel_3';

        final mussel1 = Animal((builder) => builder
          ..id = id0
          ..category = 'Mollusc'
          ..isPet = false
          ..props['Name'] = JsonObject('apple')
          ..props['Tail'] = JsonObject(0)
          ..props['Leg'] = JsonObject(0));

        final mussel2 = Animal((builder) => builder
          ..id = id1
          ..category = 'Mollusc'
          ..isPet = false
          ..props['Name'] = JsonObject('banana')
          ..props['Leg'] = JsonObject(0)
          ..props['Tail'] = JsonObject(0));

        var mussels = [mussel1, mussel2];

        var bulkDocResponds = await _service.bulkDocs(mussels);
        /* bulkDocResponds =
        [{ok: true, id: mussel_1, rev: 1-2f8d9e3a4ec9d958ed0de20ee2db16e9}, 
         {ok: true, id: mussel_2, rev: 1-53074bea813d3a16886a6b9304ea4712}]
        */

        expect(bulkDocResponds[0].ok, isTrue);
        expect(bulkDocResponds[0].id, equals(id0));

        expect(bulkDocResponds[1].ok, isTrue);
        expect(bulkDocResponds[1].id, equals(id1));

        // bulk create again, expect error if id is used before (spider-1)
        final mussel3 = mussel2.rebuild((builder) => builder
          ..props['Name'] = JsonObject('cucumber')
          ..id = id2);

        mussels = [mussel1, mussel3];

        bulkDocResponds = await _service.bulkDocs(mussels);
        /*
        [{ok: false, id: mussel_1, rev: null}, 
         {ok: true, id: mussel_3, rev: 1-3c7ea1c13c47d14e4d36bf91082e9dc8}]
        */

        expect(bulkDocResponds[0].ok, isFalse);
        expect(bulkDocResponds[0].id, equals(id0));

        expect(bulkDocResponds[1].ok, isTrue);
        expect(bulkDocResponds[1].id, equals(id2));
      } catch (e) {
        fail('Error: $e');
      }
    });

    test('Get a bunch of existing documents', () async {
      try {
        final ids = ['spider_1', 'spider_2'];

        // allDocs()
        final docs = await _service.fetchMultiple<Animal>(ids);

        docs.forEach((doc) => expect(
            doc,
            AnimalMatcher(_testingDb.initialAnimals
                .firstWhere((data) => data.id == doc.id))));
      } catch (e) {
        fail('Error: $e');
      }
    });

    test('Get a bunch of existing documents revisions', () async {
      try {
        final ids = ['spider_1', 'spider_2'];

        final docs = await _service.fetchMultiple<Animal>(ids);

        final docsRevOnly = await _service.fetchMultipleRevisionOnly(ids);
        /*
        {spider_1: 1-4c96873101df75bcb640250dc661d118,
         spider_2: 1-2ad422f0e4aeb69a1224cb8a3d4c12b5}
        */
        expect(docsRevOnly.length, docs.length);
        for (var i = 0; i < docs.length; i++) {
          expect(docsRevOnly.containsKey(docs[i].id), isTrue);
          expect(docsRevOnly[docs[i].id], docs[i].rev);
        }
      } catch (e) {
        fail('Error: $e');
      }
    });

    test('Update a bunch of existing documents', () async {
      try {
        // read
        final ids = ['spider_4', 'spider_5'];

        final docs = await _service.fetchMultiple<Animal>(ids);

        final spider1New = docs[0].rebuild((builder) => builder
          ..props['Name'] = JsonObject('Cool Wincy')
          ..isPet = true
          ..props['Tail'] = JsonObject(1));

        final spider2New = docs[1].rebuild((builder) => builder
          ..props['Name'] = JsonObject('Dazed Wincy')
          ..isPet = true
          ..props['Leg'] = JsonObject(16));

        final newDocs = [spider1New, spider2New];

        // update
        final bulkDocResponds = await _service.bulkDocs(newDocs);
        /*
        [{ok: true, id: spider_4, rev: 2-f3ea9ab5a5ecdbf4c5558c7341c173df},
         {ok: true, id: spider_5, rev: 2-eee42b0d1676877679880a9cfb7c9b2e}]
        */

        for (var i = 0; i < newDocs.length; i++) {
          newDocs[i] = newDocs[i]
              .rebuild((builder) => builder..rev = bulkDocResponds[i].rev);
        }

        // read
        final fetchedAgainDocs = await _service.fetchMultiple<Animal>(ids);
        expect(fetchedAgainDocs, newDocs);
      } catch (e) {
        fail('Error: $e');
      }
    });

    test('bulk delete (bulkdDocs())', () async {
      try {
        final id0 = 'spider_4';
        final id1 = 'spider_5';

        final ids = [id0, id1];

        var docs = await _service.fetchMultiple<Animal>(ids);

        final updatedDocs = docs
            .map((doc) => doc.rebuild((builder) => builder..deleted = true))
            .toList();

        // bulk delete
        final bulkDocResponds = await _service.bulkDocs(updatedDocs);
        /*
        [{ok: true, id: spider_4, rev: 2-8d4f27f3d19394633696321b2e1f3067}, 
         {ok: true, id: spider_5, rev: 2-6c8d91f3539b6ea3bd928f890cc21a8a}]
        */
        bulkDocResponds.forEach((doc) {
          expect(doc.ok, isTrue);
        });

        // fetch should throw NotFoundException
        ids.forEach((id) {
          expect(_service.fetch(id), throwsA(TypeMatcher<NotFoundException>()));
        });

        // fetchMultiple should return nulls
        docs = await _service.fetchMultiple<Animal>(ids);

        expect(docs[0], isNull);
        expect(docs[1], isNull);
      } catch (e) {
        fail('Error: $e');
      }
    });

    group('generateQueryString', () {
      final type = 'Equipment';
      final parent = "props";
      final AtlasCopco = 'Atlas Copco';
      final ZH350 = 'ZH-350';
      final PlantCX = 'PlantCX';

      final filter = [
        Filter('Make', FilterType.selection, AtlasCopco, parent: parent),
        Filter('Model', FilterType.selection, ZH350, parent: parent),
        Filter('Plant', FilterType.selection, PlantCX)
      ];

      test('correct selector for type and filter', () async {
        Map<String, dynamic> query = _service.generateQueryString(type, filter);

        expect(query['selector']['type'], isNotNull);
        expect(query['selector']['type'], type);

        expect(query['selector']['props.Make'], isNotNull);
        expect(query['selector']['props.Make'], {'\$eq': AtlasCopco});
        expect(query['selector']['props.Model'], isNotNull);
        expect(query['selector']['props.Model'], {'\$eq': ZH350});
        expect(query['selector']['Plant'], isNotNull);
        expect(query['selector']['Plant'], {'\$eq': PlantCX});

        expect(query['limit'], isNull);
        expect(query['skip'], isNull);
        expect(query['sort'], isNull);
      });

      test('correct skip and limit', () async {
        final limit = 4;
        final skip = 3;
        Map<String, dynamic> query =
            _service.generateQueryString(type, null, limit: limit, skip: skip);

        expect(query['limit'], limit);
        expect(query['skip'], skip);
        expect(query['sort'], isNull);
      });

      test('correct sort', () async {
        Map<String, dynamic> query =
            _service.generateQueryString(type, filter, sortingFilters: filter);

        expect(query['sort'], filter.map((f) => f.selector));
      });

      test('throw if selecting filter does not contain all item in sort',
          () async {
        final incomplete = [
          Filter('Make', FilterType.selection, AtlasCopco, parent: parent),
          Filter('Plant', FilterType.selection, PlantCX)
        ];

        // Wrap the call into another function because that's what `throwsA` needs.
        expect(
            () => _service.generateQueryString(type, incomplete,
                sortingFilters: filter),
            throwsA(TypeMatcher<BadRequestException>()));
      });
    });

    test("test find single GameHero) ",
        () async {
      try {
        // sort by id
        final request = {
          'selector': {
            '_id': {'\$eq': 'pikachu'},
            'type': {'\$eq': 'GameHero'}
          },
        };

        final response = await _service.find<GameHero>(request);
        expect(response, isNotNull);

        final results = response.results;
        var numOfItems = results.length;
        expect(numOfItems, 1);

        expect(results[0].id, 'pikachu');

      } catch (e) {
        fail('Error: $e');
      }
    });

    test("test find() all `_id`'s >= 'dk', sorted by id (default index)",
        () async {
      try {
        // sort by id
        final request = {
          'selector': {
            '_id': {'\$gte': 'dk'},
            'type': {'\$eq': 'GameHero'}
          },
          'sort': ['_id']
        };

        final response = await _service.find<GameHero>(request);
        final results = response.results;

        var i = 0;
        var idsSeen = [];

        expect(results.length, isPositive);

        results.forEach((result) {
          var currentId = result.id;
          /*
          For each iteration:
          a. Test to see that currentId > all the ids seen in the past idsSeen.
          b. add() the currentId to the idsSeen list.
          c. Repeat.

          num, currentId, idsSeen
          0. dk, []
          1. falcon, [dk]
          2. fox, [dk, falcon]
          3. kirby, [dk, falcon, fox]
          4. link, [dk, falcon, fox, kirby]
          5. luigi, [dk, falcon, fox, kirby, link]
          6. mario, [dk, falcon, fox, kirby, link, luigi]
          7. ness, [dk, falcon, fox, kirby, link, luigi, mario]
          8. pikachu, [dk, falcon, fox, kirby, link, luigi, mario, ness]
          9. puff, [dk, falcon, fox, kirby, link, luigi, mario, ness, pikachu]
          10. samus, [dk, falcon, fox, kirby, link, luigi, mario, ness, pikachu, puff]
          11. yoshi, [dk, falcon, fox, kirby, link, luigi, mario, ness, pikachu, puff, samus]
          */

          if (i > 0) {
            idsSeen.forEach((idSeen) {
              expect(currentId.compareTo(idSeen), isNonNegative);
            });
          }

          idsSeen.add(currentId);

          i++;
        });
      } catch (e) {
        fail('Error: $e');
      }
    });

    test('test find() sort by name', () async {
      try {
        // create a binary tree of name entries
        await _service.createIndex('index', ['name']);

        // Using the $gt fails if we don't have all documents with the 'name'
        // field.
        // In this case have to use $gte for null (lowest value for CouchDB
        // collation order)

        final request = {
          'selector': {
            'name': {'\$gte': null},
            'type': {'\$eq': 'GameHero'}
          },
          'sort': ['name']
        };

        // sort by name
        final response = await _service.find<GameHero>(request);

        final results = response.results;
        expect(results.length, isPositive);

        var i = 0;
        var namesSeen = [];

        results.forEach((result) {
          var currentName = result.name;

          /*
          For each iteration:
          a. Test to see that currentName > all the names seen in the past 
            namesSeen.
          b. add() the currentName to the namesSeen list.
          c. Repeat.

          Here is what it looks like:

          0. Captain Falcon, []
          1. Donkey Kong, [Captain Falcon]
          2. Fox, [Captain Falcon, Donkey Kong]
          3. Jigglypuff, [Captain Falcon, Donkey Kong, Fox]
          4. Kirby, [Captain Falcon, Donkey Kong, Fox, Jigglypuff]
          5. Link, [Captain Falcon, Donkey Kong, Fox, Jigglypuff, Kirby]
          6. Luigi, [Captain Falcon, Donkey Kong, Fox, Jigglypuff, Kirby, Link]
          7. Mario, [Captain Falcon, Donkey Kong, Fox, Jigglypuff, Kirby, Link, Luigi]
          8. Ness, [Captain Falcon, Donkey Kong, Fox, Jigglypuff, Kirby, Link, Luigi, Mario]
          9. Pikachu, [Captain Falcon, Donkey Kong, Fox, Jigglypuff, Kirby, Link, Luigi, Mario, Ness]
          10. Samus, [Captain Falcon, Donkey Kong, Fox, Jigglypuff, Kirby, Link, Luigi, Mario, Ness, Pikachu]
          11. Yoshi, [Captain Falcon, Donkey Kong, Fox, Jigglypuff, Kirby, Link, Luigi, Mario, Ness, Pikachu, Samus]
          */

          if (i > 0) {
            namesSeen.forEach((nameSeen) {
              expect(currentName.compareTo(nameSeen), isNonNegative);
            });
          }

          namesSeen.add(currentName);

          i++;
        });
      } catch (e) {
        fail('Error: $e');
      }
    });

    test('test find() sort by Debut', () async {
      try {
        // create a binary tree of debut entries
        await _service.createIndex('index', ['debut']);

        final request = {
          'selector': {
            'debut': {'\$gt': 1990},
            'type': {'\$eq': 'GameHero'}
          },
          'sort': ['debut']
        };

        // sort by debut
        final response = await _service.find<GameHero>(request);

        final results = response.results;
        expect(results.length, isPositive);

        var i = 0;
        var debutsSeen = [];

        results.forEach((result) {
          var currentDebut = result.debut;

          /*
          For each iteration:
          a. Test to see that currentDebut > all the debuts seen in the past 
            debutsSeen.
          b. add() the currentDebut to the debutsSeen list.
          ggc. Repeat.

          Here is what it looks like:

          0. 1990, []
          1. 1990, [1990]
          2. 1992, [1990, 1990]
          3. 1993, [1990, 1990, 1992]
          4. 1994, [1990, 1990, 1992, 1993]
          5. 1996, [1990, 1990, 1992, 1993, 1994]
          6. 1996, [1990, 1990, 1992, 1993, 1994, 1996]

          */

          if (i > 0) {
            debutsSeen.forEach((debutSeen) {
              expect(currentDebut.compareTo(debutSeen), isNonNegative);
            });
          }

          debutsSeen.add(currentDebut);

          i++;
        });
      } catch (e) {
        fail('Error: $e');
      }
    });

    test('test find() Mario series only', () async {
      try {
        // create a binary tree of debut entries
        await _service.createIndex('index', ['series', 'debut']);

        // For databases whose documents do not all have the same fields,
        // make sure to put all fields which appear in sort in selector.
        // In this case, debut needs to be in the selector as well.
        final request = {
          'selector': {
            'series': {'\$eq': 'Mario'},
            'type': {'\$eq': 'GameHero'},
            'debut': {'\$exists': true}
          },
          'sort': [
            {'series': 'desc'},
            {'debut': 'desc'}
          ]
        };

        final response = await _service.find<GameHero>(request);

        final results = response.results;
        expect(results.length, isPositive);
        var i = 0;
        var debutsSeen = [];

        results.forEach((result) {
          var currentDebut = result.debut;
          /*
          For each iteration:
          a. Test to see that currentDebut < all the debuts seen in the past 
            debutsSeen.
          b. add() the currentDebut to the debutsSeen list.
          c. Repeat.

          Here is what it looks like:
          0, 1990, []
          1, 1983, [1990]
          2, 1981, [1990, 1983]
          3, 1981, [1990, 1983, 1981]

          */

          if (i > 0) {
            debutsSeen.forEach((debutSeen) {
              expect(currentDebut.compareTo(debutSeen), isNonPositive);
            });
          }

          expect(result.series, equals('Mario'));

          debutsSeen.add(currentDebut);

          i++;
        });
      } catch (e) {
        fail('Error: $e');
      }
    });

    test("test find() with pagination", () async {
      try {
        // example of how to find number of items first
        var request = {
          'selector': {
            '_id': {'\$gte': 'dk'},
            'type': {'\$eq': 'GameHero'}
          },
          'sort': ['_id']
        };
        var response = await _service.find<GameHero>(request);
        var numOfItems = response.results.length;
        expect(numOfItems, 12);

        // sort by id with pagination
        int pageSize = 5;
        request = {
          'selector': {
            '_id': {'\$gte': 'dk'},
            'type': {'\$eq': 'GameHero'}
          },
          'limit': pageSize,
          'sort': ['_id']
        };

        int i = 0;
        bool keepGoing = true;
        while (keepGoing) {
          request['skip'] = pageSize * i;
          response = await _service.find<GameHero>(request);

          if (0 == i) {
            expect(response.hasNextPage, true);

            List expectedIds = ['dk', 'falcon', 'fox', 'kirby', 'link'];
            var results = response.results;
            expect(results, isNotNull);
            expect(results.length, pageSize);

            int j = 0;
            results.forEach((result) {
              expect(result.id, expectedIds[j]);
              j++;
            });
          }

          if (1 == i) {
            expect(response.hasNextPage, true);

            List expectedIds = ['luigi', 'mario', 'ness', 'pikachu', 'puff'];
            var results = response.results;
            expect(results, isNotNull);
            expect(results.length, pageSize);

            int j = 0;
            results.forEach((result) {
              expect(result.id, expectedIds[j]);
              j++;
            });
          }

          if (2 == i) {
            expect(response.hasNextPage, false);

            List expectedIds = ['samus', 'yoshi'];
            var results = response.results;
            expect(results, isNotNull);
            expect(results.length, 2);

            int j = 0;
            results.forEach((result) {
              expect(result.id, expectedIds[j]);
              j++;
            });
          }

          keepGoing = response.hasNextPage;
          i++;
        }
      } catch (e) {
        fail('Error: $e');
      }
    });

    test('test map/reduce returning document', () async {
      try {
        final ddoc = DesignDocument((builder) => builder
          ..id = '_design/hero'
          ..views = MapBuilder<String, DesignDocumentView>({
            "find": DesignDocumentView((viewBuilder) =>
                viewBuilder..map = "function (doc) {emit(doc.name);}")
          }));

        await _service.upload<DesignDocument>(ddoc);

        final queryOptions = {'key': 'Captain Falcon', 'include_docs': true};

        final gameHerosFromService =
            await _service.query<GameHero>('hero/find', queryOptions);

        var gameHeroExpected = await _service.fetch('falcon');

        expect(gameHerosFromService.length, equals(1));
        expect(gameHerosFromService[0], gameHeroExpected);
      } catch (e) {
        fail('Error: $e');
      }
    });

    test('test map/reduce returning reduced value', () async {
      try {
        final ddoc = DesignDocument((builder) => builder
          ..id = '_design/hero'
          ..views = MapBuilder<String, DesignDocumentView>({
            "series": DesignDocumentView((viewBuilder) => viewBuilder
              ..reduce = '_count'
              ..map = "function (doc) {emit(doc.series, 1);}")
          }));

        await _service.upload<DesignDocument>(ddoc);

        final queryOptions = {'key': 'Pokemon', 'reduce': true};
        final result = await _service.query('hero/series', queryOptions);

        expect(result[0]['value'], 2);
      } catch (e) {
        fail('Error: $e');
      }
    });
  }); // group('CRUD', () {
}
