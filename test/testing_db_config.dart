/// To test with local CouchDB, change the `db_prefix` to `http://localhost:5984/poucher`
/// To test with remote Cloudant, change according too.
///
/// Remember to use your own credential for above options.
///
/// To test with browser's local storage with Poucher, keep the username and password to `null`

const String db_prefix = 'poucher';
const String username = null;
const String password = null;
