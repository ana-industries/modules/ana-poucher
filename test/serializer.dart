import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

import 'package:ana_poucher/src/poucher_doc.dart';

import 'test_data.dart';

part 'serializer.g.dart';

@SerializersFor([
  BuiltList,
  BuiltMap,
  DesignDocument,
  DesignDocumentView,
  JsonObject,
  Animal,
  GameHero,
  Building,
])
final Serializers serializers = _$serializers;
final standardSerializers = (serializers.toBuilder()
      ..addPlugin(StandardJsonPlugin(discriminator: 'type')))
    .build();
